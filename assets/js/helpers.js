const noop = () => {};
const month = ['January','February','March','April','May','June','July','August','September','October','November','December'];
function constructObjectFromFormData(elementSelector) {
  const selectedElement = $(elementSelector);

  if (selectedElement[0].localName !== 'form') {
    return {};
  }

  const formData = selectedElement.serializeArray();
  let payload = {};

  for (let x = 0; x < formData.length; x++) {
    payload[formData[x].name] = formData[x].value
  }

  return payload;
}

function validateFields(elementSelector) {
  const fields = $(elementSelector).find('input, textarea, select');
  let isFieldsValid = true;

  for (let x = 0; x < fields.length; x++) {
    const field = fields[x];
    const fieldContainer = $(field).parents('.form-group');

    if (field.validity.valid) {
      fieldContainer.removeClass('has-error');
      continue;
    }

    fieldContainer.addClass('has-error');
    isFieldsValid = false;
  }

  return isFieldsValid;
}

function invokeLoading() {
	Swal.fire({
		title: LOADING_MESSAGE,
		imageUrl: `${baseUrl}/assets/images/spinner.svg`,
		imageWidth: 70,
		imageHeight: 70,
		imageAlt: 'Loading GIF',
		allowOutsideClick: false,
		allowEscapeKey: false,
		showCloseButton: false,
		showConfirmButton: false
	})
}

function dismissDialog() {
	Swal.close();
}

function invokeDialog(options) {
  const {
    type = SWEETALERT.TYPE.SUCCESS,
    title = '',
    text = '',
    confirmButtonText = SWEETALERT.TEXT.COMMON.OK_BUTTON,
    cancelButtonText = SWEETALERT.TEXT.COMMON.CANCEL_BUTTON,
    okCallback = noop,
    cancelCallback = noop
  } = options;

  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success swal2-btn',
      cancelButton: 'btn btn-danger swal2-btn'
    },
    buttonsStyling: false,
  });

  swalWithBootstrapButtons.fire({
    title,
    text,
    type,
    confirmButtonText,
    cancelButtonText,
    showCancelButton: true
  }).then((result) => {
    if (result.value) {
      okCallback();
    } else if (result.dismiss === Swal.DismissReason.cancel) {
      cancelCallback();
    }
  })
}

function invokeError(customMessage) {
  Swal.fire({
    type: SWEETALERT.TYPE.ERROR,
    title: SWEETALERT.TITLE.ERROR,
    text: customMessage || SWEETALERT.TEXT.COMMON.ERROR
  });
}

function navigateTo(route) {
  window.location.href = route;
}

/*=========================*/
/*      DataTable		   */
/*=========================*/
const renderNumericValue = (cell, cellData) => {
	$(cell).text(formatNumber(cellData));
};


/*=========================*/
/*      Accounting		   */
/*=========================*/
function formatNumber(number) {
	return accounting.formatNumber(number, COMMON_ACCOUNTING_SETTINGS);
}

function unformatNumber(formattedNumber) {
	return accounting.unformat(formattedNumber, COMMON_ACCOUNTING_SETTINGS.decimal);
}

function getMonthName(monthNo){
	return month[monthNo];
}

$(document).ready(function() {
	$('body').on('keyup change', '.accounting-enabled', function (event) {
		if (event.key !== '-') {
			const rawValue = unformatNumber($(this).val());
			$(this).val(formatNumber(rawValue));
		}
	});
});
