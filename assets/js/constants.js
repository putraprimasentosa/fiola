// Common Sweetalert messages
const SWEETALERT = {
	TITLE: {
		ERROR: 'Terjadi Kesalahan',
		SUCCESS: 'Berhasil Tersimpan'
	},
	TEXT: {
		COMMON: {
			ERROR: 'Terjadi Kesalahan. Silahkan coba lagi.',
			OK_BUTTON: 'Ya',
			CANCEL_BUTTON: 'Tidak'
		},
		RETURN: {
			ERROR_GET_ORDER: 'Gagal memuat pesanan. Silahkan coba kembali',
			INVALID_DETAIL_AMOUNT: 'Ada jumlah yang tidak cocok di detil retur Anda. Harap periksa kembali.',
			INVALID_REQUIRED_FIELDS: 'Harap isi semua kolom informasi retur.',
			ALL_AMOUNTS_ARE_ZERO: 'Semua jumlah retur barang Anda adalah 0. Harap periksa kembali.',
			SAVE_FAILED: 'Terjadi kesalahan saat menyimpan data retur Anda. Silahkan coba kembali',
			SAVE_SUCCESS: 'Berhasil menyimpan data retur. Lanjut ke pembuatan nota?'
		}
	},
	TYPE: {
		ERROR: 'error',
		SUCCESS: 'success'
	}
};

// Date Format
const INTENDED_DATE_FORMAT = 'DD-MM-YYYY';
const INTENDED_DATE_MONTH_FORMAT = 'YYYY-MM';
const DB_DATE_FORMAT = 'YYYY-MM-DD';

// Return
const NO_COURIER = 'TANPA KURIR';
const SELECT_ORDER_PLACEHOLDER = 'Pilih Pesanan';
const GOODS_CONDITIONS = {
	INTACT: 'intact',
	CRACKED: 'cracked',
	DESTROYED: 'destroyed',
	LOST: 'lost',
	OTHERS: 'others'
};

// Others
const LOADING_MESSAGE = 'Memuat...';
const DESCRIPTION_PLACEHOLDER = 'Keterangan';
const COMMON_ACCOUNTING_SETTINGS = {
	precision: 0,
	thousand: '.',
	decimal: ','
};
