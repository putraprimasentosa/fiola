<div class="content-wrapper">
    <section class="content">
        <table class="table table-bordered">
            <h2 class="text-center margin-bottom"><?php echo $title; ?></h2>
            <form id="form-add-product">
                <tr>
                    <td>Kode : <font color="red">*</font></td>
                    <td colspan="5"><input name="code" id="code" value="<?php echo $data['code']; ?>" class="form-control" required /></td>
                </tr>
                <tr>
                    <td>Nama : <font color="red">*</font></td>
                    <td colspan="5"><input name="name" id="name" value="<?php echo $data['name']; ?>" class="form-control" required /></td>
                </tr>
                <tr>
                    <td>Brand : <font color="red">*</font></td>
                    <td colspan="5"><input name="brand" id="brand" value="<?php echo $data['brand']; ?>" class="form-control" required /></td>
                </tr>
                <tr>
                    <td>Satuan : <font color="red">*</font></td>
                    <td colspan="2">
                        <select class="form-control" name="satuan_id" id="satuan_id" style="width:100%" required>
                            <option value="" disabled selected>-- Pilih Satuan --</option>
                          <?php foreach ($Satuan as $row) {
                            echo "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
                          }
                          ?>
                        </select>
                    </td>
                    <td>Jenis : <font color="red">*</font></td>
                    <td colspan="2">
                        <select class="form-control" name="m_product_category_id" id="m_product_category_id" style="width:100%" required>
                            <option value="" disabled selected>-- Pilih Jenis --</option>
                          <?php foreach ($Category as $row) {
                            echo "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
                          }
                          ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <center><strong>Dimensi</strong></center>
                    </td>
                </tr>
                <tr>
                    <td>Panjang : <font color="red">*</font></td>
                    <td>
                        <div class="input-group">
                            <input type="number" class="form-control" id="dimensionx" name="dimensionx" value="<?php echo $data['dimensionx']; ?>">
                            <div class="input-group-addon">M</div>
                        </div>
                    </td>
                    <td>Lebar : <font color="red">*</font></td>
                    <td>
                        <div class="input-group">
                            <input type="number" class="form-control" id="dimensiony" name="dimensiony" value="<?php echo $data['dimensiony']; ?>">
                            <div class="input-group-addon">M</div>
                        </div>
                    </td>
                    <td>Tebal : <font color="red">*</font></td>
                    <td>
                        <div class="input-group">
                            <input type="number" class="form-control" id="dimensionz" name="dimensionz" value="<?php echo $data['dimensionz']; ?>">
                            <div class="input-group-addon">M</div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Berat : <font color="red">*</font></td>
                    <td colspan="5">
                        <div class="input-group">
                            <input type="number" class="form-control" id="weight" name="weight" value="<?php echo $data['weight']; ?>">
                            <div class="input-group-addon">Kg</div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Harga Jual : <font color="red">*</font></td>
                    <td colspan="5">
                        <div class="input-group">
                            <div class="input-group-addon">Rp</div>
                            <input type="text" class="form-control" id="unitprice" name="unitprice" placeholder="Amount" value="<?php echo $data['unitprice']; ?>">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td colspan="5">
                        <label>Jual
                            <input type="radio" id="isactive" name="isactive" value=1 >
                        </label>
                        <label>Tidak Jual
                            <input type="radio" id="isactive" name="isactive" value=0 >
                        </label>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="6" style="text-align: center">
                        <button type="submit" class="btn btn-success" id="simpan-anggota">
                            Simpan
                        </button>
                        <button type="button" class="btn btn-danger" id="bersih">
                            Bersihkan
                        </button>
                    </td>
                </tr>
            </form>
        </table>
    </section>
</div>

<script>
  $(document).ready(function () {
    $("#form-add-product").submit(function (event){
        event.preventDefault();
        var data = {};
            $.each($("#form-add-product").serializeArray(), function(key, value){
            data[value.name] = value.value;            
        });
        $.ajax({
            url:"<?php echo base_url();?>master/updateProduct/<?php echo $data['id']; ?>",
            type: "POST",
            datatype: "json",
            data:{ product: JSON.stringify(data)},
            //success:function(e){}
        })
        .done(function (data) {
            Swal.fire({
                title: 'Sukses!',
                text: 'Berhasil Memasukkan Data',
                type: 'success',
                confirmButtonText: 'OK',
                    onAfterClose: () => window.location.reload()
            });
        })
        .fail(function (e) {
            //console.log(e);
            Swal.fire({
                title: 'Gagal!',
                text: 'Data Belum Masuk, Silahkan Coba Lagi',
                type: 'error',
                confirmButtonText: 'OK'
            });
        });
    });

    $('#m_product_category_id').val('<?php echo $data['m_product_category_id']; ?>');
    $('#satuan_id').val('<?php echo $data['satuan_id']; ?>');
    $("input[name=isactive][value=<?php echo $data['isactive']; ?>]").prop("checked", true);

    $('#bersih').on('click', function () {
        $('#code').val('');
        $('#name').val('');
        $('#brand').val('');
        $('#satuan_id').val('');
        $('#m_product_category_id').val('');
        $('#dimensionx').val('');
        $('#dimensiony').val('');
        $('#dimensionz').val('');
        $('#weight').val('');
        $('#unitprice').val('');   
        $("input[name=isactive][value=1]").prop("checked", true);
    });

  })
</script>