<div class="content-wrapper">
    <section class="content">
        <h2 class="text-center margin-bottom"><?php echo $title; ?></h2>
        <form id="form-add">
            <div class="row">
                <div class="row form-group col-md-12">
                    <label for="taxid" class="col-sm-2 control-label">Ekspedisi <span
                                class="text-red">*</span></label>
                    <div class="col-sm-10">
                        <select name="m_courier_id" id="m_courier_id" class="form-control" required>
                            <option value="" disabled selected>-- Pilih Ekspedisi --</option>
                          <?php foreach ($Courier as $row) {
                            echo "<option value='" . $row['id'] . "'". ">" . $row['name'] . "</option>";
                          }
                          ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row form-group col-md-12">
                    <label for="m_province_id" class="col-sm-2 control-label">Provinsi <span
                                class="text-red">*</span></label>
                    <div class="col-sm-10">
                        <select name="m_province_id" id="m_province_id" class="form-control" required>
                            <option value="" disabled selected>-- Pilih Provinsi --</option>
                          <?php foreach ($Province as $row) {
                            echo "<option value='" . $row['id'] . "'". ">" . $row['name'] . "</option>";
                          }
                          ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row form-group col-md-12">
                    <label for="m_province_city_id" class="col-sm-2 control-label">Kota <span class="text-red">*</span></label>
                    <div class="form-group col-sm-10">
                        <select name="m_province_city_id" id="m_province_city_id" class="form-control" required>
                            <option value="" disabled selected>-- Pilih Kota --</option>
                            <?php foreach ($City as $row) {
                                echo "<option value='" . $row['id'] . "'". ">" . $row['name'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row form-group col-md-12">
                    <label for="price" class="col-sm-2 col-md-2 col-lg-2 control-label">Tarif <span class="text-red">*</span></label>
                    <div class="form-group col-sm-10 col-md-10 col-lg-10">
                        <div class="input-group">
                            <div class="input-group-addon">Rp</div>
                            <input type="text" class="form-control" id="price" name="price" placeholder="Tarif"
                                value="<?php echo $data['price']; ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="col-md-6">
                    <a href="<?php echo base_url(); ?>master/masterFare"
                       class="btn btn-warning"
                    >
                        <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                        Kembali
                    </a>
                </div>
                <div class="col-md-6 text-right" style="padding-right: 60px">
                    <button type="submit" class="btn btn-success" id="simpan">
                        Simpan
                    </button>
                    <button type="button" class="btn btn-danger" id="bersih">
                        Bersihkan
                    </button>
                </div>
            </div>
        </form>
    </section>
</div>

<script>
  $(document).ready(function () {
    $('#m_province_id').on('change', function () {
      $.ajax({
        url: "<?php echo base_url()?>MasterApi/getCity",
        type: 'POST',
        data: {
          provinceId: $(this).val()
        }
      })
      .done(function (result) {
        let cityElements = $('#m_province_city_id');
        const cities = jQuery.parseJSON(result);

        cityElements.html(`
            <option value="">-- Pilih Kota --</option>
        `);
        for (let x = 0; x < cities.length; x++) {
          cityElements.append(`
            <option value="${cities[x].id}">${cities[x].name}</option>
          `);
        }
        if(cityElements.val() === ""){
            cityElements.val('<?php echo $data['m_province_city_id']; ?>');
        }
      })
      .fail(function (error) {
        console.log(error);
      });
    });

    $('#m_province_id').val(<?php echo $fare_city->province->id; ?>).change();
    $('#m_province_city_id').val('<?php echo $data['m_province_city_id']; ?>').change();
    $('#m_courier_id').val('<?php echo $data['m_courier_id']; ?>').change();    
    $("#form-add").submit(function (event){
        event.preventDefault();
        var data = {};
            $.each($("#form-add").serializeArray(), function(key, value){
                if(value.name !== 'm_province_id'){
                    data[value.name] = value.value;
                }
            });
        $.ajax({
            url:"<?php echo base_url();?>master/updateFare/<?php echo $data['id']; ?>",
            type: "POST",
            datatype: "json",
            data:{ fare: JSON.stringify(data)},
            //success:function(e){}
        })
        .done(function (data) {
            res = JSON.parse(data);
            console.log(res);
            if(res.code == '200'){
                Swal.fire({
                    title: 'Sukses!',
                    text: 'Berhasil Memasukkan Data',
                    type: 'success',
                    confirmButtonText: 'OK',
                    onAfterClose: () => window.location.reload()
                });
            } else {
                Swal.fire({
                    title: 'Gagal!',
                    text: 'Maaf. '+res.message,
                    type: 'error',
                    confirmButtonText: 'OK'
                });
            }
        })
        .fail(function (e) {
            //console.log(e);
            Swal.fire({
                title: 'Gagal!',
                text: 'Data Belum Masuk, Silahkan Coba Lagi. '+e,
                type: 'error',
                confirmButtonText: 'OK'
            });
        });
    });

    $('#bersih').on('click', function () {
        $('#courier_id').val('');
        $('#m_province_id').val('');
        $('#m_province_city_id').empty();
        $('#price').val('');
    });

  })
</script>