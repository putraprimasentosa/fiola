<div class="container-fluid">
    <a class="btn btn-sm bg-gradient-primary" id="addNewAdmin" style="color:white;" data-toggle="collapse" data-target="#collapseForm"><span class="fa fa-user-plus"></span> Tambah Admin Baru</a>
    <hr />
    <h3 style="text-decoration: underline;">
        <strong>Daftar Admin</strong>
    </h3>
    <div id="collapseForm" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <table class="table table-bordered">
            <form id="form-add-admin" data-toggle="validator" role="form">
                <tr>
                    <td>Nama Lengkap : <font color="red">*</font>
                    </td>
                    <td colspan="5"><input name="admin_name" id="name" value="" class="form-control" required /></td>
                    <td>Kontak : <font color="red">*</font>
                    </td>
                    <td colspan="5"><input type="number" name="admin_contact" id="contact" value="" class="form-control" required /></td>
                </tr>
                <tr>
                    <td>Alamat : <font color="red">*</font>
                    </td>
                    <td colspan="10"><input name="admin_address" id="address" value="" class="form-control" required /></td>
                </tr>
                <tr>
                    <td>Username : <font color="red">*</font>
                    </td>
                    <td colspan="5">
                        <div class="input-group">
                            <input type="text" name="username" id="username" class="form-control small" placeholder="Username" required>
                        </div>
                    </td>
                    <td>Status : <font color="red">*</font>
                    </td>
                    <td colspan="5">
                        <select class="form-control" name="status" id="status" style="width:100%" required>
                            <option value="1">Aktif</option>
                            <option value="0">Nonaktif
                            <option>
                        </select>
                    </td>
                </tr>
                <tr>

                </tr>
                <tr>
                    	<td>Kata Sandi : <font color="red">*</font>
                    </td>
                    <td colspan="5">
                        <div class="input-group">
                            <input type="password" class="form-control" id="password" name="password" placeholder="****" required>
                        </div>
                    </td>
                    <td>Hak Akses</td>
                    <td colspan="5">
                        <select class="form-control" id="role_id" name="role">
                            <option value="1">User</option>
                            <option value="2">Admin</option>
                        </select>
                    </td>
                </tr>
                <tr>

                </tr>

                <tr>
                    <td colspan="10" style="text-align: center">
                        <button type="button" class="btn btn-sm btn-success" id="form-submit">
                            <span class="fa fa-check"></span>
                            Simpan
                        </button>
                        <button type="reset" class="btn btn-sm btn-danger" id="form-cancel">
                            <span class="fa fa-ban"></span>
                            Batal
                        </button>
                    </td>
                </tr>
            </form>
        </table>
        <hr>
    </div>
    <div id="collapseFormUpdate" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <table class="table table-bordered">
            <form id="form-update-admin">
                <tr>
                    <td>Nama Lengkap : <font color="red">*</font>
                    </td>
                    <td colspan="5"><input name="product_code" id="code" value="" class="form-control" required /></td>
                    <td>Kontak : <font color="red">*</font>
                    </td>
                    <td colspan="5"><input name="product_name" id="name" value="" class="form-control" required /></td>
                </tr>
                <tr>
                    <td>Alamat : <font color="red">*</font>
                    </td>
                    <td colspan="10"><input name="product_brand" id="brand" value="" class="form-control" required /></td>
                </tr>
                <tr>
                    <td>Username : <font color="red">*</font>
                    </td>
                    <td colspan="5">
                        <div class="input-group">
                            <input type="text" name="product_size" id="size" class="form-control small" placeholder="Username">
                        </div>
                    </td>
                    <td>Status : <font color="red">*</font>
                    </td>
                    <td colspan="5">
                        <select class="form-control" name="product_uom_id" id="uom" style="width:100%">
                            <option value="1">Aktif</option>
                            <option value="0">Nonaktif
                            <option>
                        </select>
                    </td>
                </tr>
                <tr>

                </tr>
                <tr>
                    <td>Kata Sandi : <font color="red">*</font>
                    </td>
                    <td colspan="5">
                        <div class="input-group">
                            <input type="password" class="form-control" id="price" name="product_price" placeholder="****">
                        </div>
                    </td>
                    <!-- <td>Status</td>
                    <td colspan="5">
                        <select class="form-control">
                            <option>Jual</option>
                            <option>Discontinue</option>
                        </select>
                    </td> -->
                </tr>
                <tr>

                </tr>

                <tr>
                    <td colspan="10" style="text-align: center">
                        <button type="button" class="btn btn-sm btn-success" id="form-submit2">
                            <span class="fa fa-check"></span>
                            Simpan
                        </button>
                        <button type="reset" class="btn btn-sm btn-danger" id="form-cancel">
                            <span class="fa fa-ban"></span>
                            Batal
                        </button>
                    </td>
                </tr>
            </form>
        </table>
        <hr>
    </div>
    <table id="admin-data-table" class="table table-bordered">
        <thead>
            <tr>
                <th></th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Kontak</th>
                <th>Username</th>
                <th>Status</th>
                <th>Tindakan</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($admin as $data) : ?>
                <tr>
                    <td><?php echo $data['id'] ?></td>
                    <td><?php echo $data['admin_name'] ?></td>
                    <td><?php echo $data['admin_address'] ?></td>
                    <td><?php echo $data['admin_contact'] ?></td>
                    <td><?php echo $data['username'] ?></td>
                    <td><?php echo $data['status'] > 0 ? 'Active' : 'InActive' ?></td>
                    <td><?php if ($data['status'] > 0) {
                                ?>
                            <button type="button" class="btn btn-warning btn-sm btn-block deactive" data-id="<?php echo $data['id'];?>"><span class="fa fa-ban"></span> Nonaktivasi</button>
                        <?php
                            } else {
                                ?>
                            <button type="button" class="btn btn-success btn-sm btn-block active" data-id="<?php echo $data['id'];?>"><span class="fa fa-redo"></span> Aktivasi</button>
                        <?php
                            } ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script>
    var dTable = null;
    $(document).ready(function() {
        dTable = $("#admin-data-table").DataTable({
            "columnDefs": [{
                "targets": [0],
                "visible": false
            }]
        });
    })

    $("#form-submit").click(function(e) {
        $("#form-add-admin").submit();
    });

    $("#form-add-admin").submit(function(event) {
        event.preventDefault();
        var validate = true;

		$("form#form-add-admin").each(function(){
			if(!$(this)[0].checkValidity())
				validate = false;
		});
		if(!validate){
			Swal.fire({
				title: 'Silahkan isi data terlebih dahulu!',
				text: 'Tidak ada data yang terisi',
				type: 'error',
				confirmButtonText: 'OK'
			});
			return;
		};


        var data = {};
        $.each($("#form-add-admin").serializeArray(), function(key, value) {
            data[value.name] = value.value;
        });
        console.log(data);
        $.ajax({
                url: "<?php echo base_url(); ?>master/insertAdmin",
                type: "POST",
                datatype: "json",
                data: {
                    admin: JSON.stringify(data)
                },
                //success:function(e){}
            })
            .done(function(data) {
                Swal.fire({
                    title: 'Sukses!',
                    text: 'Berhasil Memasukkan Data',
                    type: 'success',
                    confirmButtonText: 'OK',
                    onAfterClose: () => window.location.reload()
                });
            })
            .fail(function(e) {
                //console.log(e);
                Swal.fire({
                    title: 'Gagal!',
                    text: 'Data Belum Masuk, Silahkan Coba Lagi',
                    type: 'error',
                    confirmButtonText: 'OK'
                });
            });
    });
    $("#admin-data-table").on('click', 'button.deactive', function(e) {
        Swal.fire({
            title: 'Yakin?',
            text: "User akan diaktifkan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                        url: "<?php echo base_url() ?>/master/deActiveAdmin",
                        type: "POST",
                        data: {
                            id: $(this).data("id"),
                        }
                    })
                    .done(function(addressRawData) {
                        Swal.fire(
                            'De-Activated!',
                            '',
                            'success'
                        )
                        window.location.reload();
                    })
                    .fail(function() {
                        Swal.fire(
                            'Cancelled',
                            'Gagal untuk mengubah status',
                            'error'
                        )
                    });

            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire(
                    'Cancelled',
                    'Proses Dibatalkan.',
                    'error'
                )
            }
        });
    });
    $("#admin-data-table").on('click', 'button.active', function(e) {
        Swal.fire({
            title: 'Yakin?',
            text: "User akan diaktifkan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                        url: "<?php echo base_url() ?>/master/activeAdmin",
                        type: "POST",
                        data: {
                            id: $(this).data("id"),
                        }
                    })
                    .done(function(addressRawData) {
                        Swal.fire(
                            'Activated!',
                            '',
                            'success'
                        )
                        window.location.reload();
                    })
                    .fail(function() {
                        Swal.fire(
                            'Cancelled',
                            'Gagal untuk mengubah status',
                            'error'
                        )
                    });

            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire(
                    'Cancelled',
                    'Proses Dibatalkan.',
                    'error'
                )
            }
        });
    });
</script>
