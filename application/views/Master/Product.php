<div class="container-fluid">
	<!-- <a class="btn btn-block btn-info" href="<?php echo base_url(); ?>master/addProduct"><span class="fa fa-gift"></span> Add New Product</a> -->
	<a class="btn btn-sm bg-gradient-primary" id="addNewProduct" style="color:white;" data-toggle="collapse"
	   data-target="#collapseForm"><span class="fa fa-gift"></span> Tambah Produk Baru</a>
	<hr>
	<h3 style="text-decoration: underline;">
		<strong>Daftar Produk (List Product)</strong>
	</h3>
	<div id="collapseForm" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
		<table class="table table-bordered">
			<form id="form-add-product">
				<tr>
					<td>Kode : <font color="red">*</font>
					</td>
					<td colspan="5"><input name="product_code" id="code" value="" class="form-control" required/></td>
					<td>Nama : <font color="red">*</font>
					</td>
					<td colspan="5"><input name="product_name" id="name" value="" class="form-control" required/></td>
				</tr>
				<tr>
					<td>Brand : <font color="red">*</font>
					</td>
					<td colspan="5"><input name="product_brand" id="brand" value="" class="form-control" required/></td>
					<td>Berat (Kg) : <font color="red">*</font>
					</td>
					<td colspan="5">
						<div class="input-group">
							<input type="number" step="any" class="form-control border-0 small" name="product_weight"
								   id="weight" placeholder="Weight" required>
							<div class="input-group-append">
								<button class="btn btn-primary" type="button" disabled>
									Kg
								</button>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>Ukuran : <font color="red">*</font>
					</td>
					<td colspan="5">
						<div class="input-group">
							<input type="text" name="product_size" id="size" class="form-control small"
								   placeholder="Ukuran" required>
						</div>
					</td>
					<td>Satuan : <font color="red">*</font>
					</td>
					<td colspan="5">
						<select class="form-control" name="product_uom_id" id="uom" style="width:100%">
							<?php foreach ($UOM as $row) {
								echo "<option value='" . $row['id'] . "'>" . $row['uom_name'] . "</option>";
							}
							?>
						</select>
					</td>
				</tr>
				<tr>

				</tr>
				<tr>
					<td>Harga Jual : <font color="red">*</font>
					</td>
					<td colspan="5">
						<div class="input-group">
							<div class="input-group-append">
								<button class="btn btn-primary" type="button" disabled>
									Rp
								</button>
							</div>
							<input type="text" class="form-control" id="price" name="product_price"
								   placeholder="Amount" required>
						</div>
					</td>
					<!-- <td>Status</td>
					<td colspan="5">
						<select class="form-control">
							<option>Jual</option>
							<option>Discontinue</option>
						</select>
					</td> -->
				</tr>
				<tr>

				</tr>

				<tr>
					<td colspan="10" style="text-align: center">
						<button type="button" class="btn btn-sm btn-success" id="form-submit">
							<span class="fa fa-check"></span>
							Simpan
						</button>
						<button type="reset" class="btn btn-sm btn-danger" id="form-cancel">
							<span class="fa fa-ban"></span>
							Batal
						</button>
					</td>
				</tr>
			</form>
		</table>
		<hr>
	</div>
	<div id="collapseFormUpdate" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
		<table class="table table-bordered">
			<form id="form-update-product">
				<tr>

					<td>Kode : <font color="red">*</font>
					</td>
					<td colspan="5"><input class="form-control" id="u_id" value="" hidden/><input name="product_code"
																								  id="ucode" value=""
																								  class="form-control"
																								  required/></td>
					<td>Nama : <font color="red">*</font>
					</td>
					<td colspan="5"><input name="product_name" id="uname" value="" class="form-control" required/></td>
				</tr>
				<tr>
					<td>Brand : <font color="red">*</font>
					</td>
					<td colspan="5"><input name="product_brand" id="ubrand" value="" class="form-control" required/>
					</td>
					<td>Berat (Kg) : <font color="red">*</font>
					</td>
					<td colspan="5">
						<div class="input-group">
							<input type="text" class="form-control border-0 small" name="product_weight" id="uweight"
								   placeholder="Berat" required>
							<div class="input-group-append">
								<button class="btn btn-primary" type="button" disabled>
									Kg
								</button>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>Ukuran : <font color="red">*</font>
					</td>
					<td colspan="5">
						<div class="input-group">
							<input type="text" name="product_size" id="usize" class="form-control small"
								   placeholder="Size" required>
						</div>
					</td>
					<td>Satuan : <font color="red">*</font>
					</td>
					<td colspan="5">
						<select class="form-control" name="product_uom_id" id="uuom" style="width:100%" required>
							<option value="" disabled selected>-- Pilih Satuan --</option>
							<?php foreach ($UOM as $row) {
								echo "<option value='" . $row['id'] . "'>" . $row['uom_name'] . "</option>";
							}
							?>
						</select>
					</td>
				</tr>
				<tr>

				</tr>
				<tr>
					<td>Harga Jual : <font color="red">*</font>
					</td>
					<td colspan="5">
						<div class="input-group">
							<div class="input-group-append">
								<button class="btn btn-primary" type="button" disabled>
									Rp
								</button>
							</div>
							<input type="text" class="form-control" id="uprice" name="product_price"
								   placeholder="Harga" required>
						</div>
					</td>
					<!-- <td>Status</td>
					<td colspan="5">
						<select class="form-control">
							<option>Jual</option>
							<option>Discontinue</option>
						</select>
					</td> -->
				</tr>
				<tr>

				</tr>

				<tr>
					<td colspan="10" style="text-align: center">
						<button type="button" class="btn btn-sm btn-success" id="form-update-submit">
							<span class="fa fa-check"></span>
							Simpan
						</button>
						<button type="reset" class="btn btn-sm btn-danger" id="form-update-cancel">
							<span class="fa fa-ban"></span>
							Batal
						</button>
					</td>
				</tr>
			</form>
		</table>
		<hr>
	</div>
	<table id="product-data-table" class="table table-bordered">
		<thead>
		<tr>
			<th></th>
			<th id="kode">Kode Produk</th>
			<th>Nama</th>
			<th>Ukuran</th>
			<th>Satuan</th>
			<th>Brand</th>
			<th>Harga</th>
			<th>Berat (Kg)</th>
			<th>Action</th>
			<th></th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($Product as $data) : ?>
			<tr>
				<td><?php echo $data['id']; ?></td>
				<td><?php echo $data['product_code']; ?></td>
				<td><?php echo $data['product_name']; ?></td>
				<td><?php echo $data['product_size']; ?> </td>
				<td><?php echo $data['uom']['uom_name']; ?> </td>
				<td><?php echo $data['product_brand']; ?> </td>
				<td><?php echo number_format($data['product_price'], 0, ',', '.'); ?> </td>
				<td><?php echo $data['product_weight']; ?> </td>
				<td align="center">
					<a href="<?php echo base_url(); ?>master/editMasterProduct/<?php echo $data['id']; ?>"
					   class="btn btn-info"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
						Edit
					</a>
				</td>
				<td><?php echo $data['product_uom_id']; ?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>

<script>
	var dTable = null;
	var isUpdate = 0;
	$(document).ready(function () {
		$("#uom, #uuom").select2();
		dTable = $('#product-data-table').DataTable({
			"columnDefs": [{
				"targets": 8,
				"data": null,
				"defaultContent": `
                            <button type='button' class='btn btn-sm btn-block btn-danger delRow' ><span class="fa fa-window-close"></span> Hapus</button>
                            <button type='button' class='btn btn-sm btn-block btn-info editRow'><span class="fa fa-pen-alt"></span>  Ubah</button>
                            `
			}, {
				"targets": [0, 9],
				"visible": false
			}]

		});
	})
	$("#form-submit").click(function () {
		$("#form-add-product").submit();
	});
	$("#form-add-product").submit(function (event) {
		event.preventDefault();

		let validate = true;
		$("form#form-add-product").each(function () {
			console.log($(this)[0].checkValidity());
			if (!$(this)[0].checkValidity())
				validate = false;
		});
		if (!validate) {
			Swal.fire({
				title: 'Silahkan isi data terlebih dahulu!',
				text: 'Tidak ada data yang terisi',
				type: 'error',
				confirmButtonText: 'OK'
			});
			return;
		}
		;

		var data = {};
		$.each($("#form-add-product").serializeArray(), function (key, value) {
			data[value.name] = value.value;
		});

		$.ajax({
			url: "<?php echo base_url(); ?>master/insertProduct",
			type: "POST",
			datatype: "json",
			data: {
				product: JSON.stringify(data)
			},
			//success:function(e){}
		})
				.done(function (data) {
					$("#product-data-table").DataTable().row.add(
							[
								0,
								$("#code").val(),
								$("#name").val(),
								$("#size").val(),
								$("#uom option:selected").text(),
								$("#brand").val(),
								$("#price").val(),
								$("#weight").val(), '', $("#uom option:selected").val()
							]
					).draw();
					Swal.fire({
						title: 'Sukses!',
						text: 'Berhasil Memasukkan Data',
						type: 'success',
						confirmButtonText: 'OK',
						onAfterClose: () => window.location.reload()
					});
				})
				.fail(function (e) {
					//console.log(e);
					Swal.fire({
						title: 'Gagal!',
						text: 'Data Belum Masuk, Silahkan Coba Lagi',
						type: 'error',
						confirmButtonText: 'OK'
					});
				});
	});

	$("#form-update-product").submit(function (e) {
		e.preventDefault();
		let validate = true;
		$("form#form-update-product").each(function () {
			console.log($(this)[0].checkValidity());
			if (!$(this)[0].checkValidity())
				validate = false;
		});
		if (!validate) {
			Swal.fire({
				title: 'Silahkan isi data terlebih dahulu!',
				text: 'Tidak ada data yang terisi',
				type: 'error',
				confirmButtonText: 'OK'
			});
			return;
		}
		;
		var data = {};
		$.each($("#form-update-product").serializeArray(), function (key, value) {
			data[value.name] = value.value;
		});
		console.log($("#uid").val());
		$.ajax({
			url: "<?php echo base_url(); ?>master/updateProduct",
			type: "POST",
			datatype: "json",
			data: {
				id: $("#u_id").val(),
				product: JSON.stringify(data)
			},
			//success:function(e){}
		})
				.done(function (data) {
					Swal.fire({
						title: 'Sukses!',
						text: 'Berhasil Mengubah Data',
						type: 'success',
						confirmButtonText: 'OK',
						onAfterClose: () => window.location.reload()
					});
				})
				.fail(function (e) {
					//console.log(e);
					Swal.fire({
						title: 'Gagal!',
						text: 'Data gagal diubah, silahkan coba lagi!',
						type: 'error',
						confirmButtonText: 'OK'
					});
				});
	});
	$("#product-data-table").on('click', 'button.delRow', function (e) {
		Swal.fire({
			title: 'Yakin?',
			text: "Data yang telah dihapus tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes, delete it!',
			cancelButtonText: 'No, cancel!',
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				var data = dTable.row($(this).parents('tr')).data();
				dTable.row($(this).parents('tr')).remove().draw();
				$.ajax({
					url: "<?php echo base_url() ?>/master/delProduct",
					type: "POST",
					data: {
						id: data[0],
					}
				})
						.done(function (addressRawData) {
							Swal.fire(
									'Deleted!',
									'Data tidak bisa dikembalikan.',
									'success'
							)
						})
						.fail(function () {
							Swal.fire(
									'Cancelled',
									'Penghapusan data gagal.',
									'error'
							)
						});

			} else if (
					/* Read more about handling dismissals below */
					result.dismiss === Swal.DismissReason.cancel
			) {
				Swal.fire(
						'Cancelled',
						'Penghapusan data dibatalkan.',
						'error'
				)
			}
		});
	});
	$("#product-data-table").on('click', 'button.editRow', function (e) {
		console.log(dTable.row($(this).parents('tr')).data());
		var data = dTable.row($(this).parents('tr')).data();
		$("#u_id").val(data[0]);
		$("#ucode").val(data[1]);
		$("#uname").val(data[2]);
		$("#usize").val(data[3]);
		$("#uuom").val(data[9]).trigger('change');
		$("#ubrand").val(data[5]);
		$("#uprice").val(unformatNumber(data[6]));
		$("#uweight").val(data[7]);
		$("#collapseFormUpdate").collapse('show');
		$("#collapseForm").collapse('hide');
	});

	$("#form-update-submit").click(function () {
		$("#form-update-product").submit();
	});

	$("#form-cancel").click(function () {
		$("#addNewProduct").click();
	});

	$("#addNewProduct, #form-update-cancel").click(function (e) {
		$("#collapseFormUpdate").collapse('hide');
	});
</script>
