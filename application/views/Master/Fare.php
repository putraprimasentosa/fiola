<div class="content-wrapper">
    <section class="content">
        <h3 style="text-decoration: underline;"><center><strong>Daftar Tarif</strong></center></h3>
        <br>
        <table id="Fare-data-table" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Ekspedisi</th>
                <th scope="col">Pengiriman</th>
                <th scope="col">Harga / kg</th>
                <th scope="col">Edit</th>
                <th scope="col">Hapus</th>
            </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
        <br>
        <table class="table table-bordered">
            <form id="form-master">
                <tr>
                    <td width="20%">Ekspedisi : <font color="red">*</font></td>
                    <td>
                        <select name="m_courier_id" id="m_courier_id" class="form-control" required>
                            <option value="0" disabled>-- Pilih Ekspedisi --</option>
                            <?php
                            foreach ($Courier as $courier) {
                                echo '<option value="' . $courier['id'] . '"> (' . $courier['code'] . ') ' . $courier['name'] . '</option>';
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="20%">Kota : <font color="red">*</font></td>
                    <td>
                        <select name="m_province_city_id" id="m_province_city_id" class="form-control" required>
                            <option value="0" disabled>-- Pilih Kota --</option>
                            <?php
                            foreach ($City as $city) {
                                echo '<option value="' . $city['id'] . '">' . $city->province->name . ' - ' . $city['name'] . '</option>';
                            }
                            ?>
                        </select> 
                    </td>                    
                </tr>
                <tr>
                    <td width="20%">Harga per kg : <font color="red">*</font></td>
                    <td>
                    <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input name="price" id="price" value="" placeholder="0" class="form-control" required/>
                    </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center">
                        <button type="button" class="btn btn-success" id="simpan-Fare">
                            Simpan
                        </button>
                        <button type="button" class="btn btn-danger" id="bersih">
                            Bersihkan
                        </button>
                    </td>
                </tr>
            </form>
        </table>
    </section>
</div>

<script>
    $(document).ready(function() {
        $("#m_courier_id").select2();
        $("#m_province_city_id").select2();

        $('.delete-Fare').on('click', function() {
            var _id = $(this).data('id');
            $.ajax({
                url: '<?php echo base_url(); ?>Master/deleteFare',
                type: 'POST',
                data: {
                    id: JSON.stringify(_id),
                }
            })
            .done(function (data) {
                Swal.fire({
                    title: 'Sukses!',
                    text: 'Berhasil Menghapus Data',
                    type: 'success',
                    confirmButtonText: 'OK',
                    onAfterClose: () => window.location.reload()
                });
            })
            .fail(function (e) {
                Swal.fire({
                    title: 'Gagal!',
                    text: 'Data Belum Terhapus, Silahkan Coba Lagi',
                    type: 'error',
                    confirmButtonText: 'OK'
                });
            });

        });

        $('#simpan-Fare').on('click', function () {
            event.preventDefault();
            var data = {};
                $.each($("#form-master").serializeArray(), function(key, value){
                data[value.name] = value.value;
            });
            $.ajax({
                url:"<?php echo base_url();?>master/insertFare",
                type: "POST",
                datatype: "json",
                data:{ fare: JSON.stringify(data) },
                //success:function(e){}
            })
            .done(function (data) {
                res = JSON.parse(data);
                console.log(res);
                if(res.code == '200'){
                    Swal.fire({
                        title: 'Sukses!',
                        text: 'Berhasil Memasukkan Data',
                        type: 'success',
                        confirmButtonText: 'OK',
                        onAfterClose: () => window.location.reload()
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        text: 'Maaf. '+res.message,
                        type: 'error',
                        confirmButtonText: 'OK'
                    });
                }
            })
            .fail(function (e) {
                //console.log(e);
                Swal.fire({
                    title: 'Gagal!',
                    text: 'Data Belum Masuk, Silahkan Coba Lagi. '+e,
                    type: 'error',
                    confirmButtonText: 'OK'
                });
            });
        });

        $('#Fare-data-table').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "lengthMenu": [25, 50, 100, "All"],
            "paging": true,
            // "serverSide": true,
            "order": [[0, "asc" ]],
            "ajax":{
                    url : '<?php echo base_url();?>master/getFare',
                    type : 'POST'
            }
        });

        function bersih(){
            $('#code').val('');
            $('#m_courier_id').val('');
            $('#m_province_city_id').val('');   
        }
               
        $('#bersih').on('click', function() {
            bersih();
        });
    })
</script>
