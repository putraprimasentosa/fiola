<div class="container-fluid">
    <section class="content">


        <br>
		<div>
			<a class="btn btn-sm bg-gradient-primary" id="addNewProvince" style="color:white;" data-toggle="collapse" data-target="#collapseForm"><span class="fa fa-city"></span> Tambah Kota Baru</a>
		</div>
		<br>
		<h3 style="text-decoration: underline;"><strong><?php echo $title; ?></strong></h3>
		<div id="collapseForm" class="collapse">
			<table class="table table-bordered">
				<form id="form-kota">
					<tr>
						<td colspan="6">
							<center><strong>Menambah Kota</strong></center>
						</td>
					</tr>
					<tr>
						<td width="20%">Nama Kota : <font color="red">*</font></td>
						<td>
							<input name="name" id="name" value="" class="form-control" required />
							<input type="hidden" name="m_province_id" id="m_province_id" value="<?php echo $Province['id']; ?>" class="form-control" />
						</td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: center">
							<button type="button" class="btn btn-success" id="simpan-kota">
								Simpan
							</button>
							<button type="button" class="btn btn-danger" id="bersih">
								Bersihkan
							</button>
						</td>
					</tr>
				</form>
			</table>
		</div>
        <table>
            <tr>
                <th>Nama Provinsi : <?php echo $Province['name']; ?></th>
            </tr>
        </table>
        <br>
        <table id="city-data-table" class="table table-bordered table-hover">
            <thead>
            <?php $no=1; ?>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Daftar Kota</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($Cities as $index => $city) : ?>
                <tr>
                    <td><?php echo $no;?></td>
                    <td><?php echo $city; ?></td>
                    <?php $no++; ?>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <br>
        <br>

    </section>
</div>

<script>
    $(document).ready(function() {
        $('#bersih').on('click', function() {
            $('#name').val('')
        });

        $('#simpan-kota').on('click', function() {
			var validate = true;

			$("form#form-kota").each(function(){
				console.log($(this)[0].checkValidity());
				if(!$(this)[0].checkValidity())
					validate = false;
			});
			if(!validate){
				Swal.fire({
					title: 'Silahkan isi data terlebih dahulu!',
					text: 'Tidak ada data yang terisi',
					type: 'error',
					confirmButtonText: 'OK'
				});
				return;
			};
            const formData = $("#form-kota").serializeArray();
            let payload = {};
            for (var x = 0; x < formData.length; x++) {
                payload[formData[x].name] = formData[x].value
            }

            $.ajax({
                url: '<?php echo base_url(); ?>Master/addCity',
                type: 'POST',
                data: {
                    payload: JSON.stringify(payload)
                }
            })
            .done(function (data) {
                data = JSON.parse(data)
                if (data.code == '200'){
                    Swal.fire({
                        title: 'Sukses!',
                        text: 'Berhasil Memasukkan Data',
                        type: 'success',
                        confirmButtonText: 'OK',
                        onAfterClose: () => window.location.reload()
                    })
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        text: '[' + data.code + '] ' + data.message,
                        type: 'error',
                        confirmButtonText: 'OK'
                    });
                }
            })
            .fail(function (e) {
                Swal.fire({
                    title: 'Gagal!',
                    text: 'Data Belum Masuk, Silahkan Coba Lagi',
                    type: 'error',
                    confirmButtonText: 'OK'
                });
            });
        })

        $('#city-data-table').DataTable();
    })
</script>
