<div class="content-wrapper">
    <section class="content">
        <h3 style="text-decoration: underline;"><center><strong>Skema Bonus Diskon Untuk Pelanggan</strong></center></h3>
        <br>
        <table>
            <tr>
                <th>Nama Induk : <?php echo $shippingAddress->partner->name;?></th>
            </tr>
            <tr>
                <th></th>
            </tr>
            <tr>
                <th>Nama Pelanggan : <?php echo $shippingAddress['receiver_name'];?></th>
            </tr>
            <tr>
                <th>Alamat Pelanggan : <?php echo $shippingAddress['receiver_address'];?></th>
            </tr>
        </table>
        <br>
        <table class="table table-bordered" id="git myTable">
            <thead>
            <tr>
                <th colspan="2">
                    <center>Pembelian</center>
                </th>
                <th>
                    <center>Diskon</center>
                </th>
                <th>
                    <center>Status</center>
                </th>
            </tr>
            <tr>
                <th scope="col">Nama Barang</th>
                <th scope="col"><center>Minimal</center></th>
                <th scope="col"><center>Diskon (%)</center></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($Schema as $data) : ?>
                <tr>
                    <td><?php echo $data['barang']; ?></td>
                    <td><?php echo $data['minvalue']; ?></td>
                    <td><?php echo $data['discount']; ?></td>
                    <td align="center">
                        <a class="btn btn-danger delete-disc-schema" data-id="<?php echo $data['id']; ?>"
                        ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <br>
        <br>
        <table class="table table-bordered">
            <form id="form-master-schema">
                <input type="hidden" id="m_shippingaddress_id" name="m_shippingaddress_id" value="<?php echo $shippingAddress['id'];?>"/>
                <tr>
                    <td colspan="6">
                        <center><strong>Untuk Pembelian Barang</strong></center>
                    </td>
                </tr>
                <tr>
                    <td width="20%">Nama Barang : <font color="red">*</font></td>
                    <td>
                        <select class="form-control" id="m_product_id" name="m_product_id">
                            <?php
                            foreach ($Products as $product)
                                echo "<option  id='pro_" . $product['id'] . "' value='" . $product['id'] . "'>" . $product['name'] . "</option>";
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="20%">Minimal : <font color="red">*</font></td>
                    <td><input type="number" name="minvalue" id="minvalue" value="" class="form-control" required /></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <center><strong>Akan Mendapat Bonus Diskon </strong></center>
                    </td>
                </tr>
                <tr>
                    <td width="20%">Diskon (%) : <font color="red">*</font></td>
                    <td><input type="number" name="discount" id="discount" value="" class="form-control" required /></td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">
                        <button type="submit" class="btn btn-success" id="simpan-anggota">
                            Simpan
                        </button>
                        <button type="button" class="btn btn-danger" id="bersih">
                            Bersihkan
                        </button>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: right">
                        <a type="button" href="<?php echo base_url(); ?>master/shippingAddress/<?php echo $shippingAddress->partner->id;?>"
                           class="btn btn-warning"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                            Kembali
                        </a>
                    </td>

                </tr>
            </form>
        </table>
    </section>
</div>

<script>
  $(document).ready(function () {
    $("#m_product_id").select2();
    $("#form-master-schema").submit(function (event){
        event.preventDefault();           
        var data = {};    
        $.each($("#form-master-schema").serializeArray(), function(key, value){
            data[value.name] = value.value;
        });
        data['m_product_id'] = $("#m_product_id").val();

        $.ajax({
            url:"<?php echo base_url();?>master/insertDiscountSchema",
            type: "POST",
            datatype: "json",
            data:{ schema: JSON.stringify(data)},
            //success:function(e){}
        })
        .done(function (data) {
            Swal.fire({
                title: 'Sukses!',
                text: 'Berhasil Memasukkan Data',
                type: 'success',
                confirmButtonText: 'OK',
                onAfterClose: () => window.location.reload()
            });
        })
        .fail(function (e) {
            //console.log(e);
            Swal.fire({
                title: 'Gagal!',
                text: 'Data Belum Masuk, Silahkan Coba Lagi',
                type: 'error',
                confirmButtonText: 'OK'
            });
        });
    });

    $(".delete-disc-schema").click(function (event){
        event.preventDefault();           
        var _idSchema = $(this).data('id');
        $.ajax({
            url:"<?php echo base_url();?>master/deleteDiscountSchema",
            type: "POST",
            datatype: "json",
            data:{ idSchema: JSON.stringify(_idSchema)},
            //success:function(e){}
        })
        .done(function (data) {
            Swal.fire({
                title: 'Sukses!',
                text: 'Berhasil Menghapus Data',
                type: 'success',
                confirmButtonText: 'OK',
                onAfterClose: () => window.location.reload()
            });
        })
        .fail(function (e) {
            //console.log(e);
            Swal.fire({
                title: 'Gagal!',
                text: 'Data Belum Terhapus, Silahkan Coba Lagi',
                type: 'error',
                confirmButtonText: 'OK'
            });
        });
    });

    $('#bersih').on('click', function () {
        $("input[name=m_product_id]").val('');
        $('#m_product_id').val('');
        $('#minvalue').val('');
        $('#discount').val('');
    });

  })
</script>