<div class="container-fluid">
    <section class="content">

        <br>
		<div>
			<a class="btn btn-sm bg-gradient-primary" id="addNewProvince" style="color:white;" data-toggle="collapse" data-target="#collapseForm"><span class="fa fa-city"></span> Tambah Provinsi Baru</a>
		</div>
		<br>
		<h3 style="text-decoration: underline;">
			<strong>Daftar Provinsi</strong>
		</h3>
		<div id="collapseForm" class="collapse">
			<table class="table table-bordered ">
				<form id="form-master-province" data-toggle="validator" role="form"3>

					<tr>
						<td width="20%">Nama Provinsi : <font color="red">*</font></td>
						<td><input name="name" id="name" class="form-control uppercase" required/></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: center">
							<button type="button" class="btn btn-success" id="simpan-province">
								Simpan
							</button>
							<button type="button" class="btn btn-danger" id="kembali">
								Bersihkan
							</button>
						</td>
					</tr>
				</form>
			</table>
		</div>
        <table id="province-data-table" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th scope="col">Nama Provinsi</th>
                <th scope="col">Daftar Kota</th>

            </tr>
            </thead>
            <tbody>
            <?php foreach ($province as $data) : ?>
                <tr>
                    <td><?php echo $data['name']; ?></td>
                    <td align="center">
                        <a href="<?php echo base_url(); ?>master/city/<?php echo $data['id']; ?>"
                           class="btn btn-primary"><span class="glyphicon glyphicon-send" aria-hidden="true"></span>
                            Daftar Kota
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <br>
    </section>
</div>

<script>
    $(document).ready(function () {
        $('.uppercase').on('keyup', function () {
            const originalValue = $(this).val();
            $(this).val(originalValue.toUpperCase());
        });

		$("#simpan-province").click(function(){
			$("#form-master-province").submit();
		})
        $('#form-master-province').on('submit', function (e) {
        	e.preventDefault();
			var validate = true;

			$("form#form-master-province").each(function(){
				console.log($(this)[0].checkValidity());
				if(!$(this)[0].checkValidity())
					validate = false;
			});
			if(!validate){
				Swal.fire({
					title: 'Silahkan isi data terlebih dahulu!',
					text: 'Tidak ada data yang terisi',
					type: 'error',
					confirmButtonText: 'OK'
				});
				return;
			};

            const formData = $("#form-master-province").serializeArray();
            let payload = {};
            for (var x = 0; x < formData.length; x++) {
                payload[formData[x].name] = formData[x].value
            }

            $.ajax({
                url: '<?php echo base_url(); ?>/Master/addProvince',
                type: 'POST',
                data: {
                    payload: JSON.stringify(payload)
                }
            })
                .done(function (data) {
                    data = JSON.parse(data)
                    if (data.code == '200'){
                        Swal.fire({
                            title: 'Sukses!',
                            text: 'Berhasil Memasukkan Data',
                            type: 'success',
                            confirmButtonText: 'OK',
                            onAfterClose: () => window.location.reload()
                        })
                    } else {
                        Swal.fire({
                            title: 'Gagal!',
                            text: '[' + data.code + '] ' + data.message,
                            type: 'error',
                            confirmButtonText: 'OK'
                        });
                    }
                })
                .fail(function (e) {
                    Swal.fire({
                        title: 'Gagal!',
                        text: 'Data Belum Masuk, Silahkan Coba Lagi',
                        type: 'error',
                        confirmButtonText: 'OK'
                    });
                });
        })

        $('#province-data-table').DataTable();

        function bersih() {
            $('#name').val('');
        }
    })
</script>
