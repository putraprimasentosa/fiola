<div class="container-fluid">
    <h3 style="text-decoration: underline;">
        <strong>Detil Target</strong>
    </h3>
    <table class="table table-bordered">
        <form id="form-add-line">
            <tr>
                <td><a href="<?php echo base_url('master/target/'); ?>"><span class="fa fa-backward"></span> Bulan</a></td>
                <td colspan="0"><?php echo $target->period; ?><input type="number" id="target_id" name="target_id" value="<?php echo $target->id; ?>" class="form-control" hidden></input></td>

            </tr>
			<tr>
				<td>Sales : <font color="red">*</font>
				</td>
				<td>
					<select class="form-control form-control-sm" id="sales_id" name="sales_id">
						<?php foreach ($sales as $sale) {
							echo '<option value="' . $sale['id'] . '"> ' . $sale['admin_name'].'</option>';
						} ?>
					</select>
				</td>
			</tr>
    </table>
	<h3>
		<strong style="text-decoration: underline;">Tambah Produk</strong>
		<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#copyModal"><span class="fa fa-sm fa-copy"></span> Salin Capaian Penjualan</button>
	</h3>
	<table class="table table-bordered">
		<tr>
			<td>Produk : <font color="red">*</font>
			</td>
			<td>
				<select class="form-control form-control-sm" id="product_id" name="product_id">
					<?php foreach ($product as $pro) {
						echo '<option value="' . $pro['id'] . '"> ' . $pro['product_code'].' - '.$pro['product_name'].'</option>';
					} ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Target Jual : <font color="red">*</font></td>
			<td>
				<input type="number" step="any" class="form-control form-control-sm" id="target_qty" min="1" name="target_qty" required/>
			</td>
		</tr>
		<tr>
			<td colspan="5" style="">
				<button type="button" class="btn btn-sm btn-success" id="form-submit">
					<span class="fa fa-check"></span>
					Simpan
				</button>
			</td>
		</tr>
		</form>
	</table>
    <table class="table table-hovered" id="line-table">
        <thead>
            <th></th>
			<th></th>
            <th>Produk</th>
			<th>Jumlah</th>
			<th>Tindakan</th>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<div class="modal fade" id="copyModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Salin Capaian Penjualan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="form-copy-period">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="exampleFormControlInput1">Silahkan Pilih Bulan</label>
							<select class="form-control" id="periodSelector">
								<?php
									foreach($targetList as $t){
										echo '<option value="'.$t['id'].'">'.$t['period'].'</option>';
									}
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table id="periodTbl" style="width:100%;	" class="table table-hovered">
							<thead>
							<th></th>
							<th></th>
							<th></th>
							<th>Produk</th>
							<th>Jumlah</th>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>
				</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="submitCopy" class="btn btn-primary">Copy</button>
			</div>
		</div>
	</div>
</div><!--.modal-->

<script>
	var detailTable = null;
	var periodTable = null;

    $(document).ready(function() {
        $("#sales_id,#periodSelector").select2({
			width: '100%'
		});
        $("#product_id").select2({
			width: '100%'
		});

        detailTable= $("#line-table").DataTable({
			ajax:{
				url: "<?php echo base_url(); ?>master/getTargetLineSalesProduct",
				type: "GET",
				dataSrc: "",
				data:{
					"sales_id" : function(d){
						return getSalesIDField();
					},
					"target_id" : $("#target_id").val()
				}
			},
			"columns":[{
				data: "product_id"
			}
			,
				{
					data: "sales_id"
				},
				{
					data: "product"
				},
				{
					data: "quantity"
				},
			],
			"columnDefs" : [
				{
					targets: [0,1],
					visible: false
				},
				{
					"targets": 4,
					"data": null,
					"defaultContent": "<button class='btn btn-sm btn-block btn-danger delRow' >Remove</button>"
				}
			]
		});;

		periodTable= $("#periodTbl").DataTable({
			ajax:{
				url: "<?php echo base_url(); ?>master/getTargetLineSalesTarget",
				type: "GET",
				dataSrc: "",
				data:{
					"sales_id" : function(d){
						return getSalesIDField();
					},
					"target_id" : $("#periodSelector").val(),
					"cur_target_id" : $("#target_id").val()
				}
			},
			"columns":[{data: "checker"},{
				data: "product_id"
			}
				,
				{
					data: "sales_id"
				},
				{
					data: "product"
				},
				{
					data: "quantity"
				},
			],
			"columnDefs" : [
				{
					targets: [1,2],
					visible: false
				},
				{
					className: 'select-checkbox',
					targets: 0,
					defaultContent: ''
				}
			],
			select:{
				style: 'multi',
				info:true,
			},
			initComplete: function() {
				periodTable.rows().every(function() {
					$(this.node()).toggleClass('selected');
				})
			}
		});;


    });

    $("#submitCopy").click(function (){
		$("#form-copy-period").submit();
	});

    $("#form-copy-period").submit(function(e){
		e.preventDefault();
		var data = {};
		$.each($("#form-add-line").serializeArray(), function(key, value) {
			data[value.name] = value.value;

		});

		console.log(data);
		$.ajax({
			url: "<?php echo base_url(); ?>master/copyTarget",
			type: "POST",
			datatype: "json",
			data: {
				data: $("#periodTbl").DataTable().rows('.selected').data().toArray(),
				sales_id : $("#sales_id").val(),
				target_id : $("#target_id").val()
			},
		})
				.done(function(e) {
					Swal.fire({
						title: 'Success',
						text: "Data Berhasil Tersimpan !",
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Okay!'
					}).then((result) => {
						if (result.value) {
							location.reload();
						}
					})
				})
				.fail(function(e) {
					//console.log(e);
					Swal.fire({
						title: 'Gagal!',
						text: 'Data Belum Masuk, Pastikan product belum terdaftar pada periode ini ! Silahkan Coba Lagi',
						type: 'error',
						confirmButtonText: 'OK'
					});
				});
	});

    function getSalesIDField(){
    	var sales_id = $("#sales_id").val();
    	return sales_id;
	}
    $("#sales_id").change(function(){
    	loadProductTarget($(this).val());
	});

    $("#periodSelector").change(function(){
    	loadPeriodTbl($("#sales_id").val(), $("#periodSelector").val())
	})

	function loadPeriodTbl(sales_id, target_id){
		console.log(sales_id+" "+target_id);
	}

    function loadProductTarget(sales_id){
		$.ajax({
			url: "<?php echo base_url(); ?>master/getTargetLineSalesProduct",
			type: "GET",
			datatype: "json",
			data: {
				sales_id: sales_id,
				target_id: $("#target_id").val()
			},
		})
			.done(function(a) {
				detailTable.ajax.reload();

			})
			.fail(function(e) {
				//console.log(e);
				Swal.fire({
					title: 'Gagal!',
					text: 'Data Belum Masuk, Pastikan product belum terdaftar pada periode ini ! Silahkan Coba Lagi',
					type: 'error',
					confirmButtonText: 'OK'
				});
			});

	}


    $("#form-submit").click(function() {
        $("#form-add-line").submit();
    });
    $("#form-add-line").submit(function(e) {
        e.preventDefault();
		let validate = true;
		$("form#form-add-line").each(function(){
			console.log($(this)[0].checkValidity());
			if(!$(this)[0].checkValidity())
				validate = false;
		});
		if(!validate){
			Swal.fire({
				title: 'Silahkan isi data terlebih dahulu!',
				text: 'Tidak ada data yang terisi',
				type: 'error',
				confirmButtonText: 'OK'
			});
			return;
		};
        var data = {};
        $.each($("#form-add-line").serializeArray(), function(key, value) {
            data[value.name] = value.value;

        });

        console.log(data);
        $.ajax({
                url: "<?php echo base_url(); ?>master/insertTargetLine",
                type: "POST",
                datatype: "json",
                data: {
                    target: JSON.stringify(data)
                },
            })
            .done(function(e) {
				$("#line-table").DataTable().row.add({
					"product_id" : data.product_id,
					"sales_id" :data.sales_id,
					"product" : $("#product_id").select2('data')[0].text.split(" - ")[1],
					"quantity" : data.target_qty
				}).draw();
                Swal.fire({
                    title: 'Sukses!',
                    text: 'Berhasil Memasukkan Data',
                    type: 'success',
                    confirmButtonText: 'OK',

                });
            })
            .fail(function(e) {
                //console.log(e);
                Swal.fire({
                    title: 'Gagal!',
                    text: 'Data Belum Masuk, Pastikan product belum terdaftar pada periode ini ! Silahkan Coba Lagi',
                    type: 'error',
                    confirmButtonText: 'OK'
                });
            });
    });

    $("#line-table").on('click', 'button.delRow', function(e) {
        Swal.fire({
            title: 'Yakin?',
            text: "Data yang telah dihapus tidak dapat dikembalikan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                var data = $("#line-table").DataTable().row($(this).parents('tr')).data();
                console.log("-----");
                console.log(data);
				console.log("-----");
                $("#line-table").DataTable().row($(this).parents('tr')).remove().draw();
                $.ajax({
                        url: "<?php echo base_url() ?>/master/delTargetLine",
                        type: "POST",
                        data: {
                            product: data.product_id,
                            sales_id: data.sales_id,
							target_id: $("#target_id").val()
                        }
                    })
                    .done(function(addressRawData) {
                        Swal.fire(
                            'Deleted!',
                            'Data tidak bisa dikembalikan.',
                            'success'
                        )
                    })
                    .fail(function() {
                        Swal.fire(
                            'Cancelled',
                            'Penghapusan data gagal.',
                            'error'
                        )
                    });

            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire(
                    'Cancelled',
                    'Penghapusan data dibatalkan.',
                    'error'
                )
            }
        });
    });
</script>
