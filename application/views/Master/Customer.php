<div class="container-fluid">
    <a class="btn btn-sm bg-gradient-primary" id="addNewAdmin" style="color:white;" data-toggle="collapse" data-target="#collapseForm"><span class="fa fa-user"></span> Tambah Pelanggan Baru</a>
    <hr />
    <h3 style="text-decoration: underline;">
        <strong>Daftar Pelanggan</strong>
    </h3>
    <div id="collapseForm" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <table class="table table-bordered align-center">
            <form id="form-add-customer">
                <tr>
                    <td>Nama Pelanggan : <font color="red">*</font></td>
                    <td colspan="10"><input name="customer_name" id="Customer_name" placeholder="" class="form-control" required /></td>
                </tr>
                <tr>
                    <td>Alamat Pelanggan :
                    </td>
                    <td colspan="10"><textarea name="customer_address" id="customer_address" placeholder=""  class="form-control" required></textarea></td>
                </tr>
                <tr>
                    <td>Provinsi : <font color="red">*</font></td>
                    <td colspan="5">
                        <select name="m_province_id" id="m_province_id" class="form-control" required>
                            <option value="" disabled selected>Pilih Provinsi</option>
                            <?php foreach ($Province as $row) {
                                echo "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
                            }
                            ?>
                        </select>
                    </td>
                    <td>Kota : <font color="red">*</font></td>
                    <td colspan="5">
                        <select name="m_city_id" id="m_city_id" class="form-control" required>
                            <option value="" disabled selected>Pilih Kota</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Kontak : <font color="red">*</font>
                    </td>
                    <td colspan="5"><input name="customer_contact" id="customer_contact" placeholder="" class="form-control" required /></td>
                    <td>E-Mail : <font color="red">*</font>
                    </td>
                    <td colspan="5"><input type="email" name="customer_email" id="customer_email" placeholder="" class="form-control" required /></td>
                </tr>
                <tr>
                    <td colspan="10" style="text-align: center">
                        <button type="submit" class="btn btn-sm btn-success" id="form-submit">
                            <span class="fa fa-check"></span>
                            Simpan
                        </button>
                        <button type="button"  class="btn btn-sm btn-danger" id="bersih">
                            <span class="fa fa-ban"></span>
                            Batal
                        </button>
                    </td>
                </tr>
            </form>
        </table>
        <hr>
    </div>
    <!-- Form Update -->
    <div id="collapseFormUpdate" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <table class="table table-bordered align-center">
            <form id="form-update-customer">
                <tr>
                    <td>Kode Pelanggan : <font color="red">*</font></td>
                    <td colspan="10"><input  id="customer_id"  class="form-control" required readonly /></td>
                </tr>
                <tr>
                    <td>Nama Pelanggan : <font color="red">*</font></td>
                    <td colspan="10"><input name="customer_name" id="ucustomer_name" placeholder="Customer Name" class="form-control" required /></td>
                </tr>
                <tr>
                    <td>Alamat Pelanggan :
                    </td>
                    <td colspan="10"><textarea name="customer_address" id="ucustomer_address" placeholder="Customer Address"  class="form-control" required></textarea></td>
                </tr>
                <tr>
                    <td>Provinsi : <font color="red">*</font></td>
                    <td colspan="5">
                        <select name="m_province_id" id="um_province_id" class="form-control" required>
                            <option value="" disabled selected>Pilih Provinsi</option>
                            <?php foreach ($Province as $row) {
                                echo "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
                            }
                            ?>
                        </select>
                    </td>
                    <td>Kota : <font color="red">*</font></td>
                    <td colspan="5">
                        <select name="m_city_id" id="um_city_id" class="form-control" required>
                            <option value="" disabled selected>Pilih Kota</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Kontak : <font color="red">*</font>
                    </td>
                    <td colspan="5"><input name="customer_contact" id="ucustomer_contact" placeholder="Customer Contact" class="form-control" required /></td>
                    <td>E-Mail : <font color="red">*</font>
                    </td>
                    <td colspan="5"><input type="email" name="customer_email" id="ucustomer_email" placeholder="Customer Email" class="form-control" required /></td>
                </tr>
                <tr>
                    <td colspan="10" style="text-align: center">
                        <button type="submit" class="btn btn-sm btn-success" id="form-submit">
                            <span class="fa fa-check"></span>
                            Simpan
                        </button>
                        <button type="button"  class="btn btn-sm btn-danger" id="bersih">
                            <span class="fa fa-ban"></span>
                            Batal
                        </button>
                    </td>
                </tr>
            </form>
        </table>
        <hr>
    </div>
    <!-- End Form Update -->
    <br>
    <table id="customer-data-table" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th scope="col">Kode Pelanggan</th>
                <th scope="col">Nama Pelanggan</th>
                <th scope="col">Alamat Pelanggan</th>
                <th></th>
                <th scope="col">Provinsi</th>
                <th></th>
                <th scope="col">Kota</th>
                <th scope="col">Kontak</th>
                <th scope="col">E-Mail</th>
                <th>Tindakan</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($Customer as $data) : ?>
                <tr>
                    <td><?php echo $data['id']; ?></td>
                    <td><?php echo $data['customer_name']; ?></td>
                    <td><?php echo $data['customer_address']; ?></td>
                    <td><?php echo $data['m_province_id']; ?></td>
                    <td><?php echo $data['province']; ?></td>
                    <td><?php echo $data['m_city_id']; ?></td>
                    <td><?php echo $data['city']; ?></td>
                    <td><?php echo $data['customer_contact']; ?></td>
                    <td><?php echo $data['customer_email']; ?></td>
                    <td></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <br>
</div>

<script>
    var dTable = null;
    var cityUpdate = null;
    $(document).ready(function() {
        dTable = $('#customer-data-table').DataTable({
            "columnDefs": [{
                "targets": 9,
                "data": null,
                "defaultContent": `
                            <button type='button' class='btn btn-sm btn-block btn-danger delRow' ><span class="fa fa-window-close"></span> Hapus</button>
                            <button type='button' class='btn btn-sm btn-block btn-info editRow'><span class="fa fa-pen-alt"></span>  Ubah</button>
                            `
            }, {
                "targets": [3, 5],
                "visible": false
            }]

        });
        $("#customer-data-table").DataTable();
    });

    $("#customer-data-table").on('click', 'button.editRow', function(e) {
        console.log(dTable.row($(this).parents('tr')).data());
        var data = dTable.row($(this).parents('tr')).data();
        $("#customer_id").val(data[0]);
        $("#ucustomer_name").val(data[1]);
        $("#ucustomer_address").val(data[2]);

        $("#um_city_id").val(data[5]);
        cityUpdate = data[5];
        $("#um_province_id").val(data[3]).trigger('change');
        $("#ucustomer_contact").val(data[7]);
        $("#ucustomer_email").val(data[8]);
        $("#collapseFormUpdate").collapse('show');
        $("#collapseForm").collapse('hide');
    });

	$("#customer-data-table").on('click', 'button.delRow', function(e) {
		Swal.fire({
			title: 'Yakin?',
			text: "Data yang telah dihapus tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes, delete it!',
			cancelButtonText: 'No, cancel!',
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				var data = dTable.row($(this).parents('tr')).data();
				dTable.row($(this).parents('tr')).remove().draw();
				$.ajax({
					url: "<?php echo base_url() ?>/master/delCustomer",
					type: "POST",
					data: {
						id: data[0],
					}
				})
					.done(function(addressRawData) {
						Swal.fire(
							'Deleted!',
							'Data tidak bisa dikembalikan.',
							'success'
						)
					})
					.fail(function() {
						Swal.fire(
							'Cancelled',
							'Penghapusan data gagal.',
							'error'
						)
					});

			} else if (
				/* Read more about handling dismissals below */
				result.dismiss === Swal.DismissReason.cancel
			) {
				Swal.fire(
					'Cancelled',
					'Penghapusan data dibatalkan.',
					'error'
				)
			}
		});
	});

    $("#form-add-customer").submit(function (event){
        event.preventDefault();
        console.log($.isNumeric($("#customer_contact").val()));
		if(!$.isNumeric($("#customer_contact").val())){
			Swal.fire({
				title: 'Gagal!',
				text: 'Silahkan isi nomor kontak dengan menggunakan angka',
				type: 'error',
				confirmButtonText: 'OK'
			});
			return;
		}
        var data = {};
        $.each($("#form-add-customer").serializeArray(), function(key, value){
            data[value.name] = value.value;
        });
        $.ajax({
            url:"<?php echo base_url();?>master/insertCustomer",
            type: "POST",
            datatype: "json",
            data:{ partner: JSON.stringify(data)},
            //success:function(e){}
        })
            .done(function (data) {
                Swal.fire({
                    title: 'Sukses!',
                    text: 'Berhasil Memasukkan Data',
                    type: 'success',
                    confirmButtonText: 'OK',
                    onAfterClose: () => window.location.reload()
                });
            })
            .fail(function (e) {
                Swal.fire({
                    title: 'Gagal!',
                    text: 'Data Belum Masuk, Silahkan Coba Lagi',
                    type: 'error',
                    confirmButtonText: 'OK'
                });
            });
    });

    $("#form-update-customer").submit(function(e) {
        e.preventDefault();
        var data = {};
        $.each($("#form-update-customer").serializeArray(), function(key, value) {
            data[value.name] = value.value;
        });
        //console.log($("#uid").val());
        $.ajax({
            url: "<?php echo base_url(); ?>master/updateCustomer",
            type: "POST",
            datatype: "json",
            data: {
                id: $("#customer_id").val(),
                partner: JSON.stringify(data)
            },
            //success:function(e){}
        })
            .done(function(data) {
                Swal.fire({
                    title: 'Sukses!',
                    text: 'Berhasil Mengubah Data',
                    type: 'success',
                    confirmButtonText: 'OK',
                    onAfterClose: () => window.location.reload()
                });
            })
            .fail(function(e) {
                //console.log(e);
                Swal.fire({
                    title: 'Gagal!',
                    text: 'Data gagal diubah, silahkan coba lagi!',
                    type: 'error',
                    confirmButtonText: 'OK'
                });
            });
    });

    $('#m_province_id').on('change', function () {
        $.ajax({
            url: "<?php echo base_url()?>MasterApi/getCity",
            type: 'POST',
            data: {
                provinceId: $(this).val()
            }
        })
            .done(function (result) {
                let cityElements = $('#m_city_id');
                const cities = jQuery.parseJSON(result);

                cityElements.html(`
            <option value="">Pilih Kota</option>
        `);
                for (let x = 0; x < cities.length; x++) {
                    cityElements.append(`
            <option value="${cities[x].id}">${cities[x].name}</option>
          `);
                }
            })
            .fail(function (error) {
                console.log(error);
            });
    });

    $('#um_province_id').on('change', function () {
        $.ajax({
            url: "<?php echo base_url()?>MasterApi/getCity",
            type: 'POST',
            data: {
                provinceId: $(this).val()
            }
        })
            .done(function (result) {
                let cityElements = $('#um_city_id');
                const cities = jQuery.parseJSON(result);

                cityElements.html(`
            <option value="">Pilih Kota</option>
        `);
                for (let x = 0; x < cities.length; x++) {
                    cityElements.append(`
            <option value="${cities[x].id}" ${cityUpdate==cities[x].id ? "selected" : ""}>${cities[x].name}</option>
          `);
                }
            })
            .fail(function (error) {
                console.log(error);
            });
    });

    $('#bersih').on('click', function () {
        $('#customer_name').val('');
        $('#customer_address').val('');
        $('#m_province_id').val('');
        $('#m_city_id').empty();
        $('#customer_contact').val('');
        $('#customer_email').val('');
        $("#collapseForm").collapse('hide');
    });

    $("#form-update-submit").click(function() {
        $("#form-update-customer").submit();
    });

</script>
