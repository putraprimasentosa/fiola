<div class="container-fluid">
    <a class="btn btn-sm bg-gradient-primary" id="addNewAdmin" style="color:white;" data-toggle="collapse" data-target="#collapseForm"><span class="fa fa-truck"></span> Tambah Ekspedisi Baru</a>
    <hr />
    <h3 style="text-decoration: underline;">
        <strong>Daftar Ekspedisi</strong>
    </h3>
    <div id="collapseForm" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <table class="table table-bordered align-center">
            <form id="form-add-admin">
                <tr>
                    <td>Kode Ekspedisi : <font color="red">*</font>
                    </td>
                    <td colspan="5"><input name="courier_code" id="name" value="" class="form-control" required /></td>
                    <td>Nama Ekspedisi : <font color="red">*</font>
                    </td>
                    <td colspan="5"><input name="courier_name" id="contact" value="" class="form-control" required /></td>
                </tr>
                <tr>

                </tr>

                <tr>
                    <td colspan="10" style="text-align: center">
                        <button type="button" class="btn btn-sm btn-success" id="form-submit">
                            <span class="fa fa-check"></span>
                            Simpan
                        </button>
                        <button type="reset" class="btn btn-sm btn-danger" id="form-cancel">
                            <span class="fa fa-ban"></span>
                            Batal
                        </button>
                    </td>
                </tr>
            </form>
        </table>
        <hr>
    </div>

    <table id="courier-data-table" class="table table-bordered">
        <thead>
            <tr>
                <th></th>
                <th>Kode Ekspedisi</th>
                <th>Nama Ekspedisi</th>
                <th>Tindakan</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($Courier  as $data) : ?>
                <tr>
                    <td><?php echo $data['id'] ?></td>
                    <td><?php echo $data['courier_code'] ?></td>
                    <td><?php echo $data['courier_name'] ?></td>
                    <td><button type="button" class="btn btn-danger btn-block delRow"><span class="fa fa-ban"></span> Hapus</button></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script>
    var dTable = null;
    $(document).ready(function() {
        dTable = $("#courier-data-table").DataTable({
            "columnDefs": [{

                "targets": [0],
                "visible": false
            }]
        });
    })

    $("#form-submit").click(function(e) {
        $("#form-add-admin").submit();
    });
    $("#form-add-admin").submit(function(event) {
        event.preventDefault();
		var validate = true;

		$("form#form-add-admin").each(function(){
			if(!$(this)[0].checkValidity())
				validate = false;
		});
		if(!validate){
			Swal.fire({
				title: 'Silahkan isi data terlebih dahulu!',
				text: 'Ada data yang belum terisi',
				type: 'error',
				confirmButtonText: 'OK'
			});
			return;
		};
        var data = {};
        $.each($("#form-add-admin").serializeArray(), function(key, value) {
            data[value.name] = value.value;
        });
        console.log(data);
        $.ajax({
                url: "<?php echo base_url(); ?>master/insertCourier",
                type: "POST",
                datatype: "json",
                data: {
                    courier: JSON.stringify(data)
                },
                //success:function(e){}
            })
            .done(function(data) {
                Swal.fire({
                    title: 'Sukses!',
                    text: 'Berhasil Memasukkan Data',
                    type: 'success',
                    confirmButtonText: 'OK',
                    onAfterClose: () => window.location.reload()
                });
            })
            .fail(function(e) {
                //console.log(e);
                Swal.fire({
                    title: 'Gagal!',
                    text: 'Data Belum Masuk, Silahkan Coba Lagi',
                    type: 'error',
                    confirmButtonText: 'OK'
                });
            });
    });
    $("#courier-data-table").on('click', 'button.delRow', function(e) {
        Swal.fire({
            title: 'Yakin?',
            text: "Data yang telah dihapus tidak dapat dikembalikan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                var data = dTable.row($(this).parents('tr')).data();
                dTable.row($(this).parents('tr')).remove().draw();
                $.ajax({
                        url: "<?php echo base_url() ?>/master/delCourier",
                        type: "POST",
                        data: {
                            id: data[0],
                        }
                    })
                    .done(function(addressRawData) {
                        Swal.fire(
                            'Deleted!',
                            'Data tidak bisa dikembalikan.',
                            'success'
                        )
                    })
                    .fail(function() {
                        Swal.fire(
                            'Cancelled',
                            'Penghapusan data gagal.',
                            'error'
                        )
                    });

            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire(
                    'Cancelled',
                    'Penghapusan data dibatalkan.',
                    'error'
                )
            }
        });
    });
</script>
