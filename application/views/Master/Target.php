<div class="container-fluid">
    <h3 style="text-decoration: underline;">
        <strong>Target Penjualan</strong>
    </h3>
    <hr>
    <table class="table table-bordered">
        <form id="form-add-target">
            <tr>
                <td>Bulan : <font color="red">*</font>
                </td>
                <td colspan="5">
                    <select id="month" name="month" class="form-control form-control-sm">
                        <option value="JANUARY">January</option>
                        <option value="FEB">February</option>
                        <option value="MAR">March</option>
                        <option value="APR">April</option>
                        <option value="MAY">May</option>
                        <option value="JUN">June</option>
                        <option value="JUL">July</option>
                        <option value="AUG">August</option>
                        <option value="SEP">September</option>
                        <option value="OCT">October</option>
                        <option value="NOV">November</option>
                        <option value="DEC">December</option>
                    </select>
                </td>
                <td>Tahun : <font color="red">*</font>
                </td>
                <td colspan="5"><input type="number" id="year" name="year" value="" class="form-control form-control-sm" required /></td>
            </tr>

            <tr>
                <td colspan="10" style="text-align: center">
                    <button type="button" class="btn btn-sm btn-success" id="form-submit">
                        <span class="fa fa-check"></span>
                        Simpan
                    </button>
                    <button type="reset" class="btn btn-sm btn-danger" id="form-cancel">
                        <span class="fa fa-ban"></span>
                        Batal
                    </button>
                </td>
            </tr>
        </form>
    </table>
    <hr>
    <table class="table table-hovered" id="target-data-table">
        <thead>
            <th>Bulan</tH>
            <th>Tanggal Mulai</tH>
            <th>Tanggal Selesai</tH>
            <th>Tindakan</tH>
        </thead>
        <tbody>
            <?php foreach ($target as $data) : ?>
                <tr>
                    <td><?php echo $data['period'] ?></td>
                    <td><?php echo $data['validfrom'] ?></td>
                    <td><?php echo $data['validto'] ?></td>
                    <td>
                        <a id="targetLine" class="btn btn-info btn-sm btn-block deactive" style="color:white;" href="<?php echo base_url('/master/TargetLine/'.$data['id']);?>" data-id="<?php echo $data['id']; ?>"><span class="fa fa-arrow-circle-down"></span> Lihat</button>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<script>
    
    $(document).ready(function() {
        $("#target-data-table").DataTable({
			"order": [[ 2	, "desc" ]]
		});
    });

    $("#form-submit").click(function() {
        $("#form-add-target").submit();
    });

    $("#form-add-target").submit(function(e) {
        e.preventDefault();
		var validate = true;

		$("form#form-add-target").each(function(){
			if(!$(this)[0].checkValidity())
				validate = false;
		});
		if(!validate){
			Swal.fire({
				title: 'Silahkan isi data terlebih dahulu!',
				text: 'Tidak ada data yang terisi',
				type: 'error',
				confirmButtonText: 'OK'
			});
			return;
		};
        var data = {};
        $.each($("#form-add-target").serializeArray(), function(key, value) {
            data[value.name] = value.value;

        });

        console.log(data);
        $.ajax({
                url: "<?php echo base_url(); ?>master/insertTarget",
                type: "POST",
                datatype: "json",
                data: {
                    target: JSON.stringify(data)
                },
            })
            .done(function(data) {
                Swal.fire({
                    title: 'Sukses!',
                    text: 'Berhasil Memasukkan Data',
                    type: 'success',
                    confirmButtonText: 'OK',
                    onAfterClose: () => window.location.reload()
                });
            })
            .fail(function(e) {
                //console.log(e);
                Swal.fire({
                    title: 'Gagal!',
                    text: 'Data Belum Masuk, Silahkan Coba Lagi',
                    type: 'error',
                    confirmButtonText: 'OK'
                });
            });
    });
</script>
