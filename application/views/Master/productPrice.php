<div class="content-wrapper">
    <section class="content">
        <h3 style="text-decoration: underline;"><center><strong>Harga Jual Barang</strong></center></h3>
        <br>
        <table>
            <tr>
                <th>Nama Pelanggan : <?php echo $Customer['name'];?></th>
            </tr>
            <br>
            <tr>
                <th>Kode Pelanggan : <?php echo $Customer['id'];?></th>
            </tr>
        </table>
        <br>
        <table class="table table-bordered" id="git myTable">
            <thead>
            <tr>
                <th>
                    <center>Produk</center>
                </th>
                <th>
                    <center>Harga Jual</center>
                </th>
                <th>
                    <center>Harga Beli</center>
                </th>
                <th>
                    <center>Valid dari Tgl</center>
                </th>
                <th>
                    <center>Status</center>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($Price as $data) : ?>
                <tr>
                    <td><?php echo $data['barang']; ?></td>
                    <td><?php echo $data['pricesell']; ?></td>
                    <td><?php echo $data['pricebuy']; ?></td>
                    <td><?php echo $data['validfrom']; ?></td>
                    <td align="center">
                        <a class="btn btn-danger delete-product-price" data-id="<?php echo $data['id']; ?>"
                        ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <br>
        <br>
        <table class="table table-bordered">
            <form id="form-master-price">
                <input type="hidden" id="m_partner_id" name="m_partner_id" value="<?php echo $Customer['id'];?>"/>
                <tr>
                    <td colspan="6">
                        <center><strong>Untuk Pembelian Barang</strong></center>
                    </td>
                </tr>
                <tr>
                    <td width="20%">Nama Barang : <font color="red">*</font></td>
                    <td>
                        <select class="form-control" id="m_product_id" name="m_product_id">
                            <?php
                            foreach ($Products as $product)
                                echo "<option  id='pro_" . $product['id'] . "' value='" . $product['id'] . "'>" . $product['name'] . "</option>";
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="20%">Harga Jual: <font color="red">*</font></td>
                    <td><input type="text" name="pricesell" id="pricesell" value="" placeholder ="0.00" class="form-control" required /></td>
                </tr>
                <tr>
                    <td width="20%">Harga Beli: <font color="red">*</font></td>
                    <td><input type="text" name="pricebuy" id="pricebuy" value="" placeholder ="0.00" class="form-control" required /></td>
                </tr>
                <tr>
                    <td width="20%">Valid Dari Tanggal: </td>
                    <td>
                        <input name="validfrom" id="validfrom" class="form-control" required />
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center">
                        <button type="submit" class="btn btn-success" id="simpan-anggota">
                            Simpan
                        </button>
                        <button type="button" class="btn btn-danger" id="bersih">
                            Bersihkan
                        </button>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: right">
                        <a href="<?php echo base_url(); ?>master/masterCustomer"
                           class="btn btn-warning"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                            Kembali
                        </a>
                    </td>

                </tr>
            </form>
        </table>
    </section>
</div>

<script type="text/javascript" src="<?php echo getNodeUrl('bootstrap-datepicker/dist/js/bootstrap-datepicker.js'); ?>"></script>
<script>
  $(document).ready(function () {
    $("#validfrom").datepicker({
        autoclose: true
    }).datepicker("setDate", new Date());

    $("#m_product_id").combobox();
    $("#form-master-price").submit(function (event){
        event.preventDefault();           
        var data = {};    
        $.each($("#form-master-price").serializeArray(), function(key, value){
            data[value.name] = value.value;
        });
        data['m_product_id'] = $("#m_product_id").val();

        $.ajax({
            url:"<?php echo base_url();?>master/insertProductPrice",
            type: "POST",
            datatype: "json",
            data:{ productPrice: JSON.stringify(data)},
            //success:function(e){}
        })
        .done(function (data) {
            Swal.fire({
                title: 'Sukses!',
                text: 'Berhasil Memasukkan Data',
                type: 'success',
                confirmButtonText: 'OK',
                onAfterClose: () => window.location.reload()
            });
        })
        .fail(function (e) {
            //console.log(e);
            Swal.fire({
                title: 'Gagal!',
                text: 'Data Belum Masuk, Silahkan Coba Lagi',
                type: 'error',
                confirmButtonText: 'OK'
            });
        });
    });

    $(".delete-product-price").click(function (event){
        event.preventDefault();           
        var _idProductPrice = $(this).data('id');
        $.ajax({
            url:"<?php echo base_url();?>master/deleteProductPrice",
            type: "POST",
            datatype: "json",
            data:{ idProductPrice: JSON.stringify(_idProductPrice)},
            //success:function(e){}
        })
        .done(function (data) {
            Swal.fire({
                title: 'Sukses!',
                text: 'Berhasil Menghapus Data',
                type: 'success',
                confirmButtonText: 'OK',
                onAfterClose: () => window.location.reload()
            });
        })
        .fail(function (e) {
            //console.log(e);
            Swal.fire({
                title: 'Gagal!',
                text: 'Data Belum Terhapus, Silahkan Coba Lagi',
                type: 'error',
                confirmButtonText: 'OK'
            });
        });
    });

    $('#bersih').on('click', function () {
        $("input[name=m_product_id]").val('');
        $('#m_product_id').val('');
        $('#pricesell').val('');
        $('#pricebuy').val('');
        $('#validfrom').val('');
    });

  })
</script>