<div class="content-wrapper">
    <section class="content">
        <h2 class="text-center margin-bottom"><?php echo $title; ?></h2>
        <form id="form-add-customer">
            <div class="row">
                <div class="row form-group col-md-6">
                    <label for="code" class="col-sm-2 control-label">Kode <span class="text-red">*</span></label>
                    <div class="col-sm-10">
                        <input name="code" id="code" class="form-control hidden" value="<?php echo $Code; ?>" />
                        <input class="form-control" value="<?php echo $Code; ?>" disabled />
                    </div>
                </div>
                <div class="row form-group col-md-6">
                    <label for="credit_limit" class="col-sm-4 control-label">Limit Jumlah Piutang</label>
                    <div class="input-group col-sm-8">
                        <div class="input-group-addon">Rp</div>
                        <input type="number" name="credit_limit" id="credit_limit"
                               class="form-control" value="0"
                        />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row form-group col-md-6">
                    <label for="name" class="col-sm-2 control-label">Nama <span class="text-red">*</span></label>
                    <div class="col-sm-10">
                        <input name="name" id="name" class="form-control" required />
                    </div>
                </div>
                <div class="row form-group col-md-6">
                    <label for="daily_credit_limit" class="col-sm-4 control-label">Limit Piutang Harian</label>
                    <div class="input-group col-sm-8">
                        <input type="number" name="daily_credit_limit" id="daily_credit_limit"
                               class="form-control" value="0"  />
                        <div class="input-group-addon">Hari</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row form-group col-md-6">
                    <label for="taxid" class="col-sm-2 control-label">NPWP</label>
                    <div class="col-sm-10">
                        <input name="taxid" id="taxid" class="form-control" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row form-group col-md-6">
                    <label for="address" class="col-sm-2 control-label">Alamat <span class="text-red">*</span></label>
                    <div class="col-sm-10">
                        <textarea name="address" id="address" class="form-control" required></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row form-group col-md-6">
                    <label for="m_province_id" class="col-sm-2 control-label">Provinsi <span
                                class="text-red">*</span></label>
                    <div class="col-sm-10">
                        <select name="m_province_id" id="m_province_id" class="form-control" required>
                            <option value="" disabled selected>-- Pilih Propinsi --</option>
                          <?php foreach ($Province as $row) {
                            echo "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
                          }
                          ?>
                        </select>
                    </div>
                </div>
                <div class="row form-group col-md-6">
                    <label for="m_city_id" class="col-sm-4 control-label">Kota <span class="text-red">*</span></label>
                    <div class="input-group col-sm-8">
                        <select name="m_city_id" id="m_city_id" class="form-control" required>
                            <option value="" disabled selected>-- Pilih Kota --</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row form-group col-md-6">
                    <label for="contactno" class="col-sm-2 control-label">Telepon <span class="text-red">*</span></label>
                    <div class="col-sm-10">
                        <input name="contactno" id="contactno" class="form-control" />
                    </div>
                </div>
                <div class="row form-group col-md-6">
                    <label for="faxno" class="col-sm-4 control-label">Fax <span class="text-red">*</span></label>
                    <div class="input-group col-sm-8">
                        <input name="faxno" id="faxno" class="form-control"  />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row form-group col-md-6">
                    <label for="email" class="col-sm-2 control-label">
                        Email <span class="text-red">*</span>
                    </label>
                    <div class="col-sm-10">
                        <input type="email" name="email" id="email" class="form-control" />
                    </div>
                </div>
                <div class="row form-group col-md-6">
                    <label for="name2" class="col-sm-4 control-label">
                        Kontak <span class="text-red">*</span>
                    </label>
                    <div class="input-group col-sm-8">
                        <input name="name2" id="name2" class="form-control" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row form-group col-md-6">
                    <label for="bank_no" class="col-sm-2 control-label">
                        No Rek. <span class="text-red">*</span>
                    </label>
                    <div class="col-sm-10">
                        <input type="number" name="bank_no" id="bank_no"
                               class="form-control"
                        />
                    </div>
                </div>
                <div class="row form-group col-md-6">
                    <label for="bank_name" class="col-sm-4 control-label">
                        Rek. A/N <span class="text-red">*</span>
                    </label>
                    <div class="input-group col-sm-8">
                        <input name="bank_name" id="bank_name"
                               class="form-control"
                        />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row form-group col-md-6">
                    <label for="nama_pkp" class="col-sm-2 control-label">
                        Nama PKP
                    </label>
                    <div class="col-sm-10">
                        <input type="text" name="nama_pkp" id="nama_pkp" class="form-control"/>
                    </div>
                </div>
                <div class="row form-group col-md-6">
                    <label for="alamat_pkp" class="col-sm-4 control-label">
                        Alamat PKP 
                    </label>
                    <div class="input-group col-sm-8">
                        <textarea name="alamat_pkp" id="alamat_pkp" class="form-control"></textarea>
                    </div>
                </div>
            </div>

            <input type="text" name="created_at" id="created_at"  value="<?php echo date("m/d/Y");?>" hidden>
            <input type="text" name="updated_at" id="updated_at" value="<?php echo date("m/d/Y");?>" hidden>

            <div class="row" style="margin-top: 20px;">
                <div class="col-md-6">
                    <a href="<?php echo base_url(); ?>master/masterCustomer"
                       class="btn btn-warning"
                    >
                        <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                        Kembali
                    </a>
                </div>
                <div class="col-md-6 text-right" style="padding-right: 60px">
                    <button type="submit" class="btn btn-success" id="simpan-anggota">
                        Simpan
                    </button>
                    <button type="button" class="btn btn-danger" id="bersih">
                        Bersihkan
                    </button>
                </div>
            </div>
        </form>
    </section>
</div>

<script>
  $(document).ready(function () {
    $('#m_province_id').on('change', function () {
      $.ajax({
        url: "<?php echo base_url()?>MasterApi/getCity",
        type: 'POST',
        data: {
          provinceId: $(this).val()
        }
      })
      .done(function (result) {
        let cityElements = $('#m_city_id');
        const cities = jQuery.parseJSON(result);

        cityElements.html(`
            <option value="">-- Pilih Kota --</option>
        `);
        for (let x = 0; x < cities.length; x++) {
          cityElements.append(`
            <option value="${cities[x].id}">${cities[x].name}</option>
          `);
        }
      })
      .fail(function (error) {
        console.log(error);
      });
    });

    // $('#clone-address-toggle').on('click', function () {
    //   const checked = $(this).prop('checked');

    //   if (checked) {
    //     $('#receiver-name').val($('#name').val());
    //     $('#receiver-address').val($('#address').val());
    //     $('#receiver-province').val($('#province').val());
    //     $('#receiver-city').val($('#city').val());
    //   }
    // });

    $("#form-add-customer").submit(function (event){
        event.preventDefault();
        alert('aaa');
        var data = {};
            $.each($("#form-add-customer").serializeArray(), function(key, value){
            data[value.name] = value.value;
        });
        $.ajax({
            url:"<?php echo base_url();?>master/insertCustomer",
            type: "POST",
            datatype: "json",
            data:{ partner: JSON.stringify(data)},
            //success:function(e){}
        })
        .done(function (data) {
            Swal.fire({
                title: 'Sukses!',
                text: 'Berhasil Memasukkan Data',
                type: 'success',
                confirmButtonText: 'OK',
                onAfterClose: () => window.location.reload()
            });
        })
        .fail(function (e) {
            //console.log(e);
            Swal.fire({
                title: 'Gagal!',
                text: 'Data Belum Masuk, Silahkan Coba Lagi',
                type: 'error',
                confirmButtonText: 'OK'
            });
        });
    });

    $('#bersih').on('click', function () {
        $('#credit_limit').val('');
        $('#daily_credit_limit').val('');
        $('#taxid').val('');
        $('#address').val('');
        $('#m_province_id').val('');
        $('#m_city_id').empty();
        $('#contactno').val('');
        $('#faxno').val('');
        $('#email').val('');
        $('#name2').val('');
        $('#bank_no').val('');
        $('#bank_name').val('');        
    });

  })
</script>
