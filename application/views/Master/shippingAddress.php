<style>
	.title {
		text-decoration: underline;
		text-align: center;
		font-weight: bold;
	}
</style>

<div class="content-wrapper">
	<section class="content">
		<h3 class="title">
			Daftar Alamat Kirim Pelanggan
		</h3>
		<br>
		<table>
			<tr>
				<th>Nama Pelanggan : <?php echo $Customer['name']; ?></th>
			</tr>
			<tr>
				<th>Kode Pelanggan : <?php echo $Customer['id']; ?></th>
			</tr>
		</table>
		<br>
		<table id="shippingaddress-data-table" class="table table-bordered table-hover" style="width: 100%">
			<thead>
			<?php $no = 1; ?>
			<tr>
				<th scope="col">No</th>
                <th scope="col">Kode</th>
				<th scope="col">Nama Penerima</th>
				<th scope="col">Alamat Penerima</th>
				<th scope="col">Provinsi</th>
				<th scope="col">Kota</th>
				<th scope="col">Skema</th>
				<th scope="col">Status</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($Address as $index => $data) : ?>
				<tr>
					<td><?php echo $no; ?></td>
                    <td><?php echo $data['code']; ?></td>
					<td><?php echo $data['receiver_name']; ?></td>
					<td><?php echo $data['receiver_address']; ?></td>
					<td><?php echo $data['province']; ?></td>
					<td><?php echo $data['city']; ?></td>
					<td align="center"><a href="<?php echo base_url(); ?>master/bonusSchema/<?php echo $data['id']; ?>"
																class="btn btn-info"><span class="glyphicon glyphicon-tag" aria-hidden="true"></span>
							Bonus Barang
						</a>
						<a href="<?php echo base_url(); ?>master/discountSchema/<?php echo $data['id']; ?>"
							 class="btn btn-warning"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
							Bonus Diskon
						</a>
						<a style="display: none;" href="<?php echo base_url(); ?>master/productPrice/<?php echo $data['id']; ?>"
							 class="btn btn-success"><span class="glyphicon glyphicon-th" aria-hidden="true"></span>
							Harga Jual Beli
						</a>
					</td>
					<td align="center">
						<?php
						if ($data['isactive'] == 1) {
							echo "
                            <button class='btn btn-danger make-inactive' data-id='".$data['id']."'>
                                <i class='fa fa-close'></i> Deactive
                            </button>";
						} else {
							echo "
                            <button class='btn btn-success make-active' data-id='".$data['id']."'>
                                <i class='fa fa-check'></i> Active
                            </button>
                            ";
						}
						?>
					</td>
					<?php $no++; ?>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		<br>
		<br>
		<table class="table table-bordered">
			<form id="form-alamat-kirim">
				<tr>
					<td colspan="6">
						<center><strong>Menambah Alamat Kirim</strong></center>
					</td>
				</tr>
				<tr>
					<td width="20%">Nama penerima : <font color="red">*</font></td>
					<td><input name="receiver_name" id="receiver_name" value="" class="form-control" required /></td>
				</tr>
				<tr>
					<td width="20%">Alamat Kirim : <font color="red">*</font></td>
					<td><textarea name="receiver_address" id="receiver_address" value="" class="form-control" required></textarea>
					</td>
					<input name="m_partner_id" id="m_partner_id" value="<?php echo $Customer['id']; ?>" hidden />
				</tr>
				<tr>
					<td width="20%">Provinsi : <font color="red">*</font></td>
					<td>
						<select name="m_province_id" id="m_province_id" class="form-control" required>
							<option value="" disabled selected>-- Pilih Propinsi --</option>
							<?php foreach ($Province as $row) {
								echo "<option value='".$row['id']."'>".$row['name']."</option>";
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td width="20%">Kota : <font color="red">*</font></td>
					<td>
						<select name="m_city_id" id="m_city_id" class="form-control" required>
							<option value="" disabled selected>-- Pilih Kota --</option>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center">
						<button type="button" class="btn btn-success" id="simpan-anggota">
							Simpan
						</button>
						<button type="button" class="btn btn-danger" id="kembali">
							Bersihkan
						</button>
					</td>
				</tr>
			</form>
		</table>
	</section>
</div>

<script>
	$(document).ready(function () {
		function changeShippingAddressState(_idAddress, _state) {
			$.ajax({
				url: '<?php echo base_url(); ?>/Master/updateShippingAddressState',
				type: 'POST',
				data: {
					idAddress: JSON.stringify(_idAddress),
					state: JSON.stringify(_state)
				}
			})
			.done(function (data) {
				Swal.fire({
					title: 'Sukses!',
					text: 'Berhasil Mengganti Data',
					type: 'success',
					confirmButtonText: 'OK',
					onAfterClose: () => window.location.reload()
				});
			})
			.fail(function (e) {
				Swal.fire({
					title: 'Gagal!',
					text: 'Data Belum Terganti, Silahkan Coba Lagi',
					type: 'error',
					confirmButtonText: 'OK'
				});
			});

		}

		$('.make-active').on('click', function () {
			var idAdd = $(this).data('id');
			var state = 1;
			changeShippingAddressState(idAdd, state);
		});

		$('.make-inactive').on('click', function () {
			var idAdd = $(this).data('id');
			var state = 0;
			changeShippingAddressState(idAdd, state);
		});

		$('#simpan-anggota').on('click', function () {
			const formData = $('#form-alamat-kirim').serializeArray();
			let payload = {};
			for (var x = 0; x < formData.length; x++) {
				payload[formData[x].name] = formData[x].value
			}

			$.ajax({
				url: '<?php echo base_url(); ?>Master/addShippingAddress',
				type: 'POST',
				data: {
					payload: JSON.stringify(payload)
				}
			})
			.done(function (data) {
				Swal.fire({
					title: 'Sukses!',
					text: 'Berhasil Memasukkan Data',
					type: 'success',
					confirmButtonText: 'OK',
					onAfterClose: () => window.location.reload()
				});
			})
			.fail(function (e) {
				Swal.fire({
					title: 'Gagal!',
					text: 'Data Belum Masuk, Silahkan Coba Lagi',
					type: 'error',
					confirmButtonText: 'OK'
				});
			});
		})

		$('#shippingaddress-data-table').DataTable({
			scrollX: true
		});

		$('#m_province_id').on('change', function () {
			$.ajax({
				url: "<?php echo base_url()?>MasterApi/getCity",
				type: 'POST',
				data: {
					provinceId: $(this).val()
				}
			})
			.done(function (result) {
				let cityElements = $('#m_city_id');
				const cities = jQuery.parseJSON(result);

				cityElements.html(`
                    <option value="">-- Pilih Kota --</option>
                `);
				for (let x = 0; x < cities.length; x++) {
					cityElements.append(`
                    <option value="${cities[x].id}">${cities[x].name}</option>
                `);
				}
			})
			.fail(function (error) {
				console.log(error);
			});
		});
	})
</script>
