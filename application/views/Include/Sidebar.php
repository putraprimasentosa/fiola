<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

	<!-- Sidebar - Brand -->
	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url('/home/') ?>">
		<div class="sidebar-brand-icon rotate-n-15">
			<img src="<?php echo base_url('assets/images/logo-sidebar.png') ?>" width="60px" height="60px">
		</div>
		<div class="sidebar-brand-text mx-3">Royalty<sup>.id </sup></div>
	</a>

	<!-- Divider -->
	<hr class="sidebar-divider my-0">
	<hr class="sidebar-divider">

	<div class="sidebar-heading">
		<i class="fas fa-fw fa-database"></i>
		&ensp;&ensp; Pengelolaan Data
	</div>

	<!-- Nav Item - Pages Collapse Menu -->
	<?php if ($_SESSION['user']['role'] == 2) { ?>
		<li class="nav-item">
			<a class="nav-link collapsed" href="<?php echo base_url('master/admin') ?>">
				<i class="fas fa-book-reader"></i>
				<span>Admin</span>
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link collapsed" href="<?php echo base_url('master/commerce') ?>">
				<i class="fas fa-fw fa-truck-pickup"></i>
				<span>Marketplace</span>
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link collapsed" href="<?php echo base_url('master/courier') ?>">
				<i class="fas fa-fw fa-gift"></i>
				<span>Ekspedisi</span>
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link collapsed" href="<?php echo base_url('master/province') ?>">
				<i class="fas fa-fw fa-city"></i>
				<span>Kota/Provinsi</span>
			</a>
		</li>
	<?php } ?>
	<li class="nav-item">
		<a class="nav-link collapsed" href="<?php echo base_url('master/product') ?>">
			<i class="fas fa-fw fa-gift"></i>
			<span>Produk</span>
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link collapsed" href="<?php echo base_url('master/customer') ?>">
			<i class="fas fa-fw fa-city"></i>
			<span>Pelanggan</span>
		</a>
	</li>

	<div class="sidebar-heading">
		<i class="fas fa-fw fa-database"></i>
		&ensp;&ensp; Transaksi
	</div>
	<?php if ($_SESSION['user']['role'] == 1) { ?>
	<li class="nav-item">
		<a class="nav-link collapsed" href="<?php echo base_url('sales/all') ?>">
			<i class="fas fa-fw fa-coins"></i>
			<span>Penjualan</span>
		</a>
	</li>
	<?php } ?>

	<?php if ($_SESSION['user']['role'] == 2) { ?>
		<li class="nav-item">
			<a class="nav-link collapsed" href="<?php echo base_url('master/target') ?>">
				<i class="fas fa-fw fa-coins"></i>
				<span>Capaian Penjualan</span>
			</a>
		</li>
	<?php } ?>

	<hr class="sidebar-divider">
	<!-- Heading -->
	<div class="sidebar-heading">
		<i class="fas fa-fw fa-cog"></i>
		&ensp;&ensp; Laporan
	</div>

	<!-- Nav Item - Pages Collapse Menu -->
	<li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseReport" aria-expanded="true"
		   aria-controls="collapseReport">
			<i class="fas fa-fw fa-chart-bar"></i>
			<span>Laporan Penjualan</span>
		</a>
		<div id="collapseReport" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<!-- <h6 class="collapse-header">Custom Components:</h6> -->
				<a class="collapse-item" href="<?php echo base_url('/report/sales') ?>">Laporan Penjualan Bulanan</a>
				<a class="collapse-item" href="<?php echo base_url('/report/salesprod') ?>">Laporan Penjualan Produk</a>
				<a class="collapse-item" href="<?php echo base_url('/report/salestarget') ?>">Laporan Pencapaian
					Jual</a>
				<?php if ($_SESSION['user']['role'] == 2) { ?>
					<a class="collapse-item" href="<?php echo base_url('/report/salesbyadmin') ?>" target="_blank">Laporan
						Penjualan Admin</a>
				<?php } ?>
			</div>
		</div>
	</li>

	<li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
		   aria-controls="collapseTwo">
			<i class="fas fa-fw fa-chart-line"></i>
			<span>Rekapan Data</span>
		</a>
		<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<!-- <h6 class="collapse-header">Custom Components:</h6> -->
				<?php if ($_SESSION['user']['role'] == 2) { ?>
					<a class="collapse-item" href="<?php echo base_url('/report/admin') ?>" target="_blank">Admin</a>
				<?php } ?>
				<a class="collapse-item" href="<?php echo base_url('/report/product') ?>" target="_blank">Produk</a>
				<a class="collapse-item" href="<?php echo base_url('/report/customer') ?>" target="_blank">Pelanggan</a>
			</div>
		</div>
	</li>

	<!-- Nav Item - Utilities Collapse Menu -->
	<!-- <li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
	<i class="fas fa-fw fa-chart-pie"></i>
	<span>Penilaian (<i>Dashboard</i>)</span>
  </a>
  <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
	<div class="bg-white py-2 collapse-inner rounded">
	  <h6 class="collapse-header">Custom Utilities:</h6>
	  <a class="collapse-item" href="utilities-color.html">Colors</a>
	  <a class="collapse-item" href="utilities-border.html">Borders</a>
	  <a class="collapse-item" href="utilities-animation.html">Animations</a>
	  <a class="collapse-item" href="utilities-other.html">Other</a>
	</div>
  </div>
</li> -->

	<!-- Divider -->
	<hr class="sidebar-divider">

	<!-- Heading -->


	<!-- Divider -->
	<hr class="sidebar-divider d-none d-md-block">

	<!-- Sidebar Toggler (Sidebar)
<div class="text-center d-none d-md-inline">
  <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div> -->

</ul>
