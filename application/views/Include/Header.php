<html>

<head>
	<title><?php echo $title; ?></title>
	<script>
		const baseUrl = '<?php echo base_url(); ?>';
	</script>

	<!-- <link rel="stylesheet" type="text/css" href="<?php echo getNodeUrl('admin-lte/dist/css/adminlte.min.css') ?>" /> -->
	<link rel="stylesheet" type="text/css"
		  href="<?php echo getNodeUrl('admin-lte/dist/css/skins/_all-skins.min.css') ?>"/>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bs/bootstrap.min.css') ?>"/>
	<link rel="stylesheet" type="text/css"
		  href="<?php echo getNodeUrl('bootstrap-datepicker/dist/css/bootstrap-datepicker.css') ?>"/>
	<link rel="stylesheet" type="text/css"
		  href="<?php echo getNodeUrl('@fortawesome/fontawesome-free/css/all.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo getNodeUrl('sweetalert2/dist/sweetalert2.min.css') ?>">
	<link rel="stylesheet" type="text/css"
		  href="<?php echo getNodeUrl('@danielfarrell/bootstrap-combobox/css/bootstrap-combobox.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo getNodeUrl('select2/dist/css/select2.css') ?>">
	<link rel="stylesheet" type="text/css"
		  href="<?php echo getNodeUrl('select2-bootstrap-theme/dist/select2-bootstrap.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/sb-admin-2.css') ?>">
	<link href="<?php echo base_url('assets/css/all.min.css') ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css"
		  href="<?php echo base_url('assets/plugins/datatables/datatables.min.css') ?>">
	<link rel="stylesheet" type="text/css"
		  href="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap4.min.css') ?>">
	<!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->

	<!-- <script src="<?php echo getNodeUrl('jquery/dist/jquery.min.js') ?>"></script> -->
	<script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/plugins/datatables/datatables.min.js') ?>"></script>
	<script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery.easing.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/hs/highcharts.src.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/hs/modules/data.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/hs/modules/drilldown.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/hs/modules/exporting.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/hs/modules/export-data.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/hs/modules/accessibility.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/sb-admin-2.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/Chart.min.js') ?>"></script>
	<script src="<?php echo getNodeUrl('sweetalert2/dist/sweetalert2.min.js') ?>"></script>
	<script src="<?php echo getNodeUrl('accounting-js/dist/accounting.umd.js') ?>"></script>
	<script src="<?php echo getNodeUrl('bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
	<script src="<?php echo getNodeUrl('select2/dist/js/select2.full.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/constants.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/helpers.js') ?>"></script>

	<script>
		// $.fn.select2.defaults.set( "theme", "bootstrap" );
	</script>
</head>
<style>
	.form-control {
		border: 1px solid #aaa;
	}
</style>

<body class="page-top">
<!-- Main Header -->

<div id="wrapper">
