<div class="container-fluid">
    <section class="content">
        <h2 style="margin-bottom: 20px">Daftar Penjualan</h2>
        <div class="row">
            <div class="col-md-12">
                <button id="addNew" type="button" class="btn btn-info" value="A"><span class="fa fa-plus"></span> Tambah Baru</button>
                <h3></h3>

            </div>

            <div class="col-md-12">
                <table id="salesTable" name="salesTable" class="table table-bordered">
                    <thead>
                        <th></th>
                        <th>Nomor Transaksi</th>
                        <th>Tanggal Transaksi</th>
                        <th>Pelanggan</th>
                        <th>Ekspedisi</th>
                        <th>Total Penjualan</th>
                        <th>Tindakan</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($orders as $data) {
                            echo "<tr>";
                            echo "<td>" . $data['id'] . "</td>";
                            echo "<td>" . $data['transaction_no'] . "</td>";
                            echo "<td>" . date_create_from_format('Y-m-d', $data['transaction_date'])->format('d-m-Y') . "</td>";
                            echo "<td>" . $data['customer']['customer_name'] . "</td>";
                            echo "<td>" . $data['courier']['courier_name'] . "</td>";
                            echo "<td style='text-align: right;'>Rp. " . number_format($data['grandtotal'], 0, ",", ".") . "</td>";
                            echo "<td><button type='button' id='btnView' class='btn btn-block btn-info btn-sm viewOrder' target='_blank' data-id='" . $data['id'] . "'><span class='fa fa-search' aria-hidden='true'></span> Lihat</button></td>";
                            echo "</tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>

        </div>
    </section>
</div>
<script>
    var id = 0;
    var dt = null;

    function printReport(id) {
        this.id = id;
    }
    $("#salesTable").on('click', 'button.viewOrder', function(e) {
        console.log(JSON.stringify($(this).data()));
        var id = $(this).data("id");
        window.open('<?php echo base_url() . "sales/view/" ?>' + id, '_blank');
        e.preventDefault();
    });

    $(".deleteOrder").click(function() {
        var id = $(this).data("id");
        // alert(id);
    });

    $("#simpan-detail").click(function() {
        $("#printModal").modal('hide');
        window.open('<?php echo base_url() . "penjualan/printOrder?id=" ?>' + id + '&c1=' + $('#c1').prop('checked') + '&c2=' + $('#c2').prop('checked') + '&c3=' + $('#c3').prop('checked') + '&c4=' + $('#c4').prop('checked'), '_blank');
    });
    $(document).ready(function() {
        dt = $("#salesTable").DataTable({
            columnDefs: [{
                targets: [0],
                visible: false
            }],
            "order": [
                [1, "desc"]
            ],
            ordering: false
        });
    });

    $("#salesTable").on('click', 'a.receiveOrder', function(e) {
        e.preventDefault();
        Swal.fire({
            title: 'Terima Barang?',
            text: "Barang yang telah diterima tidak dapat dibatalkan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ok!',
            cancelButtonText: 'Cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                        url: "<?php echo base_url() ?>/penjualan/receiveOrder",
                        type: "POST",
                        data: {
                            id: $(this).data("id"),
                        }
                    })
                    .done(function(addressRawData) {
                        Swal.fire(
                            'Berhasil!',
                            'Barang telah diterima.',
                            'success'
                        )
                        location.reload();
                    })
                    .fail(function() {
                        Swal.fire(
                            'Cancelled',
                            'Barang tidak jadi diterima.',
                            'error'
                        )
                    });

            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire(
                    'Cancelled',
                    'Penerimaan barang dibatalkan.',
                    'error'
                )
            }
        });
    });

    $("#salesTable").on('click', 'a.editOrder', function(e) {
        $(location).attr("href", "<?php echo base_url() ?>penjualan/updatePesanan/" + $(this).data("id"));
    });

    $("#salesTable").on('click', 'a.deleteOrder', function(e) {
        e.preventDefault();
        Swal.fire({
            title: 'Yakin?',
            text: "Data yang telah dihapus tidak dapat dikembalikan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                var data = dt.row($(this).parents('tr')).data();
                dt.row($(this).parents('tr')).remove().draw();
                $.ajax({
                        url: "<?php echo base_url() ?>/penjualan/delOrder",
                        type: "POST",
                        data: {
                            id: $(this).data("id"),
                        }
                    })
                    .done(function(addressRawData) {
                        Swal.fire(
                            'Deleted!',
                            'Data tidak bisa dikembalikan.',
                            'success'
                        )
                    })
                    .fail(function() {
                        Swal.fire(
                            'Cancelled',
                            'Penghapusan data gagal.',
                            'error'
                        )
                    });

            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire(
                    'Cancelled',
                    'Penghapusan data dibatalkan.',
                    'error'
                )
            }
        });
    });


    $("#addNew").click(function() {
        $(location).attr("href", "<?php echo base_url() ?>sales/add");
    });
</script>
