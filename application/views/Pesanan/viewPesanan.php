<div class="content-wrapper">
    <section class="content">
        <form id="form-pesanan-penjualan">
            <div class="row">
                <div class="col-md-4">
                    <div class="col-md-4 p-l-0 anggota-container">
                        <h5>Pelanggan : </h5>
                    </div>
                    <div class="col-md-8 p-l-0 anggota-container">
                        <select name="m_partner_id" class="form-control" id="m_partner_id" disabled>
                            <?php
                            echo '<option >' . $order['partner']['name'] . '</option>';
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-4 p-l-0">
                        <h5>Tanggal : </h5>
                    </div>
                    <div class="col-md-8 p-l-0">
                        <input name="orderdate" id="orderdate" class="form-control" value="<?php echo date("d-m-Y", strtotime($order['orderdate'])) ?>" disabled />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-4">
                        <h5>Ekspedisi </h5>
                    </div>
                    <div class="col-md-8">
                        <select name="m_courier_id" id="m_courier_id" class="form-control" disabled>
                            <option><?php echo ($order['m_courier_id'] == 0) ? 'Ambil Sendiri' : $order['courier']['name']; ?></option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="col-md-4 p-l-0 anggota-container">
                        <h5>Penerima : </h5>
                    </div>
                    <div class="col-md-8 p-l-0 anggota-container">
                        <select class="form-control" disabled>
                            <option><?php echo $order['shippingAddress']['receiver_name'] ?></option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-4 p-l-0">
                        <h5>Tempo : </h5>
                    </div>
                    <div class="col-md-8 p-l-0">
                        <input name="orderdate" id="orderdate" class="form-control" value="<?php echo date("d-m-Y", strtotime($order['duedate'])) ?>" disabled />
                    </div>
                </div>
                <div class="col-md-4" id="nopolGroup">
                    <div class="col-md-4 p-l-0 anggota-container">
                        <h5>No Pol : </h5>
                    </div>
                    <div class="col-md-8 p-l-0 anggota-container">
                        <input class="form-control" type="text" id="nopol" name="nopol" value="<?php echo $order['nopol']; ?>" disabled></input>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-4">
                    <div class="col-md-4 p-l-0 kirim-container">
                        <h5>Alamat Kirim : </h5>
                    </div>
                    <div class="col-md-8 p-l-0 kirim-container">
                        <textarea class="form-control" id="alamat_kirim" disabled><?php echo $order['shippingAddress']['receiver_address'] ?> </textarea>
                    </div>
                </div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4" id="driverGroup">
                    <div class="col-md-4 p-l-0 anggota-container">
                        <h5>Driver : </h5>
                    </div>
                    <div class="col-md-8 p-l-0 anggota-container">
                        <input class="form-control" type="text" id="driver" name="driver" value="<?php echo $order['driver']; ?>" disabled></input>
                    </div>
                </div>
            </div>
            <br>
            <div>
                <table id="detailTable" class="table table-bordered">
                    <thead>
                        <tr>

                            <th class="text-center">
                                Item
                            </th>
                            <th class="text-center">
                                Spesifikasi
                            </th>
                            <th class="text-center">
                                Jumlah Pesanan
                            </th>
                            <th class="text-center">
                                Harga
                            </th>
                            <th class="text-center">
                                Total
                            </th>
                            <th class="text-center">
                                Berat
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($orderLine as $line) {
                            echo "<tr>";
                            echo "<td>" . $line['product']['name'] . "</td>";
                            echo "<td>" . $line['product']['brand'] . "</td>";
                            echo "<td>" . $line['qty'] . "</td>";
                            echo "<td>" . $line['price'] . "</td>";
                            echo "<td>" . $line['qty'] * $line['price'] . "</td>";
                            echo "<td>" . $line['weight'] . "</td>";
                            echo "</tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
            <div class="row" style="margin-top:15;">
                <div class="col-lg-4 m-t-20 pull-right">
                    <div class="col-md-12 p-l-0">
                        <div class="col-md-4 p-l-0">
                            <h5>Subtotal Item : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <input class="accounting-enabled form-control numField" name="totallines" id="totallines" value="<?php echo $order['totallines'] ?>" required readonly />
                        </div>
                    </div>
                    <div class="col-md-12 p-l-0 shippingRow" hidden>
                        <div class="col-md-4 p-l-0">
                            <h5>Total Ongkos Kirim : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <input class="accounting-enabled form-control numField" name="totalShippingAmt" id="totalShippingAmt" value="<?php echo $order['totalShippingAmt'] ?>" required readonly />
                        </div>
                    </div>
                    <div class="col-md-12 p-l-0">
                        <div class="col-md-4 p-l-0">
                            <h5>Total Akhir : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <input class="accounting-enabled form-control numField" name="grandtotal" id="grandtotal" value="<?php echo $order['grandtotal'] ?>" required readonly />
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 m-t-20 pull-right">
                    <div class="col-md-12 p-l-0 m-t-10 shippingRow" hidden>

                        <div class="col-md-4 p-l-0">
                            <h5>Ongkos Kirim : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Qty" name="totalWeight" id="totalWeight" value="<?php echo $order['totalWeight'] ?>">
                                <div class="input-group-addon" id="unit">Kg / @</div>
                                <input type="text" class="form-control" id="shippingAmt" placeholder="Ongkos Kirim" value="<?php echo $order['shippingAmt'] ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 p-l-0 m-t-10">
                        <div class="col-md-4 p-l-0">
                            <h5>Potongan : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <div class="input-group">
                                <div class="input-group-addon" id="discUseCurrency" hidden>Rp </div>
                                <input tabindex="1" class="accounting-enabled form-control numField" id="discountpct" value="<?php echo $order['discountpct'] ?>" disabled />
                                <!-- <div class="input-group-addon" id="discPct"><input type="checkbox" id="discUseAmtCheck" /><span id="percent"> (use %)</span> </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 p-l-0 m-t-10" id="discUseAmt" hidden>
                    </div>
                    <div class="col-md-12 p-l-0 m-t-10">
                    </div>
                    <!-- <div class="col-md-12 p-l-0 m-t-10">
                        <div class="col-md-4 p-l-0">
                            <h5>Pajak (%) : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <input class="form-control accounting-enabled" type="number" name="taxpct" id="taxpct" value="0" />
                        </div>
                    </div> -->
                </div>
                <div class="col-md-4 m-t-20">
                    <div class="col-md-12 p-l-0">
                        <div class="col-md-3 p-l-0">
                            <h5>Keterangan : </h5>
                        </div>
                        <div class="col-md-9 p-l-0">
                            <textarea class="form-control" name="description" id="description" disabled><?php echo $order['description']; ?></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-lg-6">
                    <td>

                    </td>
                </div>
                <div class="col-lg-6">
                    <div class="pull-right">
                        <button type="button" id="cetak-pesanan-penjualan" data-toggle='modal' data-target='#printModal' class="btn btn-info"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Cetak
                        </button>
                        <button style="align: right;" type="button" class="btn btn-warning" id="btnBackToList"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Kembali
                        </button>
                    </div>
                </div>
            </div>
        </form>

        <!-- Modal -->
        <div class="modal fade" id="modal-konfirmasi-penjualan" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Konfirmasi Pembuatan DO</h4>
                    </div>
                    <div class="modal-body">
                        <p class="text-center no-padding no-margin" id="teks-konfirmasi-penjualan" style="font-size: 11pt"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="button" id="simpan-pesanan-penjualan-lanjut" class="btn btn-primary">Iya</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="printModal" tabindex="-1" aria-hidden="true" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Pilih Cetakan</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form id="form-detail-penjualan" data-toggle="validator" role="form">
                                    <div class="checkbox">
                                        <label><input type="checkbox" id="c1" value="" checked>Sales Order</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" id="c2" value="" checked>Delivery Order</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" id="c3" value="" checked>Faktur Penjualan</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" id="c4" value="" checked>Laporan Pengiriman Barang</label>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button type="button" id="simpan-detail" class="btn btn-primary">Cetak</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <script type="text/javascript" src="<?php echo getNodeUrl('bootstrap-datepicker/dist/js/bootstrap-datepicker.js'); ?>"></script>
        <script type="text/javascript">
            $("#simpan-detail").click(function() {
                $("#printModal").modal('hide');
                window.open('<?php echo base_url() . "penjualan/printOrder?id=" . $orderID ?>' + '&c1=' + $('#c1').prop('checked') + '&c2=' + $('#c2').prop('checked') + '&c3=' + $('#c3').prop('checked') + '&c4=' + $('#c4').prop('checked'), '_blank');
            });
        </script>
    </section>
</div>