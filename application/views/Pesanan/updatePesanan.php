<?php
$order = $orders;
?>
<div class="content-wrapper">
    <section class="content">
        <form id="form-pesanan-penjualan">
            <div class="row">
                <div class="col-md-4">
                    <div class="col-md-4 p-l-0 anggota-container">
                        <h5>Nomor Dokumen :</h5>
                    </div>
                    <div class="col-md-8 p-l-0 anggota-container">
                        <input type="text" class="form-control" id="documentno" value="<?php echo $order["documentno"]; ?>" readonly>
                        <input type="text" id="t_order_id" value="<?php echo $order["id"] ?>" hidden>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="col-md-4 p-l-0 anggota-container">
                        <h5>Pelanggan : <?php echo $order['m_partner_id']; ?></h5>
                    </div>
                    <div class="col-md-8 p-l-0 anggota-container">
                        <select name="m_partner_id" class="form-control" id="m_partner_id" required>
                            <!-- <option disabled selected value> -- Select Customer -- </option> -->
                            <?php
                            foreach ($partners as $partner) {
                                echo '<option value="' . $partner["id"] . '" ' . ($partner["id"] == $order["m_partner_id"] ? 'selected' : '') . '>' . $partner["code"] . ' | ' . $partner["name"] . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-4 p-l-0 anggota-container">
                        <h5>Penerima : </h5>
                    </div>
                    <div class="col-md-8 p-l-0 anggota-container">
                        <input id="address_db" value="<?php echo $order["address"]; ?>" hidden>
                        <select class="form-control" id="address" name="address"></select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-4">
                        <h5>Ekspedisi</h5>
                    </div>
                    <div class="col-md-8">
                        <select name="m_courier_id" id="m_courier_id" class="form-control">
                            <option value="0">Tanpa Ekspedisi/Diambil</option>
                            <?php
                            foreach ($couriers as $courier) {
                                echo '<option value="' . $courier['id'] . '"' . ($courier["id"] == $order["m_courier_id"] ? 'selected' : '') . '>' . $courier['code'] . ' - ' . $courier['name'] . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="col-md-4 p-l-0">
                        <h5>Tanggal <?php echo $order["orderdate"]; ?> : </h5>
                    </div>
                    <div class="col-md-8 p-l-0">
                        <input name="orderdate" id="orderdate" value="<?php echo date("d-m-Y", strtotime($order["orderdate"])); ?>" class="form-control" required />
                        <input name="deliverydate" id="deliverydate" value="<?php echo date("d-m-Y", strtotime($order["deliverydate"])); ?>" class="form-control hidden" required>
                        <input name="dateacct" id="dateacct" value="<?php echo date("d-m-Y", strtotime($order["dateacct"])); ?>" class="form-control hidden" required>
                    </div>
                </div>
                <div class="col-md-4">

                </div>
                <div class="col-md-4" id="nopolGroup" <?php echo $order["m_courier_id"] == 0 ? 'hidden' : ''; ?>>
                    <div class="col-md-4 p-l-0 anggota-container">
                        <h5>No Pol : </h5>
                    </div>
                    <div class="col-md-8 p-l-0 anggota-container">
                        <input class="form-control" type="text" id="nopol" name="nopol" value="<?php echo $order["nopol"]; ?>"></input>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group col-md-6 ">
                        <h5>Pembayaran : </h5>
                        <select class="form-control" name="paymentterm" id="paymentTerm">
                            <option value="K">Kredit</option>
                            <option value="C">Cash</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-4 p-l-0 kirim-container">
                        <h5>Alamat Kirim : </h5>
                    </div>
                    <div class="col-md-8 p-l-0 kirim-container">
                        <textarea class="form-control" id="alamat_kirim"> </textarea>
                    </div>
                </div>
                <div class="col-md-4" id="driverGroup" <?php echo $order["m_courier_id"] == 0 ? 'hidden' : ''; ?>>
                    <div class="col-md-4 p-l-0 anggota-container">
                        <h5>Driver : </h5>
                    </div>
                    <div class="col-md-8 p-l-0 anggota-container">
                        <input class="form-control" type="text" id="driver" name="driver" value="<?php echo $order["driver"]; ?>"></input>
                    </div>
                </div>
                <div class="col-md-4" id="isBorongan" <?php echo $order["m_courier_id"] == 0 ? 'hidden' : ''; ?>>
                    <div class="col-md-4 p-l-0 anggota-container">
                        <h5>Borongan <?php echo $order["isborongan"]; ?>: </h5>
                    </div>
                    <div style="margin-top: 8px;" class="col-md-8 p-t-0 anggota-container">
                        <input type="checkbox" name="isborongan" id="isBoronganCheck" <?php echo $order["isborongan"] == 1 ? 'checked' : ''; ?> /><span id="percent"> Ya</span></input>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8" id="dueGroup" <?php echo $order["paymentterm"] == "C" ? 'hidden' : '' ?>>
                    <div class="form-group col-md-2 ">
                        <h5>Tempo : </h5>
                        <input type="number" class="form-control" name="daysdue" id="daysDue" placeholder="Jatuh Tempo" value="<?php echo $order["daysdue"] == null ? 0 : $order["daysdue"]; ?>">
                    </div>
                    <div class="form-group col-md-4">
                        <h5>Jatuh Tempo : </h5>
                        <input type="text" class="form-control" id="dateDue" placeholder="Jatuh Tempo" value="0" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">

                </div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4" align="right" style="margin-top:10px">
                    <button style="margin-left: 5px;" id="addDetail" type="button" data-toggle="modal" data-target="#addItemModal" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Detail
                    </button>
                </div>
            </div>

            <br>
            <div>
                <table id="detailTable" class="table table-bordered">
                    <thead>
                    <tr>

                        <th class="text-center">
                            Item
                        </th>
                        <th class="text-center">
                            Spesifikasi
                        </th>
                        <th class="text-center">
                            Jumlah Pesanan
                        </th>
                        <th class="text-center">
                            Jumlah Bonus
                        </th>
                        <th class="text-center">
                            Harga
                        </th>
                        <th class="text-center">
                            Total
                        </th>
                        <th class="text-center">
                            Berat
                        </th>
                        <th class="text-center">
                            Bonus Diskon
                        </th>
                        <th class="text-center">
                            Diskon Tambahan
                        </th>
                        <th class="text-center">
                            Action
                        </th>
                    </tr>
                    </thead>
                </table>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
            <div class="row" style="margin-top:15;">
                <div class="col-lg-6 m-t-20 pull-right">
                    <div class="col-md-12 p-l-0">
                        <div class="col-md-4 p-l-0">
                            <h5>Subtotal Item : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <input class="accounting-enabled form-control numField" name="totallines" id="totallines" value="<?php echo number_format($order["totallines"], 0, ',', '.'); ?>" required readonly />
                        </div>
                    </div>
                    <div class="col-md-12 p-l-0 m-t-10">
                        <div class="col-md-4 p-l-0">
                            <h5>Potongan : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <div class="input-group">
                                <div class="input-group-addon" id="discUseCurrency" hidden>Rp </div>
                                <input tabindex="1" class="form-control numField" id="discountpct" value="<?php echo number_format($order["discountamt"], 0, ',', '.'); ?>" step="0.01" />
                                <div class="input-group-addon" id="discPct"><input type="checkbox" id="discUseAmtCheck" /><span id="percent"> (use %)</span> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 p-l-0 m-t-10" id="discUseAmt" hidden>
                        <div class="col-md-4 p-l-0">
                            <h5>Nilai Potongan : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <div class="input-group">
                                <div class="input-group-addon" id="discAmt">Rp </div>
                                <input class="accounting-enabled form-control numField" name="discountamt" id="discountamt" value="<?php echo number_format($order["discountamt"], 0, ',', '.'); ?>" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 p-l-0 m-t-10">
                        <div class="col-md-4 p-l-0">
                            <h5>Potongan Bonus : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <div class="input-group">
                                <div class="input-group-addon" id="discAmt">Rp </div>
                                <input class="accounting-enabled form-control numField" type="text" name="discountBonusAmt" id="discountBonusAmt" value="<?php echo number_format($order["discountBonusAmt"], 0,',','.');?>" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 p-l-0 m-t-10 oaRow">
                        <div class="col-md-4 p-l-0">
                            <h5>Potongan OA : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Qty" id="totalWeightOa" value="<?php echo $order["totalWeight"]; ?>">
                                <div class="input-group-addon" id="unit">Kg / @</div>
                                <input class="accounting-enabled form-control numField" name="qtyOa" id="qtyOa" placeholder="OA" value="<?php echo number_format($order["qtyOa"], 0, ',', '.'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 p-l-0 m-t-10 oaRow">
                        <div class="col-md-4 p-l-0">
                            <h5>Total Potongan OA : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <div class="input-group">
                                <div class="input-group-addon" id="discUseCurrency" hidden>Rp </div>
                                <input tabindex="1" class="accounting-enabled form-control numField" name="potonganoa" id="potonganoa" value="<?php echo $order["potonganoa"]; ?>" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 p-l-0 m-t-10 shippingRow" <?php echo $order["m_courier_id"] == 0 ? 'hidden' : ''; ?>>
                        <div class="col-md-4 p-l-0">
                            <h5>Ongkos Kirim : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Qty" name="totalWeight" id="totalWeight" value="<?php echo $order["totalWeight"]; ?>">
                                <div class="input-group-addon" id="unit">Kg / @</div>
                                <input type="text" class="form-control" name="shippingAmt" id="shippingAmt" placeholder="Ongkos Kirim" value="<?php echo $order["shippingAmt"]; ?>" <?php echo $order["isborongan"] < 1 ? '' : 'readonly' ?>>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 p-l-0 shippingRow" <?php echo $order["m_courier_id"] == 0 ? 'hidden' : ''; ?>>
                        <div class="col-md-4 p-l-0">
                            <h5>Total Ongkos Kirim : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <input class="accounting-enabled form-control numField" name="totalShippingAmt" id="totalShippingAmt" value="<?php echo $order["totalShippingAmt"]; ?>" required <?php echo $order["isborongan"] > 0 ? '' : 'readonly' ?> />
                        </div>
                    </div>
                    <div class="col-md-12 p-l-0 m-t-10">
                        <div class="col-md-4 p-l-0">
                            <h5>Potongan Tunai : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <div class="input-group">
                                <div class="input-group-addon" id="discUseCurrency" hidden>Rp </div>
                                <input tabindex="1" class="accounting-enabled form-control numField" name="discountCashAmt" id="discountCashAmt" value="<?php echo number_format($order["discountCashAmt"], 0, ',', '.'); ?>" readonly />
                                <div class="input-group-addon" id="discCash"><input type="checkbox" id="discCashCheck" /><span id="percent"> (1.5%)</span> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 p-l-0 m-t-10">
                        <div class="col-md-4 p-l-0">
                            <h5>Total Akhir : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <input class="accounting-enabled form-control numField" name="grandtotal" id="grandtotal" value="<?php echo number_format($order["grandtotal"], 0, ',', '.'); ?>" required readonly />
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 m-t-20 pull-right">

                    <div class="col-md-12 p-l-0 m-t-10">
                    </div>
                    <div class="col-md-12 p-l-0 m-t-10" id="discUseAmt" hidden>
                        <!-- <div class="col-md-4 p-l-0">
                            <h5>Nilai Potongan : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <div class="input-group">
                                <div class="input-group-addon" id="discAmt">Rp </div>
                                <input class="accounting-enabled form-control numField" name="discountamt" id="discountamt" value="0" readonly />
                            </div>
                        </div> -->
                    </div>
                    <div class="col-md-12 p-l-0 m-t-10">

                    </div>
                    <div class="col-md-12 p-l-0 m-t-10" hidden>
                        <div class="col-md-4 p-l-0">
                            <h5>Pajak (%) : </h5>
                        </div>
                        <div class="col-md-8 p-l-0">
                            <input class="form-control accounting-enabled" type="number" name="taxpct" id="taxpct" value="0" />
                        </div>
                    </div>
                </div>
                <div class="col-md-4 m-t-20">
                    <div class="col-md-12 p-l-0">
                        <div class="col-md-3 p-l-0">
                            <h5>Keterangan : </h5>
                        </div>
                        <div class="col-md-9 p-l-0">
                            <textarea class="form-control" name="description" id="description"><?php echo $order["description"] ?></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-lg-6">
                    <td>

                    </td>
                </div>
                <div class="col-lg-6">
                    <div class="pull-right">
                        <button type="submit" id="simpan-pesanan-penjualan" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Simpan
                        </button>
                        <button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            Batal
                        </button>
                        <button type="button" id="cetak-pesanan-penjualan" class="btn btn-info"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Cetak
                        </button>
                        <button style="align: right;" type="button" class="btn btn-warning" id="btnBackToList"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Tutup
                        </button>
                    </div>
                </div>
            </div>

            <input type="text" name="issotrx" value="T" hidden>
            <input type="text" name="istaxuse" id="istaxuse" value="T" hidden>
            <input type="text" name="usecourier" id="usecourier" hidden>
            <input type="text" name="created_at" value="<?php echo date("m/d/Y"); ?>" hidden>
            <input type="text" name="updated_at" value="<?php echo date("m/d/Y"); ?>" hidden>
            <input type="text" name="status" value="Draft" hidden>
            <input type="text" name="created" value="1" hidden>
            <input type="text" id="addressCity" value="" hidden>
        </form>

        <div class="modal fade" id="addItemModal" tabindex="-1" aria-hidden="true" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Tambah Barang Baru</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form id="form-detail-penjualan" data-toggle="validator" role="form">
                                    <div class="form-body">
                                        <input type="hidden" id="harga_asli">
                                        <input type="hidden" id="hpp_item" name="hpp_item" value="123">
                                        <input type="hidden" id="kode_item" name="kode_item">
                                        <input type="hidden" id="nama_item" name="nama_item">
                                        <input type="hidden" id="id_satuan" name="id_satuan">
                                        <input type="hidden" id="satuan" name="satuan" value="PCS">
                                        <div class="form-group form-md-line-input">
                                            <span class="help-block">Pilih item yang akan dijual</span>
                                            <select class="form-control" id="item_id" name="item_id">
                                                <option disabled selected value> -- Item-- </option>
                                                <?php
                                                foreach ($products as $product)
                                                    echo "<option  id='pro_" . $product['id'] . "' value='" . $product['id'] . "'>" . $product['code'] . " - " . $product['name'] . "</option>";
                                                ?>
                                            </select>

                                        </div>
                                        <div class="form-group form-inline">
                                            <div class="input-group col-md-4">
                                                <input type="text" class="form-control " id="qtyEntered" placeholder="Quantity" name="qtyEntered">
                                                <div class="input-group-addon" id="unit">Pcs</div>
                                            </div>

                                            <div class="input-group col-md-4">
                                                <input type="text" class="form-control" id="priceEntered" placeholder="Price" name="priceEntered">
                                                <div class="input-group-addon" id="unit">@Item</div>
                                            </div>

                                            <div class="input-group col-md-3">
                                                <input type="text" class="form-control" id="weightEntered" placeholder="Weight" name="weight">
                                                <div class="input-group-addon" id="unit"> Kg</div>
                                            </div>
                                        </div>
                                        <div class="form-group form-inline">
                                            <span class="help-block"><strong>Bonus Quantity & Discount</strong></span>
                                            <div class="input-group col-md-4">
                                                <input type="text" class="form-control " id="qtyBonus" placeholder="Bonus Quantity" name="qtyBonus">
                                                <div class="input-group-addon" id="unit">Pcs</div>
                                            </div>
                                            <div class="input-group col-md-7">
                                                <div class="input-group-addon" id="unit">Rp.</div>
                                                <input type="text" class="form-control " id="discountBonus" placeholder="Bonus Discount" name="discountBonus" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <div class="input-group col-md-12 pull-right">
                                                <div class="input-group-addon">Rp.</div>
                                                <input type="text" class="form-control" id="subTotalItem" placeholder="Sub Total" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <h5 class=""><strong>Discount</strong></h5>
                                                <div class="input-group">
                                                    <div class="input-group-addon" id="unit">Rp.</div>
                                                    <input type="text" class="form-control" id="discountItem" placeholder="Discount" value="0" name="discount">
                                                    <div class="input-group-addon" id="unit">.00</div>
                                                </div>
                                            </div>
                                            <div class="col-md-6" hidden>
                                                <h5 class=""><strong>Pajak</strong></h5>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="taxItem" placeholder="Pajak" value="0" name="tax" required>
                                                    <div class="input-group-addon" id="unit">%</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <h5 class=""><strong>Bonus Discount</strong></h5>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="additionalDisc" placeholder="Bonus Tambahan" name="additionalDisc">
                                                    <div class="input-group-addon" id="unit">%</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="">Grand Total</label>
                                            <div class="input-group col-md-12">
                                                <div class="input-group-addon" id="unit">Rp.</div>
                                                <input type="text" class="form-control" id="grandTotalItem" placeholder="Grand Total" readonly>
                                                <div class="input-group-addon" id="unit">.00</div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button type="button" id="simpan-detail" class="btn btn-primary">Simpan</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- Modal -->
        <div class="modal fade" id="modal-konfirmasi-penjualan" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Konfirmasi Pembuatan DO</h4>
                    </div>
                    <div class="modal-body">
                        <p class="text-center no-padding no-margin" id="teks-konfirmasi-penjualan" style="font-size: 11pt"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="button" id="simpan-pesanan-penjualan-lanjut" class="btn btn-primary">Iya</button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="<?php echo getNodeUrl('bootstrap-datepicker/dist/js/bootstrap-datepicker.js'); ?>"></script>
        <script src="<?php echo getNodeUrl('moment/moment.js') ?>"></script>
        <!-- <script src="<?php echo base_url(); ?>assets/metronic/global/plugins/select2/js/select2.full.js"></script> -->
        <script type="text/javascript">
            var counter = 1;
            var totalLines = 0.00;
            var dTable = null;
            var specialDisc = false;

            function makeTime() {
                var today = new Date();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                m = checkTime(m);
                s = checkTime(s);
                document.getElementById('waktu_transaksi').value = h + ":" + m + ":" + s;
                var t = setTimeout(makeTime, 500);
            }

            function checkTime(i) {
                if (i < 10) {
                    i = "0" + i
                } // add zero in front of numbers < 10
                return i;
            }
            $("#form-detail-penjualan input").keydown(function(e) {
                if (e.keyCode == 13) {
                    $('#simpan-detail').trigger('click');
                }
            });

            $(document).keydown(function(e) {
                // $("#m_partner_id").focus();
                if (e.keyCode == 13 && !$("#form-detail-penjualan").is(':visible') &&
                    !($("#description").is(":focus")) &&
                    !($("#discountpct").is(":focus"))
                ) {
                    $("#addDetail").trigger('click');
                    $("#addDetail").blur();
                }
            });

            $("#discountpct").keydown(function(e) {
                if (e.keyCode == 13) {
                    $("#simpan-pesanan-penjualan").trigger('click');
                    e.preventDefault();
                }
            });
            $("#addDetail, #description").keydown(function(e) {
                if (e.which == 9) {
                    $("#discountpct").focus();
                    e.preventDefault();
                }
            });
            $('#m_partner_id').on("keydown change", function(event) {
                if (event.keyCode == 13) {
                    $("#m_partner_id").focus();
                    event.preventDefault();
                }
            });

            $("#m_courier_id").keydown(function(e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                }
            });
            $("#btnBackToList").click(function() {
                $(location).attr("href", "<?php echo base_url(); ?>penjualan/allData");
            });

            $('body').on('shown.bs.modal', '.modal', function() {
                $(this).find('#item_id').each(function() {
                    var dropdownParent = $(document.body);
                    if ($(this).parents('.modal.in:first').length !== 0) {
                        dropdownParent = $(this).parents('.modal.in:first');
                    }
                    $(this).select2({
                        dropdownParent: dropdownParent
                        // ...
                    });

                });
                $("#item_id").select2('open');
            });

            $(document).ready(function() {

                $("#orderdate").datepicker({
                    autoclose: true,
                    format: 'd-mm-yyyy'
                });

                $("#dateDue").datepicker({
                    autoclose: true,
                    format: 'd-mm-yyyy',
                    enableOnReadonly: false,
                });

                $("#m_courier_id").select2();
                $("#m_partner_id").select2();
                $("#item_id").select2();
                $('#address').select2();


                dTable = $("#detailTable").DataTable({
                    "autoWidth": false,
                    "ordering": false,
                    columns: [

                        {
                            data: "m_product_id"
                        },
                        {
                            data: "specification"
                        },
                        {
                            data: "qtyEntered"
                        },
                        {
                            data: "qtyBonus"
                        },
                        {
                            data: "priceEntered"
                        },
                        {
                            data: "subtotal"
                        },
                        {
                            data: "weight"
                        },
                        {
                            data: "additionalDisc"
                        },
                        {
                            data: "discountItem"
                        },
                        {
                            data: "Action"
                        },
                        {
                            data: "h_product_id"
                        },
                        {
                            data: "h_qtyEntered"
                        },
                        {
                            data: "h_priceEntered"
                        },
                        {
                            data: "h_subtotal"
                        },
                        {
                            data: "h_tax"
                        },

                        {
                            data: "h_weight"
                        },
                        {
                            data: "h_discountitem"
                        },
                        {
                            data: "h_additionaldisc"
                        },
                        {
                            data: "h_qtyBonus"
                        }
                    ],
                    "columnDefs": [{
                            "targets": 9,
                            "data": null,
                            "defaultContent": "<button class='btn btn-sm btn-block btn-danger delRow' >Remove</button>"
                        },
                        {
                            "targets": [1, 10, 11, 12, 13, 14, 15, 16, 17, 18],
                            "visible": false
                        }
                    ]
                });

                $.ajax({
                    url: "<?php echo base_url(); ?>penjualan/getLineDetails",
                    type: "GET",
                    datatype: "json",
                    data: {
                        t_order_id: $("#t_order_id").val(),
                    },
                    success: function(e) {
                        const data = JSON.parse(e);
                        $.each(data, function(idx, val) {
                            addDetail(val);
                        })
                    }
                }).fail(function(a, b, c) {
                    console.log(c);
                });
            });

            function addDetail(data) {
                console.log(data.qtyBonus);
                $("#detailTable").DataTable().row.add({
                    "m_product_id": data.product.code + " - " + data.product.name,
                    "specification": "NO SPEC",
                    "qtyEntered": formatNumber(data.qty),
                    "priceEntered": formatNumber(data.price),
                    "subtotal": formatNumber(data.linenetamt),
                    "weight": formatNumber(data.weight),
                    "additionalDisc": formatNumber(data.discount * 1.0),
                    "h_product_id": data.product.id,
                    "h_qtyEntered": data.qty * 1.0,
                    "h_priceEntered": data.price * 1.0,
                    "discountItem": formatNumber(data.discountamt * 1.0),
                    "h_subtotal": data.linenetamt * 1.0,
                    "h_tax": data.taxamt * 1.0,
                    "h_weight": data.weight * 1.0,
                    "h_additionaldisc": data.discount * 1.0,
                    "h_discountitem": data.discountamt,
                    "qtyBonus": formatNumber(data.qtyBonus),
                    "h_qtyBonus": data.qtyBonus
                }).draw();
            }
            $("#detailTable").on('click', 'button.delRow', function(e) {
                e.preventDefault();
                var data = dTable.row($(this).parents('tr')).data();
                var subtotal = -(data.h_subtotal);
                dTable.row($(this).parents('tr')).remove().draw();
                calculate(subtotal);
            });



            $("#discUseAmtCheck").change(function() {
                $("#discUseAmt").toggle();
                $("#discUseCurrency").toggle();
            });

            $("#discCashCheck").change(function() {
                $("#discCashAmt").toggle();

            });

            $("#isBoronganCheck").change(function() {
                if ($('#isBoronganCheck').is(':checked')) {
                    $("#totalShippingAmt").prop('readonly', false);
                    $("#shippingAmt").prop('readonly', true);
                } else {
                    $("#totalShippingAmt").prop('readonly', true);
                    $("#shippingAmt").prop('readonly', false);
                }
            });


            $("#paymentTerm").change(function() {
                if ($(this).val() == "C") {
                    $("#dueGroup").hide();
                } else {
                    $("#dueGroup").show();
                }
                $("#discCashCheck").trigger('change');
            });

            /* Reserve for Payment Termain */
            // $("#paymentTerm").change(function(){
            //     if($(this).val()=="C"){
            //         $("#discCashCheck").prop('checked', true);
            //     }else{
            //         $("#discCashCheck").prop('checked', false);
            //     }
            //     $("#discCashCheck").trigger('change');
            // })

            $("#daysDue").change(function() {
                const [day, month, year] = $("#orderdate").val().split("-");
                var newDate = moment($("#orderdate").val(), "D-mm-yyyy").add('days', $(this).val());

                $("#dateDue").val(newDate.format("D-mm-Y"));
                checkSpecialDiscount();
            });
            $(document).ready(function() {
                const [day, month, year] = $("#orderdate").val().split("-");
                var newDate = moment($("#orderdate").val(), "D-mm-YYYY").add('days', $("#daysDue").val());
                $("#dateDue").val(newDate.format("D-mm-Y"));
                checkSpecialDiscount();

                if ($(this).val() == null) return;
                $.ajax({
                    url: "<?php echo base_url() ?>/penjualan/getPartnerAddress",
                    type: "GET",
                    data: {
                        id: $("#m_partner_id").val(),
                    },
                    success: function(e) {
                        $("#address").html(`
                            <option disabled selected>PILIH ALAMAT</option>
                        `);
                        const {
                            addresses,
                            partner
                        } = $.parseJSON(e);
                        $.each(addresses, function(idx, val) {
                            $("#address").append(`
                                <option data-city-id="${val.id}" value="${val.id}" ${val.id == $("#address_db").val() ? "selected": ""}> ${val.receiver_name} - ${val.city.name}</option>
                            `);
                        })
                        specialDisc = partner.special_discount;
                        checkSpecialDiscount();
                        $("#address").trigger('change');
                    }
                }).fail(function() {
                    alert("Gagal Mengambil Harga !!!");
                });
            });

            $("#address").on('change', function() {
                if ($(this).val() == null) return;
                $.ajax({
                        url: "<?php echo base_url() ?>/penjualan/getAddress",
                        type: "GET",
                        data: {
                            id: $(this).val(),
                        }
                    })
                    .done(function(addressRawData) {
                        const address = JSON.parse(addressRawData);
                        $("#alamat_kirim").val(address.receiver_address);
                        $("#addressCity").val(address.m_city_id);
                    })
                    .fail(function() {
                        alert("Tidak Ada Alamat ditemukan !!!");
                    });
                checkSpecialDiscount();

            });

            $("#qtyEntered").change(function() {
                $.ajax({
                        url: "<?php echo base_url() ?>/master/getBonusDiscount",
                        type: "GET",
                        data: {
                            qty: $(this).val(),
                            m_product_id: $("#item_id").val(),
                            m_shippingaddress_id: $("#address").val()
                        }
                    })
                    .done(function(data) {
                        const diskon = JSON.parse(data);

                        if (diskon > 0)
                            $("#additionalDisc").val(diskon.diskon)
                        else
                            $("#additionalDisc").val(0);
                    })
                    .fail(function() {});
            });

            $("#nopol").change(function() {
                $("#nopol").val($(this).val().toUpperCase());
            });
            $("#m_courier_id").change(function() {
                const val = $(this).val();
                const cityId = $('#address').find('option:selected').data('city-id');

                if (val > 0) {
                    $("div.shippingRow").show();
                    $("div.oaRow").hide();
                    $("#nopolGroup").show();
                    $("#driverGroup").show();
                    $("#isBorongan").show();
                } else {
                    $("div.shippingRow").hide();
                    $("div.oaRow").show();
                    $("#nopolGroup").hide();
                    $("#driverGroup").hide();
                    $("#isBorongan").hide();
                    $("#nopol").val("");
                    $("#driver").val("");
                }

                if (!$("#m_courier_id").val())
                    return;

                $.ajax({
                    url: "<?php echo base_url() ?>/penjualan/getCourierCost",
                    type: "POST",
                    data: {
                        id: $(this).val(),
                        cityId: cityId
                    },
                    success: function(e) {
                        const obj = $.parseJSON(e);
                        $.each(obj, function(index, value) {
                            $("#shippingAmt").val(value.price);
                            calculate(0);
                        });
                    },
                }).fail(function() {
                    alert("Gagal Mengambil Harga !!!");
                });
            });



            $("#orderdate").change(function() {
                var dateOrdered = $("#orderdate").val();
                $("#deliverydate, #dateacct").val(dateOrdered);
            });

            $("#qtyEntered,#priceEntered,#discountItem,#taxItem,#qtyBonus").change(function() {
                calculateModal();
            });

            function checkSpecialDiscount() {
                if (specialDisc && $("#daysDue").val() < 15)
                    $("#discCashCheck").prop('checked', true);
                else
                    $("#discCashCheck").prop('checked', false);
            }

            function validateModal(data) {
                if (data.qtyEntered == "" || data.priceEntered == "") {
                    alert('Pastikan Harga dan Qty terisi');
                    return false;
                }
                // else if (data.tax == "" || data.tax < 0) {
                //     alert('Pajak Minimal 0 %');
                //     return false;
                else if (data.discount == "") {
                    alert('Potongan Minimal 0');
                    return false;
                } else if (data.discount >= (data.qtyEntered * data.priceEntered)) {
                    alert('Potongan Tidak Boleh Melebihi SubTotal');
                    return false;
                } else if (data.item_id == 0) {
                    alert('Silahkan Pilih Product');
                    return false;
                }
                return true;
            }
            $("#discUseAmtCheck").change(function() {
                calculateDiscount();
            });

            $("#qtyOa, #totalWeightOa").change(function() {
                var Oa = unformatNumber($('#qtyOa').val());
                var weight = parseInt($("#totalWeightOa").val());
                $("#potonganoa").val(formatNumber(Oa * weight));
            });


            $("#discCashCheck").change(function() {
                calculateDiscount();
            });

            $("#discountpct").change(function() {
                calculateDiscount();
                calculate(0);
            })
            $("#item_id").change(function() {
                if ($("#item_id").val() == null)
                    return;
                $.ajax({
                    url: "<?php echo base_url() ?>/penjualan/getPrice",
                    type: "POST",
                    data: {
                        id: $("#item_id").val(),
                        address: $("#address").val()
                    },
                    success: function(e) {
                        var obj = $.parseJSON(e);
                        $.each(obj, function(index, value) {
                            console.log(value);
                            if (!value.unitprice)
                                $("#priceEntered").val(value.price);
                            else
                                $("#priceEntered").val(value.unitprice);
                            $("#qtyEntered").val(1);
                            $("#priceEntered").trigger("change");
                            $("#weightEntered").val(value.weight);
                        });
                    },
                }).fail(function() {
                    alert("Gagal Mengambil Harga !!!");
                });
            });

            $("#form-pesanan-penjualan").submit(function(event) {
                event.preventDefault();
                $(".numField").each(function(idx, value) {
                    $(this).val(unformatNumber($(this).val()));
                });
                if ($("#m_courier_id").val() > 0)
                    $("#usecourier").val("T");
                else
                    $("#usecourier").val("F");
                // if ($("#taxpct").val() === 0)
                $("#istaxuse").val("T");

                var data = {};
                $.each($("#form-pesanan-penjualan").serializeArray(), function(key, value) {
                    data[value.name] = value.value;
                });
                console.log(($("#detailTable").DataTable().rows().data().toArray()));
                $.ajax({
                    url: "<?php echo base_url(); ?>penjualan/updateSales",
                    type: "POST",
                    datatype: "json",
                    data: {
                        sales: JSON.stringify(data),
                        item: counter,
                        data: $("#detailTable").DataTable().rows().data().toArray(),
                        id: $("#t_order_id").val()
                    },
                    success: function(e) {
                        showSuccess();
                    }
                }).fail(function(a, b, c) {
                    console.log(c);
                });
            });

            function showSuccess() {
                Swal.fire({
                    title: 'Success',
                    text: "Data Berhasil Tersimpan !",
                    type: 'warning',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Okay!'
                }).then((result) => {
                    if (result.value) {
                        location.reload();
                    }
                })
            }
            $("#simpan-detail").click(function() {
                var data = {};
                $.each($("#form-detail-penjualan").serializeArray(), function(key, value) {
                    data[value.name] = value.value;
                });
                if (!validateModal(data)) return;
                var additionalDisc = data.additionalDisc * 1.0 * data.priceEntered * data.qtyEntered / 100;
                $("#detailTable").DataTable().row.add({
                    "m_product_id": $("#pro_" + data.item_id).text(),
                    "specification": "NO SPEC",
                    "qtyEntered": formatNumber(data.qtyEntered),
                    "priceEntered": formatNumber(data.priceEntered),
                    "subtotal": formatNumber(data.qtyEntered * data.priceEntered - data.discount - additionalDisc),
                    "weight": formatNumber(data.weight * data.qtyEntered),
                    "h_product_id": data.item_id,
                    "h_qtyEntered": data.qtyEntered,
                    "h_priceEntered": data.priceEntered,
                    "discountItem": formatNumber(data.discount),
                    "h_subtotal": (data.qtyEntered * data.priceEntered) - data.discount - additionalDisc,
                    "h_tax": data.tax,
                    "h_weight": (data.weight * data.qtyEntered),
                    "additionalDisc": formatNumber(additionalDisc),
                    "h_discountitem": data.discount,
                    "h_additionaldisc": additionalDisc,
                    "qtyBonus": formatNumber(data.qtyBonus),
                    "h_qtyBonus": data.qtyBonus
                }).draw();

                $("#addItemModal").modal("hide");
                $('#item_id').val(null).trigger('change');
                $("#form-detail-penjualan").trigger('reset');
                calculate((data.qtyEntered * data.priceEntered));
            });

            $("#discountamt, #taxpct, #advancepayamt, #totalWeight, #shippingAmt,#discountCashAmt, #qtyOa, #potonganoa,#totalWeightOa").change(function() {
                calculate(0);
            });

            function calculate(addValue) {
                var totalLines = 0.00;
                var potongan = unformatNumber($("#discountamt").val());
                var potonganoa = unformatNumber($("#qtyOa").val()) * $("#totalWeightOa").val();
                var cashDiscount = unformatNumber($("#discountCashAmt").val());
                // var pajak = $("#taxpct").val() / 100;
                var pajak = 0.0;

                // var totalWeight = $("#totalWeight").val();
                var totalWeight = 0.00;
                var additionalDisc = 0.00;
                var bonus = 0.00;

                /*  
                   Temporary Comment for Go - Live Implementation
                totalAkhir = totalAkhir + (pajak * totalAkhir) + (totalWeight*courierCost);
                */

                dTable.rows().every(function(idx, table, row) {
                    var data = this.data();
                    totalWeight = totalWeight + data.h_weight;
                    additionalDisc = additionalDisc + (data.h_additionaldisc * 1.0) + (data.h_discountitem * 1.0);
                    totalLines = totalLines + data.h_subtotal;
                });
                if (!$("#isBoronganCheck").prop('checked')) {
                    var courierCost = parseInt($("#shippingAmt").val()) * totalWeight * 1.0;
                    $("#totalShippingAmt").val(formatNumber(courierCost));
                }

                var totalAkhir = totalLines - potongan - cashDiscount - potonganoa;
                totalAkhir = totalAkhir + (pajak * totalAkhir) - additionalDisc;

                var kekurangan = totalAkhir;
                /*
                   Temporary Comment for Go - Live Implementation
                $("#totalShippingAmt").val(formatNumber(courierCost*totalWeight));

                 */


                $("#totallines").val(formatNumber(totalLines));
                $("#grandtotal").val(formatNumber(totalAkhir));
                $("#discountBonusAmt").val(formatNumber(additionalDisc));
                $("#totalWeight").val(totalWeight);
                $("#totalWeightOa").val(totalWeight);
                //$("#totalWeightOa").trigger('change');
                $("#potonganoa").val(formatNumber(potonganoa));


            }


            function calculateModal() {
                var subTotal = $("#qtyEntered").val() * $("#priceEntered").val();
                // var tax = $("#taxItem").val() / 100.0;
                var tax = 0.0;
                var discount = $("#discountItem").val();
                var totalItem = subTotal + (tax * subTotal) - discount;
                $("#discountBonus").val($("#qtyBonus").val() * $("#priceEntered").val());
                $("#subTotalItem").val(formatNumber(subTotal));
                $("#grandTotalItem").val(formatNumber(totalItem));
            }

            function calculateShippingAmt() {
                var shippingAMt = $("#totalShippingAmt").val(unformatNumber(courierCost));
            }

            function calculateDiscount() {
                var isPercentage = $("#discUseAmtCheck").prop('checked');
                var discount = unformatNumber($("#discountpct").val());
                var isCash = $("#discCashCheck").prop('checked');
                var subtotal = unformatNumber($("#totallines").val());
                if (isPercentage) {
                    $("#discountamt").val(discount * totalLines / 100);
                } else {
                    $("#discountamt").val($("#discountpct").val());
                }
                if (isCash) {
                    var $temp = 1.5 * subtotal / 100;
                    $("#discountCashAmt").val(formatNumber($temp));
                } else {
                    $("#discountCashAmt").val(0);
                }
                calculate(0);
            }

            $("#addDetail").click(function() {
                console.log(JSON.stringify($("#detailTable").DataTable().rows().data().toArray()));
            });
        </script>
    </section>
</div>