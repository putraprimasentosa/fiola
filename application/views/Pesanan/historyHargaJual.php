<div class="content-wrapper">
    <section class="content">
        <h2 style="margin-bottom: 20px">History Harga Jual</h2>
        <div class="row">
            <div class="col-md-12">
                <h5>Nama Item :</h5>
                <select name="m_product_id" class="form-control" id="m_product_id" required>
                    <option disabled selected value> -- Select Item --</option>
                    <?php
                    foreach ($products as $product) {
                        echo '<option value="' . $product["id"] . '">' . $product["name"] . '</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-12 date-picker input-daterange" data-date-format="dd/mm/yyyy">
                <h5>Rentang Tanggal :</h5>
                <input class="form-control" id="from" name="from">
                <span class="input-group-addon"> to </span>
                <input class="form-control" id="to" name="to"></div>
        </div>
        <br>
        <div class="text-center">
            <button class="btn btn-success" id="lihat-history" type="button">Lihat Histori</button>
        </div>

        <div id="history-table-container">
            <table class="table table-bordered" id="priceHistoryTable">
                <thead>
                <tr>
                    <th>
                        Pelanggan
                    </th>
                    <th>
                        No Transaksi
                    </th>
                    <th>
                        Tanggal
                    </th>
                    <th>
                        Harga
                    </th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </section>
</div>
<script src="<?php echo getNodeUrl('moment/moment.js') ?>"></script>
<script>
    $(document).ready(function () {
        $('#m_product_id').select2();
        const priceHistoryTable = $('#priceHistoryTable').DataTable({
            columns: [
                {data: 'name'},
                {data: 'documentno'},
                {data: 'orderdate'},
                {data: 'price'},
            ]
        });

        $(".input-daterange").datepicker({
            setDate: new Date(),
            autoclose: true,
            format: "dd-mm-yyyy",
            todayBtn: "linked"
        });

        function constructHistoryRequestPayload() {
            return {
                item_id: $("#m_product_id").val(),
                date_from: moment($("#from").val(), "DD-MM-YYYY").format("YYYY-MM-DD"),
                date_to: moment($("#to").val(), "DD-MM-YYYY").format("YYYY-MM-DD")
            };
        }

        $("#lihat-history").on("click", function () {
            $.get("<?php echo base_url(); ?>/Penjualan/getSalesPriceHistory", constructHistoryRequestPayload())
                .done(function (rawData) {
                    const data = JSON.parse(rawData);
                    priceHistoryTable.clear();
                    data.map(row => {
                        priceHistoryTable.row.add({
                            name: row.name,
                            documentno: row.documentno,
                            orderdate: moment(row.orderdate, "YYYY-MM-DD").format("DD-MM-YYYY"),
                            price: accounting.formatNumber(row.price)
                        });
                    });
                    priceHistoryTable.draw();

                })
                .fail(function () {
                    swal("Pengambilan Data Gagal", "Terjadi kesalahan pada sistem. Silahkan coba lagi.", "error");
                })
        })
    })
</script>
