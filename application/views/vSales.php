<div class="container-fluid">
    <form id="formSales">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="labelCustomer">Pelanggan</label>
                    <input type="text" class="form-control form-control-sm" value="<?php echo $order["customer"]->customer_name?>" readonly/>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="labelCustomer">Ekspedisi</label>
                    <input type="text" class="form-control form-control-sm" value="<?php echo $order["courier"]->courier_name?>" readonly/>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="labelCustomer">Marketplace</label>
                    <input type="text" class="form-control form-control-sm" value="<?php echo $order["commerce"]->name?>" readonly/>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm">
                <div class="form-group">
                    <label for="labelCustomer">Tanggal Transaksi</label>
                    <input type="date" id="datetrx" class="form-control form-control-sm" value="<?php echo $order["transaction_date"]; ?>" name="transaction_date" readonly></input>
                </div>
            </div>

            <div class="col-sm">
                <div class="form-group input-group-sm">
                    <label for="labelCustomer">Ongkos Kirim</label>
                    <div class="input-group-prepend ">
                        <input type="text" class="form-control form-control-sm" id="subtotal" name="couriercost" value="<?php echo $order["couriercost"]?>" readonly>
                        <span class="input-group-text " id="basic-addon1">/ Kilogram</span>
                    </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="form-group">
                    <label for="labelCustomer">Total Berat</label>
                    <input type="number" id="weight" name="totalweight" class="form-control form-control-sm" value ="<?php echo $order["totalWeight"]?>" readonly>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm">
            </div>
            <div class="col-sm">
            </div>
            <!-- <div class="col-sm">
                <div class="form-group input-group-sm">
                    <label for="labelCustomer">Sub Total</label>
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Rp.</span>
                        <input type="text" class="form-control form-control-sm" id="subtotal" name="subtotal" value="0" readonly>
                    </div>
                </div>
            </div> -->
            <div class="col-sm">
                <div class="form-group input-group-sm">
                    <label for="labelCustomer">Total Penjualan</label>
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Rp.</span>
                        <input type="text" class="form-control form-control-sm" id="grandtotal" name="grandtotal" value="<?php echo $order["grandtotal"]?>" readonly>
                    </div>
                </div>
            </div>
        </div>


    </form>
    <hr style="height:10px;border:1 px solid;">

    <div class="row align-items-end">
    </div>
    <table id="detailTable" class="table table-hovered">
        <thead>
            <th>No</th>
            <th>Nama Produk</th>
            <th>Jumlah</th>
            <th>Harga</th>
            <th>Total</th>
        </thead>
        <tbody>
                <?php
                    $counter = 1;
                    foreach($orderLine as $line) {
                        echo "<tr>";
                            echo "<td>".$counter."</td>";
                            echo "<td>".$line["product"]->product_name."</td>";
                            echo "<td>".$line["sales_qty"]."</td>";
                            echo "<td>".$line["unitprice"]."</td>";
                            echo "<td>".$line["lineamt"]."</td>";
                        echo "</tr>";
                        $counter++;
                    }
                ?>
        </tbody>
    </table>
    <hr>
<!--    <div class="row float-right">-->
<!--        <div class="col-sm-12">-->
<!--            <button type="button" id="submitForm" class="btn btn-block btn-info"><span class="fa fa-paste"></span> Cetak</button>-->
<!--        </div>-->
<!--    </div>-->
</div>

<script>
    var dTable = null;
    var counter = 1;
    $(document).ready(function() {
        $("#customer").select2();
        $("#dProd").select2();
        $("#courier").select2();
        $("#sales_party").select2();
        dTable = $("#detailTable").DataTable();
    });
</script>
