<style>
    .return-amount-editor {
        max-width: 120px;
    }

    .item-col {
        width: 20%;
    }

    .condition-header-col {
        width: 60%;
    }

    .condition-col {
        width: 15%
    }

    .m-t-20 {
        margin-top: 20px;
    }

    .invalid-row {
        background-color: #dc3545;
        color: white;
    }

    .no-padding-right {
        padding-right: 0 !important;
    }

    .return-description-editor {
        resize: none;
    }
</style>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center margin-bottom">Edit Retur Penjualan - Retur Barang</h2>
                <form id="return-main-info-form">
                    <div class="row margin-bottom">
                        <div class="row col-md-6">
                            <div class="col-md-12 form-group">
                                <label for="return-no" class="col-sm-4 control-label no-padding">
                                    No. Retur
                                </label>
                                <div class="col-sm-8 no-padding-right">
                                    <input id="return-no" name="returnNo" class="form-control" value="<?php echo $returnData->returnno ?>"
                                           required readonly
                                    />
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="partner" class="col-sm-4 control-label no-padding">Partner</label>
                                <div class="col-sm-8 no-padding-right">
                                    <select class="form-control no-padding-right" id="partner" required>
                                        <option value="" disabled selected>Pilih Partner</option>
                                        <?php
                                        foreach ($partners as $partner) {
                                            echo "
										<option value='".$partner['id']."'>".$partner['name']."</option>	
											";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="order-no" class="col-sm-4 control-label no-padding">No. Pesanan</label>
                                <div class="col-sm-8 no-padding-right">
                                    <select class="form-control no-padding-right" name="orderId" id="order-no" required>
                                        <option value="" disabled selected>Pilih Pesanan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="order-date" class="col-sm-4 control-label no-padding">
                                    Tanggal Pesanan
                                </label>
                                <div class="col-sm-8 no-padding-right">
                                    <input id="order-date" class="form-control" >
                                </div>
                            </div>
                        </div>
                        <div class="row col-md-6 no-padding-right">
                            <div class="col-md-12 form-group no-padding-right">
                                <label for="return-receipt-no" class="col-sm-4 control-label no-padding">
                                    No. Tanda Terima Retur
                                </label>
                                <div class="col-sm-8 no-padding-right">
                                    <input id="return-receipt-no" name="returnReceiptNo"
                                           class="form-control" value="<?php echo $returnData->returnreceiptno ?>" required
                                    />
                                </div>
                            </div>
                            <div class="col-md-12 form-group no-padding-right">
                                <label for="return-date" class="col-sm-4 control-label no-padding">
                                    Tanggal Retur
                                </label>
                                <div class="col-sm-8 no-padding-right">
                                    <input id="return-date" name="returnDate" class="form-control" value="value="<?php echo DateTime::createFromFormat('Y-m-d', $returnData->returndate)->format('d-m-Y') ?>" readonly />
                                </div>
                            </div>
                            <div class="col-md-12 form-group no-padding-right">
                                <label for="expedition-name" class="col-sm-4 control-label no-padding">
                                    Ekspedisi
                                </label>
                                <div class="col-sm-8 no-padding-right">
                                    <input id="expedition-name" class="form-control no-padding-right" value="<?php echo $returnData->order->courier ? $returnData->order->courier->name : NO_COURIER ?>" readonly />
                                </div>
                            </div>
                            <div class="col-md-12 form-group no-padding-right">
                                <label for="license-plate-no" class="col-sm-4 control-label no-padding">
                                    No. Polisi</label>
                                <div class="col-sm-8 no-padding-right">
                                    <input id="license-plate-no" class="form-control" name="licensePlateNo" value="<?php echo $returnData->vehiclelicenseplate ?>" required />
                                </div>
                            </div>
                        </div>
                </form>
            </div>
            <div class="row col-md-12">
                <table id="order-detail-table" class="table table-bordered" style="width: 100%">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th class="text-center item-col" style="vertical-align: middle">Nama Produk</th>
                        <th class="text-center item-col" style="vertical-align: middle">Harga Satuan</th>
                        <th class="text-center item-col" style="vertical-align: middle">Subtotal</th>
                        <th class="text-center item-col" style="vertical-align: middle">Pajak</th>
                        <th class="text-center item-col" style="vertical-align: middle">Jumlah Asli</th>
                        <th class="text-center item-col" style="vertical-align: middle">Jumlah Retur</th>
                        <th class="text-center item-col" style="vertical-align: middle">Keterangan Retur</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="row col-md-12 text-right" style="margin-top: 12px">
                <div class="bottom-nav-button-container">
                    <a class="btn btn-warning" href="<?php echo base_url('retur-penjualan'); ?>">
                        <i class="fa fa-arrow-left"></i> Kembali
                    </a>
                    <a id="save-SalesReturn" class="btn btn-success">
                        <i class="fa fa-save"></i> Simpan
                    </a>
                </div>
            </div>
        </div>
</div>
</section>
</div>

<script src="<?php echo getNodeUrl('moment/moment.js') ?>"></script>
<script>
    const returnDateElement = $('#return-date');
    const partnerElement = $('#partner');
    const orderElement = $('#order-no');

    partnerElement.select2();
    orderElement.select2();
    returnDateElement.val(moment().format('DD-MM-YYYY'));
    returnDateElement.datepicker({
        autoclose: true,
        clearBtn: true,
        format: INTENDED_DATE_FORMAT.toLowerCase()
    });

    const renderValueInput = (cell, cellData, rowIndex) => {
        $(cell).html(`
        <input type="number" class="form-control return-amount-editor accounting-enabled"
            id="return-amount-${rowIndex}" value="0"
        />
    `);
    };

    const renderTextArea = (cell, cellData, rowIndex) => {
        $(cell).html(`
        <textarea class="form-control return-description-editor"
            rows=2 id="return-description-${rowIndex}"></textarea>
    `);
    };

    $(document).ready(function () {
        const mainTable = $('#order-detail-table').DataTable({
            columns: [
                { data: 'id', visible: false },
                { data: 'itemName' },
                {
                    data: 'price',
                    class: 'text-right',
                    createdCell: renderNumericValue
                },
                { data: 'lineAmount', visible: false },
                { data: 'taxAmount', visible: false },
                {
                    data: 'qty',
                    class: 'qty-col text-right',
                    createdCell: renderNumericValue
                },
                {
                    data: 'returnAmount',
                    defaultContent: 0,
                    class: 'text-center',
                    createdCell: (cell, cellData, rowData, rowIndex) =>
                        renderValueInput(cell, null, rowIndex)
                },
                {
                    data: 'returnDescription',
                    defaultContent: '',
                    class: 'text-center',
                    createdCell: (cell, cellData, rowData, rowIndex) =>
                        renderTextArea(cell, null, rowIndex)
                }
            ],
            ordering: false,
            paging: false
        });

        function constructReturnDetails() {
            const tableData = mainTable.data();
            const tableNode = $('#order-detail-table');
            const items = [];

            for (let x = 1; x <= tableData.length; x++) {
                const returnAmount = unformatNumber(tableNode.find(`tbody tr:nth-child(${x}) .return-amount-editor`).val());
                const returnDescription = tableNode.find(`tbody tr:nth-child(${x}) .return-description-editor`).val();

                items.push({
                    id: tableData[x - 1].id,
                    qty: tableData[x - 1].qty,
                    tax: tableData[x - 1].taxAmount,
                    subtotal: tableData[x - 1].lineAmount,
                    returnDescription,
                    returnAmount
                });
            }

            return items;
        }

        function checkDetailsAreValid(detailData) {
            for (let x = 0; x < detailData.length; x++) {
                const detail = detailData[x];
                const isAmountExceedingOrLessThanZero = detail.returnAmount > detail.qty || detail.returnAmount < 0;
                if (isAmountExceedingOrLessThanZero) {
                    return false;
                }
            }

            return true;
        }

        function checkAllAmountsAreZero(detailData) {
            let numberOfZeros = 0;
            for (let x = 0; x < detailData.length; x++) {
                const detail = detailData[x];
                if (detail.returnAmount === 0) {
                    numberOfZeros++;
                }
            }

            return numberOfZeros === detailData.length;
        }

        function isFormInvalid() {
            const returnDetailData = constructReturnDetails();

            if (!checkDetailsAreValid(returnDetailData)) {
                invokeError(SWEETALERT.TEXT.RETURN.INVALID_DETAIL_AMOUNT);
                return true;
            }

            if (checkAllAmountsAreZero(returnDetailData)) {
                invokeError(SWEETALERT.TEXT.RETURN.ALL_AMOUNTS_ARE_ZERO);
                return true;
            }

            if (!validateFields('#return-main-info-form')) {
                invokeError(SWEETALERT.TEXT.RETURN.INVALID_REQUIRED_FIELDS);
                return true;
            }

            return false;
        }

        function mapOrderDetails(orderDetails) {
            return orderDetails.map(orderDetail => ({
                id: orderDetail.product.id,
                price: orderDetail.price,
                itemBrand: orderDetail.product.brand,
                itemName: orderDetail.product.name,
                lineAmount: parseInt(orderDetail.lineamt),
                taxAmount: parseInt(orderDetail.taxamt),
                qty: parseInt(orderDetail.qty)
            }));
        }

        $('#save-SalesReturn').on('click', function () {
            if (isFormInvalid()) {
                return;
            }

            const info = JSON.stringify(constructObjectFromFormData('#return-main-info-form'));
            const detail = JSON.stringify(constructReturnDetails());

            invokeLoading();

            $.ajax({
                url: '<?php echo base_url(); ?>retur-penjualan',
                type: 'POST',
                data: {
                    info,
                    detail
                }
            })
                .done(function (data) {
                    const result = JSON.parse(data);
                    dismissDialog();
                    invokeDialog({
                        title: SWEETALERT.TITLE.SUCCESS,
                        text: SWEETALERT.TEXT.RETURN.SAVE_SUCCESS,
                        okCallback: () => navigateTo(`<?php echo base_url(); ?>retur-penjualan/${result.id}`),
                        cancelCallback: () => navigateTo('<?php echo base_url(); ?>retur-penjualan')
                    });
                })
                .fail(function () {
                    dismissDialog();
                    invokeError(SWEETALERT.TEXT.RETURN.SAVE_FAILED);
                });
        });

        partnerElement.on('change', function () {
            const id = $(this).val();

            if (!id) {
                return;
            }

            invokeLoading();
            orderElement.html(`<option value="" disabled selected>${SELECT_ORDER_PLACEHOLDER}</option>`);

            $.get({
                url: `<?php echo base_url(); ?>partner/${id}/order`,
                data: {
                    excludeReturn: true
                }
            })
                .done(function (data) {
                    const orders = JSON.parse(data);
                    orders.forEach(order => {
                        orderElement.append(`
			    	<option value="${order.id}">
							${order.documentno} - ${order.shipping_address.receiver_name}
						</option>
			    `)
                    });
                    orderElement.trigger('change');
                    dismissDialog();
                })
                .fail(function (data) {
                    dismissDialog();
                    invokeError(SWEETALERT.TEXT.RETURN.ERROR_GET_ORDER);
                });
        });

        orderElement.on('change', function () {
            const id = $(this).val();

            if (!id) {
                return;
            }

            mainTable.clear().draw();
            invokeLoading();

            $.get({
                url: `<?php echo base_url(); ?>penjualan/getOrder/${id}`
            })
                .done(function (data) {
                    const { orderInfo, orderDetail } = JSON.parse(data);
                    const courierName = orderInfo.courier && orderInfo.courier.name ? orderInfo.courier.name : NO_COURIER;
                    const licenseNo = orderInfo.nopol;
                    const formattedOrderDate = moment(orderInfo.orderdate, DB_DATE_FORMAT).format(INTENDED_DATE_FORMAT);
                    const mappedOrderDetails = mapOrderDetails(orderDetail);

                    $('#expedition-name').val(courierName);
                    $('#order-date').val(formattedOrderDate);
                    $('#license-plate-no').val(licenseNo);
                    mainTable.rows.add(mappedOrderDetails).draw();
                    dismissDialog();
                })
                .fail(function () {
                    dismissDialog();
                    invokeError(SWEETALERT.TEXT.RETURN.ERROR_GET_ORDER);
                })
        });

        mainTable.on('change blur', '.return-amount-editor', function () {
            const row = $(this).parents('tr');
            const originalQty = parseInt(row.find('.qty-col').text());
            const returnAmount = $(this).val();

            if (returnAmount > originalQty || returnAmount < 0) {
                row.addClass('invalid-row');
            } else {
                row.removeClass('invalid-row');
            }
        });
    })
</script>
