<div class="content-wrapper">
	<section class="content">
		<h2 class="margin-bottom">Retur Penjualan</h2>
		<a href="<?php echo base_url('/retur-penjualan/tambah'); ?>"
			 class="btn btn-info"
			 style="margin-bottom: 20px;"
		>
			<i class="fa fa-plus"></i> Tambah
		</a>
		<table id="SalesReturn-table" class="table" style="width: 100%">
			<thead>
			<tr>
				<th>No. Retur</th>
				<th>Tanggal</th>
				<th>No. Nota</th>
				<th>Nama Toko</th>
				<th>Total Penjualan</th>
				<th>Total Nilai Retur</th>
				<th>Total Nilai Klaim</th>
				<th>Cetak Nota</th>
				<th>Klaim</th>
			</tr>
			</thead>
			<tbody>
			<?php
			foreach ($data as $item) {
				echo "
					<tr>
							<td>$item->returnno</td>
							<td>".
								DateTime::createFromFormat('Y-m-d', $item->returndate)->format('d-m-Y')
						."</td>
							<td>".$item->order->documentno."</td>
							<td>".$item->order->shippingaddress->receiver_name."</td>
							<td class='text-right'>
									Rp ".number_format($item->order->grandtotal, 0, ',', '.')."
							</td>
							<td class='text-right'>
									Rp ".number_format($item->totalreturnvalue, 0, ',', '.')."
							</td>
							<td class='text-right'>";

				if (isset($item->claim)) {
					echo "Rp ".number_format($item->claim->totalreturnclaim, 0, ',', '.');
				} else {
					echo "N/A";
				}

				echo "
							</td>
							<td class='text-center'>
									<a class='btn btn-default' href='".base_url('retur-penjualan/'.$item->id)."'>
										<i class='fa fa-print'></i> Cetak
									</a>
							</td>
							<td class='text-center'>
				";
				if (isset($item->claim)) {
					echo "
								<a class='btn btn-default' href='".base_url('klaim-retur-penjualan/'.$item->claim->id)."'>
									<i class='fa fa-print'></i> Cetak
								</a>
					";
				}
				else {
					echo "
								<a class='btn btn-default' href='".base_url('klaim-retur-penjualan/tambah/'.$item->id)."'>
									<i class='fa fa-plus'></i> Buat
								</a>
					";
				}

				echo "
							</td>
					</tr>
				";
			}
			?>
			</tbody>
		</table>
	</section>
</div>

<script>
  $(document).ready(function () {
    $('#SalesReturn-table').DataTable({
			ordering: false
		});
  })
</script>
