<style>
	thead td {
		text-align: center;
		font-weight: bold;
	}
</style>

<div class="content-wrapper">
	<section class="content">
		<h2 style="margin-bottom: 20px">Data Retur Penjualan</h2>
		<div class="row" style="font-size: 12pt">
			<div class="col-md-12 form-group">
				<div class="row col-md-6">
					<div class="col-md-6 no-padding">
						Nomor Retur
					</div>
					<div class="col-md-6">
						: <?php echo $returnData->returnno ?>
					</div>
				</div>
				<div class="row col-md-6">
					<div class="col-md-6 no-padding">
						No. Tanda Terima Retur
					</div>
					<div class="col-md-6">
						: <?php echo $returnData->returnreceiptno ?>
					</div>
				</div>
			</div>
			<div class="col-md-12 form-group">
				<div class="row col-md-6">
					<div class="col-md-6 no-padding">
						Tanggal Retur
					</div>
					<div class="col-md-6">
						: <?php echo DateTime::createFromFormat('Y-m-d', $returnData->returndate)->format('d-m-Y') ?>
					</div>
				</div>
				<div class="row col-md-6">
					<div class="col-md-6 no-padding">
						Nama Ekspedisi
					</div>
					<div class="col-md-6">
						: <?php echo $returnData->order->courier ? $returnData->order->courier->name : NO_COURIER ?>
					</div>
				</div>
			</div>
			<div class="col-md-12 form-group">
				<div class="row col-md-6">
					<div class="col-md-6 no-padding">
						Nomor Faktur
					</div>
					<div class="col-md-6">
						: <?php echo $returnData->order->documentno ?>
					</div>
				</div>
				<div class="row col-md-6">
					<div class="col-md-6 no-padding">
						Nomor Polisi Ekspedisi
					</div>
					<div class="col-md-6">
						: <?php echo $returnData->vehiclelicenseplate ?>
					</div>
				</div>
			</div>
		</div>
		<section>
			<div>
				<table id="return-info-data-table" class="table table-bordered" style="width: 100%">
					<thead>
					<tr>
						<th>Nama Barang</th>
						<th>Keterangan</th>
						<th>Jumlah Barang</th>
						<th>Harga Satuan</th>
						<th>Subtotal</th>
					</tr>
					</thead>
					<tbody>
					<?php
					foreach ($returnDetailData as $detail) {
						echo '
								<tr>
									<td>'.$detail['item']['name'].'</td>
									<td>'.$detail['returnDescription'].'</td>
									<td class="text-center">'.number_format($detail['totalqty'], 0, ', ', '.').'</td>
									<td class="text-right">Rp'.number_format($detail['price'], 0, ', ', '.').'</td>
									<td class="text-right">Rp '.number_format($detail['subtotalreturn'], 0, ', ', '.').'</td>
								</tr>
								';
					}
					?>
					</tbody>
					<tfoot>
					<tr class="text-bold">
						<td colspan="4" class="text-right">Total Nilai Retur</td>
						<td class="text-right">Rp <?php echo number_format($returnData->totalreturnvalue, 0, ',', '.'); ?></td>
					</tr>
					</tfoot>
				</table>
			</div>
			<div class="bottom-nav-button-container">
				<a href="<?php echo base_url('retur-penjualan') ?>" class="btn btn-warning">
					<i class="fa fa-arrow-left"></i> Kembali
				</a>
				<a target="_blank" href="<?php echo base_url('ReturPenjualan/printorder/'.$returnData->id) ?>" class="btn btn-success">
					<i class="fa fa-print"></i> Cetak Nota Retur
				</a>
			</div>
		</section>
	</section>
</div>
