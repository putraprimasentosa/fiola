<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<h2 style="margin-bottom: 20px">Laporan <?php echo $reportType; ?> </h2>
			</div>
		</div>
		<div class="row">
			<div class="form-group container">
				<label for="date-from">Tanggal Mulai</label>
				<input type="email" class="form-control" id="date-from" placeholder="Tanggal Mulai">
				<small class="form-text text-muted">Perkiraan tanggal dimulai.</small>
			</div>
			<div class="form-group container">
				<label for="date-to">Tanggal Selesai</label>
				<input type="email" class="form-control" id="date-to" placeholder="Tanggal Selesai">
				<small class="form-text text-muted">Perkiraan tanggal akhir.</small>
			</div>
		</div>
		<div class="form-group container">
			<button type="submit" id="printReport" class="btn btn-info btn-block">Cetak Laporan</button>
		</div>
	</section>
</div>

<script src="<?php echo getNodeUrl('moment/moment.js') ?>"></script>
<script>
	$(document).ready(function () {
		const dateInputFields = $('#date-from, #date-to');

		dateInputFields.datepicker({
			orientation: 'bottom',
			autoclose: true,
			format: INTENDED_DATE_FORMAT.toLowerCase()
		});
		dateInputFields.datepicker('setDate', moment().format(INTENDED_DATE_FORMAT));

		$('#printReport').on('click', function () {
			const dateFrom = moment($('#date-from').val(), INTENDED_DATE_FORMAT).format(DB_DATE_FORMAT);
			const dateTo = moment($('#date-to').val(), INTENDED_DATE_FORMAT).format(DB_DATE_FORMAT);
			const url = '<?php echo base_url('report/printReturn') ;?>';

			window.open(`${url}?date1=${dateFrom}&date2=${dateTo}`);
		});
	})
</script>
