<div class="content-wrapper">
    <section class="content">
        <h2 style="margin-bottom: 20px">Content Goes Here</h2>
        <table id="sample-data-table" class="table table-bordered" style="width: 100%">
            <thead>
            <tr>
                <td>No</td>
                <td>Title</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>Some Data</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Some Other Data</td>
            </tr>
            </tbody>
        </table>
    </section>
</div>

<script>
  $(document).ready(function () {
    $('#sample-data-table').DataTable();
  })
</script>