<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h2 style="margin-bottom: 20px">Silahkan Pilih Tanggal untuk Laporan <?php echo $reportType; ?> </h2>
		</div>
	</div>
	<div class="row">
		<div class="form-group container">
			<label>Period</label>
			<select class="form-control form-control-sm" id="period_id">
				<?php foreach ($target as $target) {
					echo '<option value="' . $target['id'] . '"> ' . $target['period'] . '</option>';
				} ?>
			</select>
			<small class="form-text text-muted">Periode Target.</small>
		</div>
	</div>

	<div class="form-group container">
		<button type="submit" id="printReport" class="btn btn-info btn-block">Print Report</button>
	</div>

</div>

<script src="<?php echo getNodeUrl('moment/moment.js') ?>"></script>
<script>
	$('#printReport').click(function() {
		window.open('<?php echo base_url() . "report/" . $type . "?PERIOD=" ?>' + $('#period_id').val());
	});
</script>