<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<h2 style="margin-bottom: 20px">Silahkan Pilih Tanggal untuk Tanda Terima Surat Jalan</h2>
			</div>
		</div>
		<div class="row">
			<div class="form-group container">
				<label>Date From</label>
				<input type="email" class="form-control" id="date1" placeholder="Enter Date">
				<small class="form-text text-muted">Perkiraan tanggal dimulai.</small>
			</div>
			<div class="form-group container">
				<label>Date To</label>
				<input type="email" class="form-control" id="date2" placeholder="Enter Date">
				<small class="form-text text-muted">Perkiraan tanggal akhir.</small>
			</div>
		</div>

		<div class="form-group container">
			<button type="submit" id="printReport" class="btn btn-info btn-block">Print Report</button>
		</div>


	</section>
</div>

<script src="<?php echo getNodeUrl('moment/moment.js') ?>"></script>
<script>
	$('#printReport').click(function () {
		window.open('<?php echo base_url()."report/printTandaTerimaSuratJalan?date1=" ?>' + $('#date1').val() + '&date2=' + $('#date2').val());
	});

	$(document).ready(function () {
		const dateInputFields = $('#date1, #date2');

		dateInputFields.datepicker({
			orientation: 'bottom',
			autoclose: true,
			format: INTENDED_DATE_FORMAT.toLowerCase()
		});
		dateInputFields.datepicker('setDate', moment().format(INTENDED_DATE_FORMAT));
	})
</script>
