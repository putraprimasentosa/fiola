<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Login</title>

    <!-- Custom fonts for this template-->
    <link rel="stylesheet" type="text/css" href="<?php echo getNodeUrl('@fortawesome/fontawesome-free/css/all.min.css') ?>">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/sb-admin-2.css') ?>">
    <style>
        body {
            Background-color: #F5F5F5;
        }
    </style>

</head>

<body>
    <?php
    if (isset($this->session->loginMessage)) {
        if ($this->session->loginMessage == "error") {
            echo "Login gagal! username dan password salah!";
        } else if ($this->session->loginMessage == "logout") {
            echo "Anda telah berhasil logout";
        } else if ($this->session->loginMessage == "belum_login") {
            echo "Anda harus login untuk mengakses halaman utama";
        }
    }
    ?>

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-6 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5" style="background-size:auto;background:url('<?php echo base_url('assets/images/login.jpg')?>')">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block ">
                                <!-- <img src="<?php echo base_url('assets/images/login.jpg')?>" width="" height="fix"> -->
                            </div>
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">
                                            <img src="<?php echo base_url('assets/images/logo-login.png')?>" width="60px" height="60px">
                                            <b>Royalty Natural Indonesia</b></h1>
                                    </div>
                                    <form class="user" method="post" action="<?php echo base_url('Home/verifyUser') ?>">
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-user" name="username" id="username" aria-describedby="idkar" placeholder="ID Karyawan">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="Password">
                                        </div>
                                        <button class="btn btn-primary btn-user btn-block">
                                            <font size="4"><b>Login </b> </font>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js') ?>"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?php echo base_url('assets/js/jquery.easing.min.js') ?>"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?php echo base_url('assets/js/sb-admin-2.min.js') ?>"></script>

</body>

</html>