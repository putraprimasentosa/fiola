<?php
	$realization_product = $real;
?>
<script type="text/javascript">
	Highcharts.chart('container4', {
		chart: {
			type: 'column'
		},
		title: {
			text: $("#chart5head").text()
		},
		legend: {
			layout: 'vertical',
			align: 'left',
			verticalAlign: 'top',
			x: 150,
			y: 50,
			floating: true,
			borderWidth: 1,
			backgroundColor:
				Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
		},

		xAxis: {
			categories: [
				<?php foreach ($realization_product as $data) : ?>
				<?php echo "'".$data['product_name']."',"; ?>
				<?php endforeach; ?>
			],

		},
		yAxis: {
			title: {
				text: 'Values'
			}
		},

		credits: {
			enabled: false
		},
		plotOptions: {
			areaspline: {
				fillOpacity: 0.5
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}</b> {point.unit}<br/>'
		},
		series: [{
			name: 'Target',
			data :[
				<?php foreach ($realization_product as $data) : ?>
				<?php echo $data['target_qty'].","; ?>
				<?php endforeach; ?>
			]
		}, {
			name: 'Realization',
			data :[
				<?php foreach ($realization_product as $data) : ?>
				<?php echo $data['sales_quantity'].","; ?>
				<?php endforeach; ?>
			]
		}]
	});
</script>
