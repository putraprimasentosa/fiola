<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->

	<!-- Content Row -->
	<div class="row">
		<div class="col-xl-12  col-lg-7">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<?php
						$months = ["January","February","Maret","April","May","June","July","August","September","October","November","December"];
					?>
					<h6 class="m-0 font-weight-bold text-primary">Capaian Penjualan</h6>
					<h6 id="chart5head" class="m-0 font-weight-bold text-primary" hidden>Capaian Penjualan (<?php echo $months[date('m')*1-1]?>)</h6>
				</div>
				<!-- Card Body -->
				<div class="card-body">
					<div class="chart-area">
						<div id="container4" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
						<?php include('chart5.php'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<div class="card shadow mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h6 class="m-0 font-weight-bold text-primary">Penjualan</h6>
				</div>
				<div class="card-body">
					<table class="table table-hover" id="revenueTable">
						<thead>
						<th>Pelanggan</th>
						<th class="text-right">Jumlah Produk</th>
						<th class="text-right">Total Penjualan (Rp)</th>
						</thead>
						<tbody>
						<?php
						foreach ($custchart as $sc) {
							echo '<tr>';
							echo '<td>'.$sc['name'].'</td>';
							echo '<td class="text-right">'.number_format($sc['qty']).'</td>';
							echo '<td class="text-right">'.number_format($sc['total']).'</td>';
							echo '</tr>';
						}
						?>
						</tbody>
					</table>

				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="card shadow mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h6 class="m-0 font-weight-bold text-primary">Penjualan Produk</h6>
				</div>
				<div class="card-body">
					<table class="table table-hover" id="cityRevenueTable">
						<thead>
						<th>Kode Produk</th>
						<th>Nama Produk</th>
						<th class="text-right">Total Penjualan (Rp)</th>
						</thead>
						<tbody>
						<?php
						foreach ($soldchart as $sc) {
							echo '<tr>';
							echo '<td>'.$sc['code'].'</td>';
							echo '<td>'.$sc['name'].'</td>';
							echo '<td class="text-right">'.number_format($sc['total']).'</td>';
							echo '</tr>';
						}
						?>
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>


</div>
<!-- /.container-fluid -->

<script>
	$("#earning").append('Rp. ' + formatNumber($("#earning_s").val()), ',-');
	$(document).ready(function () {
		$("#revenueTable").DataTable({});
		$("#cityRevenueTable").DataTable();
	});
</script>
