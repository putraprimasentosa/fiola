<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->

	<!-- Content Row -->
	<div class="row">
		<?php
		echo '<script>';
		echo 'var weeklychart = ' . json_encode($weeklychart);
		echo ';var soldchart = ' . json_encode($soldchart) . ';';
		echo 'var regionchart = ' . json_encode($regionChart) . ';';
		echo '</script>';
		$earning = (json_decode(json_encode($earning->result()[0]), true));
		?>

	</div>
	<div class="row" id="detailAccordion">
		<div class="col-sm-4">
			<div class="row">
				<div class="col-sm-12 mb-4">
					<div class="card border-left-primary shadow h-100 py-2">
						<div class="card-body">
							<div class="row no-gutters align-items-center">
								<div class="col mr-2">
									<div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><a href="#"
																											  data-toggle="collapse"
																											  data-target="#collapseEarning">Bulan
											Berjalan
										</a>
									</div>
									<?php

									?>
									<select class="form-control" id="selectedMonth">
										<?php
										$months = ["January", "February", "Maret", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
										$lastMonth = date("m", date_add(date_create(date("Y-m-d")), date_interval_create_from_date_string("-1 Year"))) * 1;
										$curMonth = date("m") * 1;
										for ($i = 0; $i < 13; $i = $i + 1) {
											echo '<option value="' . ($lastMonth + 1) . '|' . (($lastMonth > curMonth && $i < 12) || $i == 0 ? date("Y") - 1 : date("Y")) . '">' . $months[$lastMonth++] . ' - ' . (($lastMonth > curMonth && $i < 12) || $i == 0 ? date("Y") - 1 : date("Y")) . '</option>';
											if ($lastMonth > 11)
												$lastMonth = 0;
										}
										?>
									</select>
								</div>
								<div class="col-auto">
									<i class="fas fa-calendar fa-2x text-gray-300"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 mb-4">
					<div class="card border-left-primary shadow h-100 py-2">
						<div class="card-body">
							<div class="row no-gutters align-items-center">
								<div class="col mr-2">
									<div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><a href="#"
																											  data-toggle="collapse"
																											  data-target="#collapseEarning">Pendapatan
											(Bulan ini)</a>
									</div>
									<input type="number" id="earning_s"
										   value="<?php if (isset($earning) && sizeof($earning) > 0) echo $earning['gt'] * 1 ?>"
										   hidden/>
									<div class="h5 mb-0 font-weight-bold text-gray-800" id="earning"></div>
								</div>
								<div class="col-auto">
									<i class="fas fa-calendar fa-2x text-gray-300"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 mb-4">
					<div class="card border-left-success shadow h-100 py-2">
						<div class="card-body">
							<div class="row no-gutters align-items-center">
								<div class="col mr-2">
									<div class="text-xs font-weight-bold text-success text-uppercase mb-1"><a href="#"
																											  data-toggle="collapse"
																											  data-target="#collapseProduct">Produk
											Terlaku
											(Bulan ini)</a>
									</div>
									<div
											class="h5 mb-0 font-weight-bold text-gray-800"><?php if (isset($best_product) && sizeof($best_product) > 0) {
											$bestproduct = $best_product[0];
											echo $bestproduct['name'] . ' - ' . $bestproduct['qty'] . ' ' . $bestproduct['uom'];
										} else {
											echo '-';
										} ?></div>
								</div>
								<div class="col-auto">
									<i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 mb-4">
					<div class="card border-left-success shadow h-100 py-2">
						<div class="card-body">
							<div class="row no-gutters align-items-center">
								<div class="col mr-2">
									<div class="text-xs font-weight-bold text-success text-uppercase mb-1"><a href="#"
																											  data-toggle="collapse"
																											  data-target="#collapseCommerce">Marketplace
											terlaku
											(Bulan ini)</a>
									</div>
									<div
											class="h5 mb-0 font-weight-bold text-gray-800"><?php
										if (isset($best_party) && sizeof($best_party) > 0) {
											$bparty = $best_party[0];
											echo $bparty['name'];
										} else echo '-';
										?></div>
								</div>
								<div class="col-auto">
									<i class="fas fa-truck fa-2x text-gray-300"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="col-sm-8">
			<div id="collapseEarning" class="collapse show" data-parent="#detailAccordion">
				<table class="table table-hover" id="earningTable">
					<thead>
					<th>Pelanggan</th>
					<th class="text-right">Pendapatan (Rp)</th>
					</thead>
					<tbody>
					<?php
					foreach ($customer_earning as $c) {
						echo '<tr>';
						echo '<td>' . $c['name'] . '</td>';
						echo '<td class="text-right"> ' . number_format($c['total']) . '</td>';
						echo '</tr>';
					}
					?>
					</tbody>
				</table>
			</div>
			<div id="collapseProduct" class="collapse" data-parent="#detailAccordion">
				<table class="table table-hover" id="productTable">
					<thead>
					<th>Produk</th>
					<th class="text-right">Jumlah</th>
					<th class="text-right">Total (Rp)</th>
					</thead>
					<tbody>
					<?php
					foreach ($best_product as $bp) {
						echo '<tr>';
						echo '<td>' . $bp['name'] . '</td>';
						echo '<td class="text-right">' . number_format($bp['qty']) . '</td>';
						echo '<td class="text-right"> ' . number_format($bp['total']) . '</td>';
						echo '</tr>';
					}
					?>
					</tbody>
				</table>
			</div>
			<div id="collapseCommerce" class="collapse" data-parent="#detailAccordion">
				<table class="table table-hover" id="commerceTable">
					<thead>
					<th>Marketplace</th>
					<th class="text-right">Jumlah Transaksi</th>
					<th class="text-right">Penjualan (Rp)</th>
					</thead>
					<tbody>
					<?php
					foreach ($best_party as $bp) {
						echo '<tr>';
						echo '<td>' . $bp['name'] . '</td>';
						echo '<td class="text-right">' . number_format($bp['qty']) . '</td>';
						echo '<td class="text-right"> ' . number_format($bp['total']) . '</td>';
						echo '</tr>';
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- Content Row -->

	<div class="row">
		<div class="col-sm-12">
			<div class="card shadow mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h6 class="m-0 font-weight-bold text-primary">
						<div class="row">
							<div class="col">
								Penjualan Produk
							</div>

						</div>

					</h6>
				</div>
				<div class="card-body">
					<div class="row mb-4">
						<div class=" offset-sm-3 col-sm-3">
							<input type="email" class="form-control" id="period1" placeholder="Periode Awal">
						</div>
						<div class="col-sm-3">
							<input type="email" class="form-control" id="period2" placeholder="Periode Akhir">
						</div>
					</div>

				</div>
				<div class="row">

					<div class="col">
						<?php
						$date1 = 	new DateTime(date('M-Y',strtotime('Jan-1995')));
						$date2 = new DateTime(date('M-Y',strtotime('Jan-1996')));
						$counter = $date1<$date2 ? date_diff($date1,$date2)->format('%y')*12+date_diff($date1,$date2)->format('%m') : 0;
						print_r($counter);
						for($i = 0; $i<$counter; $i++){
							print_r(date_format(date_add($date1,date_interval_create_from_date_string("1 month")),'Y-m-t'));

						}
						?>
						<div class="chart-area ssum">
							<div id="trendProductContainer"
								 style="min-width: 310px; height: 300px;margin: 0 auto"></div>
							<?php include('trendProductChart.php'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="card shadow mb-4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">
					Penjualan Terbaik <span class="ssum"><a href="javascript:void(0)"
															role="button">(Click for Detail)</a></span><span
							class="sdet collapse"><a href="javascript:void(0)"
													 role="button">(Click for Summary)</a></span>
				</h6>
			</div>
			<div class="card-body">
				<div class="chart-area ssum">
					<div id="topSalesContainer" style="min-width: 310px; height: 300px;margin: 0 auto"></div>
					<?php include('topSalesChart.php'); ?>
				</div>
				<div class="chart-area collapse sdet">
					<div id="topSalesContainerd" style="min-width: 310px; height: 300px;margin: 0 auto"></div>
					<?php include('topSalesChartd.php'); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="card shadow mb-4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">
					Capaian Penjualan
				</h6>
			</div>
			<div class="card-body">
				<div class="chart-area">
					<div>
						<select id="salesPicker" class="form-control form-control-sm">
							<?php
							foreach ($sale as $value) {
								echo '<option value="' . $value['id'] . '">' . $value['admin_name'] . '</option>';
							}
							?>
						</select>
					</div>
					<div id="salesRealizationContainer"
						 style="min-width: 310px; height: 300px;margin: 0 auto"></div>
					<?php include('salesRealizationChart.php'); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xl-6  col-lg-7">
		<div class="card shadow mb-4">
			<!-- Card Header - Dropdown -->
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Produk Terlaku <span class="csum"><a
								href="javascript:void(0)" role="button">(Click for Detail)</a></span><span
							class="cdet collapse"><a href="javascript:void(0)"
													 role="button">(Click for Summary)</a></span></h6>
			</div>
			<!-- Card Body -->
			<div class="card-body">
				<div class="chart-area csum">
					<div id="container2" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
					<?php include('chart2.php'); ?>
				</div>
				<div class="chart-area collapse cdet">
					<div id="container2d" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
					<?php include('chart2d.php'); ?>
				</div>
			</div>


		</div>
	</div>
	<div class="col-xl-6  col-lg-7">
		<div class="card shadow mb-4">
			<!-- Card Header - Dropdown -->
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Penjualan per Wilayah</h6>
			</div>
			<!-- Card Body -->
			<div class="card-body">
				<div class="chart-area">
					<div id="container3" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
					<?php
					include('chart3.php');
					?>
				</div>
			</div>


		</div>
	</div>
</div>

<!-- Content Row -->


</div>
<!-- /.container-fluid -->
<script src="<?php echo getNodeUrl('moment/moment.js') ?>"></script>
<script>
	const dateInputFields = $('#period1, #period2');
	$("#earning").append('Rp. ' + formatNumber($("#earning_s").val()), ',-');
	$(document).ready(function () {
		$("#earningTable").DataTable();
		$("#productTable").DataTable();
		$("#commerceTable").DataTable();

		let monthGet = "<?= isset($_GET['month']) ? ($_GET['month']) == "" ? 0 : $_GET['month'] . '' : 0;?>";
		console.log(monthGet);
		console.log($("#selectedMonth").val());


		$("#selectedMonth").val(monthGet);

		if (monthGet == 0)
			$("#selectedMonth").prop('selectedIndex', 12);

		dateInputFields.datepicker({
			orientation: 'bottom',
			autoclose: true,
			format: INTENDED_DATE_FORMAT.toLowerCase(),
			format: "MM-yyyy",
			viewMode: "months",
			minViewMode: "months"
		});
		dateInputFields.datepicker('setDate', moment().format(INTENDED_DATE_FORMAT));

	});

	$("#selectedMonth").on('change', function (e) {
		window.location.href = "?month=" + $(this).val();
	});

	$("span.ssum").click(function (e) {
		$(".sdet").toggle('show');
		$(".ssum").toggle('hide');
	});
	$("span.sdet").click(function (e) {
		$(".ssum").toggle('show');
		$(".sdet").toggle('hide');
	});

	$("span.csum").click(function (e) {
		$(".cdet").toggle('show');
		$(".csum").toggle('hide');
	});
	$("span.cdet").click(function (e) {
		$(".csum").toggle('show');
		$(".cdet").toggle('hide');
	});


	$("#period1, #period2").on('change',function(e){

		$.ajax({
			url: "<?php echo base_url(); ?>home/getTrendProduct",
			type: "GET",
			datatype: "json",
			data: {
				date1: $("#period1").val(),
				date2: $("#period2").val(),
			},
		})
				.done(function(e) {
					const data = [];
					console.log('done');
					// console.log(e);
					$.each(JSON.parse(JSON.parse(e).qty	), function (d, v) {
							data.push({
								name: d,
								data: v
							});
					});

					hc.update({

						xAxis: {
							//categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
							categories:
									(JSON.parse(JSON.parse(e).month))
						},
						series:
							data

					}, true, false, {
						duration: 800
					});
				})
	});
</script>

