<script type="text/javascript">

	const regionChartDrilldown = JSON.parse(`<?php echo $regionChartDrilldown ?>`);
	const regionChartDrilldownFilter = JSON.parse(`<?php echo $regionChartDrilldown ?>`);
	const cityChartDrilldown = JSON.parse(`<?= $regionChartDrilldown2?>`);
	const regionChart = JSON.parse('<?php echo $regionChart ?>');

	const regionDrilldownData = [];
	const cityDrilldownData = [];
	regionChart.forEach(region => {
		const cityDrilldownData = regionChartDrilldown.filter(salesByRegionData => {
			return region.name === salesByRegionData.name
		}).map(filteredSalesByRegion => {

			return [filteredSalesByRegion.drilldown, parseInt(filteredSalesByRegion.y), filteredSalesByRegion.drilldown+'|'+filteredSalesByRegion.province_id+"|"+filteredSalesByRegion.name]
		});

		const regionDrilldownResult = {
			name: region.drilldown,
			id: region.name,
			data: cityDrilldownData,
			keys: ['name', 'y', 'drilldown']
		};
		regionDrilldownData.push(regionDrilldownResult);

		regionChartDrilldownFilter.forEach(province => {
			const drilldown = cityChartDrilldown.filter(salesByCity => {
				return salesByCity.province_id === province.province_id && province.name === salesByCity.product_name
			}).map(filteredProductByCity => {
				return [filteredProductByCity.name, parseInt(filteredProductByCity.y)]
			})
			const cityDrilldownResult = {
				name: province.drilldown,
				id: province.drilldown+'|'+province.province_id+"|"+province.name,
				data: drilldown,
				keys:['name','y','drilldown']
			}
			regionDrilldownData.push(cityDrilldownResult);
		});
	});

	$.each(regionChart, function (index, value) {
		delete value.province_id;
		value.y = value.y * 1.0;
	});
	// Create the chart
	Highcharts.chart('container3', {
		chart: {
			type: 'pie'
		},
		title: {
			text: 'Penjualan per Wilayah'
		},

		accessibility: {
			announceNewData: {
				enabled: true
			},
			point: {
				valueSuffix: '%'
			}
		},

		plotOptions: {
			series: {
				dataLabels: {
					enabled: true,
					formatter: function () {
						return this.key + ' : ' + Highcharts.numberFormat(this.y, 2);
					},
				}
			}
		},

		tooltip: {
			headerFormat:
					'<span style="font-size:11px">{series.name}</span><br>'
			,
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
		},

		series: [
			{
				name: "",
				colorByPoint: true,
				data: regionChart
			}
		],
		drilldown: {
			series: regionDrilldownData
		}
	});
</script>
