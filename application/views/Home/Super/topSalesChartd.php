<script type="text/javascript">
	var product = new Array();
	var quantity = new Array();


	const salesChartDrilldownd = JSON.parse(`<?php echo $salesChartDrilldownd ?>`);

	const salesd = JSON.parse('<?php echo $salesChartd ?>');
	const salesProductDrillDownDatad = [];

	salesd.forEach(soldProduct => {
		const partnerDrillDownData = salesChartDrilldownd.filter(soldProductByProvince => {
			return soldProductByProvince.id === soldProduct.id
		}).map(filteredSoldProductPerProvince => {
			return [ filteredSoldProductPerProvince.product_name, parseInt(filteredSoldProductPerProvince.y) ]
		});

		const topProductDrilldownResult = {
			name: soldProduct.name,
			id: soldProduct.name,
			data: partnerDrillDownData
		};

		salesProductDrillDownDatad.push(topProductDrilldownResult);
	});

	$.each(salesd, function(index, value) {
		delete value.id;
		value.y = value.y * 1.0;
	});


	console.log('PARENT DATA', salesd);
	console.log('DRILLDOWN DATA:', salesProductDrillDownDatad);

	Highcharts.chart('topSalesContainerd', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'Penjualan Terbaik'
		},
		accessibility: {
			announceNewData: {
				enabled: true
			}
		},
		xAxis: {
			type: 'category'
		},
		yAxis: {
			title: {
				text: 'Total percent market share'
			}

		},
		legend: {
			enabled: false
		},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: '{point.y:.1f}'
				}
			}
		},

		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}</b> {point.unit}<br/>'
		},

		series: [
			{
				name: "",
				colorByPoint: true,
				data: salesd
			}
		],
		drilldown: {
			series: salesProductDrillDownDatad
		}
	});
</script>
