<script type="text/javascript">
	Highcharts.chart('container4', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'Capaian Penjualan '
		},
		xAxis: {
			//categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
			categories : [
					<?php foreach ($target_product as $data) : ?>
					<?php echo "'".$data['product_name']."',"; ?>
					<?php endforeach; ?>
			]
		},
		credits: {
			enabled: false
		},
		series: [{
			name: 'Target',
			data :[
				<?php foreach ($target_product as $data) : ?>
				<?php echo $data['target_qty'].","; ?>
				<?php endforeach; ?>
			]
		}, {
			name: 'Realization',
			data :[
				<?php foreach ($realization_product as $data) : ?>
				<?php echo $data['sales_quantity'].","; ?>
				<?php endforeach; ?>
			]
		}]
	});
</script>
