<script type="text/javascript">
	var weekNo = new Array();
	var weekRevenue = new Array();
	$.each(weeklychart, function(index,value){
		weekNo.push('Week '+value.week);
		weekRevenue.push(value.revenue*1.0);
	});

	Highcharts.chart('container', {
		chart: {
			type: 'line'
		},
		title: {
			text: 'Penjualan'
		},
		subtitle: {
			text: getMonthName(new Date().getMonth())
		},
		xAxis: {
			categories: weekNo
		},
		yAxis: {
			title: {
				text: 'Rupiah (IDR)'
			}
		},
		plotOptions: {
			line: {
				dataLabels: {
					enabled: true
				},
				enableMouseTracking: false
			}
		},
		series: [{
			name: 'Revenue',
			data: weekRevenue
		}]
	});
</script>
