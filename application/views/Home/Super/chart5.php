<script type="text/javascript">
	Highcharts.chart('container4', {
		chart: {
			type: 'areaspline'
		},
		title: {
			text: 'Capaian Penjualan'
		},
		legend: {
			layout: 'vertical',
			align: 'left',
			verticalAlign: 'top',
			x: 150,
			y: 50,
			floating: true,
			borderWidth: 1,
			backgroundColor:
				Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
		},

		xAxis: {
			categories: [
				<?php foreach ($target_product as $data) : ?>
				<?php echo "'".$data['product_name']."',"; ?>
				<?php endforeach; ?>
			],
			plotBands: [{ // visualize the weekend
				from: 4.5,
				to: 6.5,
				color: 'rgba(68, 170, 213, .2)'
			}]
		},
		yAxis: {
			title: {
				text: 'Values'
			}
		},

		credits: {
			enabled: false
		},
		plotOptions: {
			areaspline: {
				fillOpacity: 0.5
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}</b> {point.unit}<br/>'
		},
		series: [{
			name: 'Target',
			data :[
				<?php foreach ($target_product as $data) : ?>
				<?php echo $data['target_qty'].","; ?>
				<?php endforeach; ?>
			]
		}, {
			name: 'Realization',
			data :[
				<?php foreach ($realization_product as $data) : ?>
				<?php echo $data['sales_quantity'].","; ?>
				<?php endforeach; ?>
			]
		}]
	});
</script>
