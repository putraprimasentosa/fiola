<script type="text/javascript">
	var chart = Highcharts.chart('salesRealizationContainer', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'Sales Realization '
		},
		xAxis: {
			//categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
			categories: []
		},
		credits: {
			enabled: false
		},
		series: [{
			name: 'Target',
			data: []
		}, {
			name: 'Realization',
			data: []
		}]
	});

	$(document).ready(function () {
		$("#salesPicker").trigger('change');
		console.log($("#salesPicker").val());
	});
	$("#salesPicker").on('change', function () {
		var categoriesData = [];
		var targetData = [];
		var realizationData = [];
		var test = [];
		$.ajax({
				url: '<?=base_url() ?>/sales/targetReal/<?= isset($_GET['month']) ? "?month=".$_GET['month'] : "";?>',
				type: 'GET',
				data: {
					sales: $(this).val()
				},
				success: function (target) {
					$.each(JSON.parse(target), function (d, v) {
						categoriesData.push(v.product_name);
						targetData.push(v.target*1.0);
						realizationData.push(v.realization*1.0);
					});
					chart.update({
						title: {
							text: 'Capaian Penjualan ' + $("#salesPicker option:selected").text()
						},
						subtitle: {
							text: ''
						},
						xAxis: {
							//categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
							categories:
								categoriesData
						},
						series: [
							{
								name: 'Target', data:
									targetData
							},
							{
								name: 'Realization', data:
									realizationData
							}
						]
					}, true, false, {
						duration: 800
					});
				}


			}
		);




	})
</script>
