<script type="text/javascript">
	var product = new Array();
	var quantity = new Array();


	const soldChartDrilldown = JSON.parse(`<?php echo $soldChartDrilldown ?>`);

	const a = JSON.parse('<?php echo $soldchart ?>');
	const soldProductDrillDownData = [];

	a.forEach(soldProduct => {
		const partnerDrillDownData = soldChartDrilldown.filter(soldProductByProvince => {
			return soldProductByProvince.id === soldProduct.id
		}).map(filteredSoldProductPerProvince => {
			return [ filteredSoldProductPerProvince.province_name, parseInt(filteredSoldProductPerProvince.y) ]
		});

		const topProductDrilldownResult = {
			name: soldProduct.name,
			id: soldProduct.name,
			data: partnerDrillDownData
		};

		soldProductDrillDownData.push(topProductDrilldownResult);
	});

	$.each(a, function(index,value) {
		delete value.id;
		value.y = value.y * 1.0;
	});


	console.log('PARENT DATA', a);
	console.log('DRILLDOWN DATA:', soldProductDrillDownData);

	Highcharts.chart('container2', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'Produk Terlaku'
		},
		accessibility: {
			announceNewData: {
				enabled: true
			}
		},
		xAxis: {
			type: 'category'
		},
		yAxis: {
			title: {
				text: 'Total percent market share'
			}

		},
		legend: {
			enabled: false
		},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: '{point.y:.1f}'
				}
			}
		},

		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}</b> {point.unit}<br/>'
		},

		series: [
			{
				name: "",
				colorByPoint: true,
				data: a
			}
		],
		drilldown: {
			series: soldProductDrillDownData
		}
	});
</script>
