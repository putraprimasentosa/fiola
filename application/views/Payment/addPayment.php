<div class="content-wrapper">
    <section class="content">
        <form id="form-pembayaran">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-4 p-l-0">
                        <h5>Nomor Dokumen : </h5>
                    </div>
                    <div class="col-md-8 p-l-0">
                        <input name="documentno" id="documentno"
                               class="form-control" required readonly/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-4 p-l-0">
                        <h5>Tanggal : </h5>
                    </div>
                    <div class="col-md-8 p-l-0">
                        <input name="paymentdate" id="paymentdate"
                               class="form-control" required/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-4 p-l-0 anggota-container">
                        <h5>Pelanggan : </h5>
                    </div>
                    <div class="col-md-8 p-l-0 anggota-container">
                        <select name="id_anggota" id="id_anggota" class="form-control" required>
                            <option value="" disabled selected>-- Pilih Pelanggan --</option>
                            <?php
                            foreach($Customer as $partner){
                                echo '<option value="'.$partner["id"].'">'.$partner["code"].' | '.$partner["name"].'</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-4 p-l-0 invoice-container">
                        <h5>Invoice : </h5>
                    </div>
                    <div class="col-md-8 p-l-0 invoice-container">
                        <select name="invoice_id" id="invoice_id" class="form-control" required>

                        </select>
                    </div>
                </div>
                <div class="col-md-6">

                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-4 p-l-0 invoiceamt-container">
                        <h5>Jumlah Invoice : </h5>
                    </div>
                    <div class="col-md-8 p-l-0 invoiceamt-container">
                        <input class="accounting-enabled form-control numField" name="openAmt" id="openAmt"
                               required readonly/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-4 p-l-0 invoiceamt-container">
                        <h5>Diskon : </h5>
                    </div>
                    <div class="col-md-8 p-l-0 invoiceamt-container">
                        <input name="writeOffAmt" id="writeOffAmt"
                               class="form-control accounting-enabled numField" value="0" required/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-4 p-l-0 invoice-container">
                        <h5>Total Yang Harus Dibayar : </h5>
                    </div>
                    <div class="col-md-8 p-l-0 invoiceamt-container">
                        <input name="total_pembayaran" id="total_pembayaran"
                               class="form-control" value="0" required readonly/>
                    </div>
                </div>
                <div class="col-md-6">

                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-4 p-l-0 invoice-container">
                        <h5>Pembayaran : </h5>
                    </div>
                    <div class="col-md-8 p-l-0 invoiceamt-container">
                        <input class="accounting-enabled form-control numField" name="jumlah_pembayaran" id="jumlah_pembayaran"
                               required y/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-4 p-l-0 invoice-container">
                        <h5>Sisa Piutang : </h5>
                    </div>
                    <div class="col-md-8 p-l-0 invoiceamt-container">
                        <input class="accounting-enabled form-control numField" name="sisa_piutang" value="0" id="sisa_piutang"
                               required readonly/>
                    </div>
                </div>

                <div class="row" style="margin-top: 20px;">
                    <div class="col-md-5">
                    </div>
                    <div class="col-md-2">
                        <button type="button" id="simpan-detail" class="btn btn-primary">Simpan</button>
                    </div>
                    <div class="col-md-5">
                    </div>
                </div>
            </div>
            <br>
            <div>
                <table id="detailTable" class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center">
                            No Invoice
                        </th>
                        <th class="text-center">
                            Jumlah Invoice
                        </th>
                        <th class="text-center">
                            Diskon
                        </th>
                        <th class="text-center">
                            Total Invoice
                        </th>
                        <th class="text-center">
                            Jumlah Pembayaran
                        </th>

                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
        </form>
        <br>
        <div class="row">
            <div class="col-lg-6">
                <td>
                    <a href="../../index.php/penjualan/pesanan_penjualan">
                        <button style="align: right;" type="button" class="btn btn-warning"><span
                                class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Tutup
                        </button>
                    </a>
                </td>
            </div>
            <div class="col-lg-6">
                <div class="pull-right">
                    <button type="button" id="simpan-pesanan-penjualan" class="btn btn-success"><span
                            class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Simpan
                    </button>
                    <button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        Batal
                    </button>
                    <button type="button" id="cetak-pesanan-penjualan" class="btn btn-info"><span class="glyphicon glyphicon-print"
                                                                                                  aria-hidden="true"></span> Cetak
                    </button>
                </div>
            </div>
        </div>

        <script type="text/javascript"
                src="<?php echo base_url(); ?>assets/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/metronic/global/plugins/select2/js/select2.full.js"></script>
        <script type="text/javascript">
            accounting.settings = {
                number: {
                    precision : 0,  // default precision on numbers is 0
                    thousand: ".",
                    decimal : ","
                }
            }

                function calculateTotal() {
                    const jumlah_invoice = unformatNumber($("#openAmt").val());
                    const diskon = unformatNumber($("#writeOffAmt").val());
                    const jumlah_pembayaran = jumlah_invoice - diskon;

                    $("#total_pembayaran").val(formatNumber(jumlah_pembayaran));
                }

                $("#openAmt").change(function() {
                    calculateTotal();
                });

                $("#writeOffAmt").change(function() {
                    calculateTotal();
                });

                $("#jumlah_pembayaran").change(function() {
                    calculateSisa();
                });

                $(document).ready(function() {

                    dTable = $("#detailTable").DataTable({
                        "autoWidth": false,
                        columns: [
                            {
                                data: "documentno"
                            },

                            {
                                data: "openAmt"
                            },
                            {
                                data: "writeOffAmt"
                            },
                            {
                                data: "jumlah_invoice"
                            },
                            {
                                data: "jumlah_pembayaran"
                            }
                            ,{
                                data: "invoice_id"
                            }
                        ],
                        "columnDefs":[
                            {
                                "targets": [5],
                                "visible": false
                            }
                        ]
                    });
                });


                function calculate(addValue) {
                    var totalLines = totalLines + addValue;

                    var diskon = $("#writeOffAmt").val();
                    var total_invoice = $("#openAmt").val();
                    var totalAkhir = totalLines - diskon;

                    $("#payAmt").val(formatNumber(totalAkhir));
                }

                function calculateSisa()
                {
                    const total_pembayaran = unformatNumber($("#total_pembayaran").val());
                    const jumlah_pembayaran = unformatNumber($("#jumlah_pembayaran").val());
                    const sisa = jumlah_pembayaran - total_pembayaran;

                    $("#sisa_piutang").val(formatNumber(sisa));
                }

                $("#simpan-detail").click(function() {
                    var data = {};
                    $.each($("#form-pembayaran").serializeArray(), function(key, value) {
                        data[value.name] = value.value;
                    });


                    $("#detailTable").DataTable().row.add({
                        "documentno": $("#invoice_id option:selected").text(),
                        "invoice_id": "NO SPEC",
                        "openAmt": data.openAmt,
                        "writeOffAmt": formatNumber(data.writeOffAmt),
                        "jumlah_invoice": formatNumber(unformatNumber(data.openAmt)- data.writeOffAmt),
                        "jumlah_pembayaran": formatNumber(data.jumlah_pembayaran),

                    }).draw();

                    $("#form-pembayaran").trigger('reset');
                });


                $('#id_anggota').on('change', function () {
                    $.ajax({
                        url: "<?php echo base_url()?>penjualan/getInvoiceByCustomerId",
                        type: 'POST',
                        data: {
                            customerId: $(this).val()
                        }
                    })
                        .done(function (result) {
                            let invoiceElements = $('#invoice_id');
                            const invoice = jQuery.parseJSON(result);

                            invoiceElements.html(`
                            <option value="">-- Pilih Invoice --</option>
                        `);
                                            for (let x = 0; x < invoice.length; x++) {
                                                invoiceElements.append(`
                            <option value="${invoice[x].id}">${invoice[x].documentno}</option>
                          `);
                            }
                        })
                });

                $("#invoice_id").change(function() {
                    $.ajax({
                        url: "<?php echo base_url() ?>/penjualan/getDetailInvoiceByCustomerId",
                        type: "POST",
                        data: {
                            invoiceId: $(this).val()
                        },
                        success: function(e) {
                            var obj = $.parseJSON(e);
                            $.each(obj, function(index, value) {
                                console.log(value);
                                $("#openAmt").val(formatNumber(value.grandtotal)).trigger('change');
                            });
                        },
                    }).fail(function() {
                        alert("Gagal Mengambil Dta !!!");
                    });
                });
        </script>
    </section>
</div>
