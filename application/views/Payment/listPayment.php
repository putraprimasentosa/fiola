<section class="content">
    <div class="content-wrapper">
        <h2 style="margin-bottom: 20px">Payment</h2>
        <div class="row">
            <div class="col-md-12" hidden>
                <button id="addNew" type="button" class="btn btn-info" value="A"><span class="fa fa-plus"></span> Add new</button>
                <h3></h3>

            </div>

            <div class="col-md-12">
                <table id="paymentTable" name="paymentTable" class="table table-bordered">
                    <thead>
                        <th style="text-align: center;">Tanggal Transaksi</th>
                        <th style="text-align: center;">No Transaksi</th>
                        <th style="text-align: center;">Partner</th>
                        <th style="text-align: center;">Total Pesanan</th>
                        <th style="text-align: center;">Total Pembayaran</th>
                        <th style="text-align: center;">Kekurangan</th>
                        <th style="text-align: center;">Pembayaran</th>
                    </thead>
                    <tbody>
                        <?php foreach ($orders as $data) : ?>
                            <tr>
                                <td class="text-center">
                                    <?php echo date_create_from_format('Y-m-d', $data['orderdate'])->format('d-m-Y'); ?>
                                </td>
                                <td align="right"><?php echo $data['documentno']; ?></td>
                                <td class="text-left"><?php echo $data['partner']['name']; ?></td>
                                <td align="right"><?php echo number_format($data['totallines'], 0, ",", "."); ?></td>
                                <td align="right"><?php echo number_format($data['totalpembayaran'], 0, ",", "."); ?></td>
                                <td align="right"><?php echo number_format($data['totallines'] - $data['totalpembayaran'], 0, ",", "."); ?></td>
                                <td><a class="btn btn-info btn-block payment" data-amt="<?php echo $data['totallines'] - $data['totalpembayaran']; ?>" data-documentno="<?php echo $data['documentno'] ?>" data-id="<?php echo $data['id'] ?>" data-toggle="modal" data-target="#paymentModal"><span class="glyphicon glyphicon-usd"></span> Payment</a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

        </div>
</section>
</div>

<div class="modal fade" id="paymentModal" tabindex="-1" aria-hidden="true" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Pelunasan Piutang</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="form-detail-penjualan" data-toggle="validator" role="form">
                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    <span class="help-block">Nomor Dokumen</span>
                                    <input type="text" id="orderID" hidden>
                                    <input type="text" class="form-control " id="documentNo" placeholder="Nomor Dokumen" name="documentNo" readonly>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <div class="input-group col-md-12 pull-right">
                                        <div class="input-group-addon">Rp.</div>
                                        <input type="text" class="form-control" id="paymentAmt" placeholder="Payment Amount">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" id="simpan-detail" class="btn btn-primary"><span class="glyphicon glyphicon-usd"></span> Pay</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $("#addNew").click(function() {
        $(location).attr("href", "<?php echo base_url() ?>payment/addPayment");
    });

    $("#paymentTable").on('click', 'a.payment', function(e) {
        $("#orderID").val($(this).data("id"));
        $("#documentNo").val($(this).data("documentno"));
        $("#paymentAmt").val($(this).data("amt"));
    });

    $("#simpan-detail").click(function(){
        Swal.fire({
            title: 'Konfirmasi Pembayaran?',
            text: "Penerimaan pembayaran akan memotong piutang!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ok!',
            cancelButtonText: 'Cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                        url: "<?php echo base_url() ?>/payment/updatePayment",
                        type: "POST",
                        data: {
                            id: $("#orderID").val(),
                            amt: $("#paymentAmt").val()
                        }
                    })
                    .done(function(addressRawData) {
                        Swal.fire(
                            'Berhasil!',
                            'Penerimaan Pembayaran Berhasil.',
                            'success'
                        )
                        location.reload();
                    })
                    .fail(function() {
                        Swal.fire(
                            'Cancelled',
                            'Penerimaan Pembayaran dibatalkan.',
                            'error'
                        )
                    });

            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire(
                    'Cancelled',
                    'Penerimaan barang dibatalkan.',
                    'error'
                )
            }
        });
    });

</script>
