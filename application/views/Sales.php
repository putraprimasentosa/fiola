<div class="container-fluid">
    <form id="formSales">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="labelCustomer">Pelanggan</label>
                    <select class="form-control form-control-sm" id="customer" name="customer_id" required>
                        <?php foreach ($customers as $customer) {
                            echo '<option value="' . $customer['id'] . '"> ' . $customer['customer_name'] . '</option>';
                        } ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="labelCustomer">Ekspedisi</label>
                    <select class="form-control form-control-sm" id="courier" name="courier_id">
                        <?php foreach ($couriers as $courier) {
                            echo '<option value=' . $courier['id'] . '> ' . $courier['courier_name'] . '</option>';
                        } ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="labelCustomer">Marketplace</label>
                    <select class="form-control form-control-sm" id="sales_party" name="sales_party_id">
                        <?php foreach ($commerces as $commerce) {
                            echo '<option value=' . $commerce['id'] . '> ' . $commerce['name'] . '</option>';
                        } ?>
                    </select>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm">
                <div class="form-group">
                    <label for="labelCustomer">Tanggal Transaksi</label>
                    <input type="date" id="datetrx" class="form-control form-control-sm" value="<?php echo date('Y-m-d'); ?>" name="transaction_date"></input>
                </div>
            </div>

            <div class="col-sm">
                <div class="form-group input-group-sm">
                    <label for="labelCustomer">Ongkos Kirim</label>
                    <div class="input-group-prepend ">
                        <input type="text" class="form-control form-control-sm" id="subtotal" name="couriercost" value="0">
                        <span class="input-group-text " id="basic-addon1">/ Kilogram</span>
                    </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="form-group">
                    <label for="labelCustomer"> Berat Total (Kg)</label>
                    <input type="number" id="weight" name="totalWeight" class="form-control form-control-sm" value="0" readonly>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm">
            </div>
            <div class="col-sm">
            </div>
            <!-- <div class="col-sm">
                <div class="form-group input-group-sm">
                    <label for="labelCustomer">Sub Total</label>
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Rp.</span>
                        <input type="text" class="form-control form-control-sm" id="subtotal" name="subtotal" value="0" readonly>
                    </div>
                </div>
            </div> -->
            <div class="col-sm">
                <div class="form-group input-group-sm">
                    <label for="labelCustomer">Total Penjualan</label>
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Rp.</span>
                        <input type="text" class="form-control form-control-sm numField" id="grandtotal" name="grandtotal" value="0" readonly>
                    </div>
                </div>
            </div>
        </div>


    </form>
    <hr style="height:10px;border:1 px solid;">

    <div class="row align-items-end">
        <div class="col-sm-3">
            <div class="form-group input-group-sm">
                <label for="labelCustomer">Produk</label>
                <select class="form-control form-control-sm" id="dProd">
                    <option value="0" selected disabled>Pilih Produk</option>
                    <?php foreach ($products as $product) {
                        echo '<option value="' . $product['id'] . '" price = "'. $product['product_price'].'" weight="'.$product['product_weight'].'"> ' . $product['product_code'] . '-' . $product['product_name'] . '</option>';
                    } ?>
                </select>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group input-group-sm">
                <label for="labelCustomer">Jumlah</label>
                <div class="input-group-prepend">
                    <input type="text" class="form-control form-control-sm" id="dQty" name="dQty" value="0">
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group input-group-sm">
                <label for="labelCustomer">Harga</label>
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Rp.</span>
                    <input type="text" class="form-control form-control-sm" id="dPrice" name="dPrice" value="" readonly>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group input-group-sm">
                <div class="row">
                    <div class="col-sm">
                        <button type="button" id="addDetail" class="btn btn-success btn-block btn-sm"><span class="fa fa-plus"></span> Tambah</button>
                    </div>
                    <div class="col-sm">
                        <button type="button" id="cancelDetail" class="btn btn-danger btn-block btn-sm"><span class="fa fa-ban"></span> Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <table id="detailTable" class="table table-hovered">
        <thead>
            <th>No</th>
            <th>Nama Produk</th>
            <th>Jumlah</th>
            <th>Harga</th>
            <th>Total</th>
            <th>Tindakan</th>
        </thead>
        <tbody>
        </tbody>
    </table>
    <hr>
    <div class="row">
        <div class="col-sm-1">
            <button type="button" id="cancelForm" class="btn btn-danger btn-sm btn-block"><span class="fa fa-angle-left"> Kembali </span></button>
        </div>
        <div class="col-sm-1">
            <button type="button" id="submitForm" class="btn btn-success btn-sm btn-block"><span class="fa fa-shopping-cart"> Simpan </span></button>
        </div>
    </div>


</div>

<script>
    var dTable = null;
    var counter = 1;
    $(document).ready(function() {
        $("#customer").select2();
        $("#dProd").select2();
        $("#courier").select2();
        $("#sales_party").select2();
        dTable = $("#detailTable").DataTable({
            columns: [{
                    data: 'counter'
                },
                {
                    data: 'name'
                },
                {
                    data: 'qty'
                },
                {
                    data: 'price'
                }, {
                    data: 'subtotal'
                }, {
                    data: 'action'
                },
                {
                    data: 'h_product'
                },
                {
                    data: 'h_price'
                },
                {
                    data: 'h_qty'
                },
				{
					data: 'h_weight'
				}
            ],
            "columnDefs": [{
                    "targets": 5,
                    "data": null,
                    "defaultContent": "<button class='btn btn-sm btn-block btn-danger delRow' >Remove</button>"
                },
                {
                    "targets": [6, 7, 8,9],
                    "visible": false
                }
            ]
        });
    });
    $("#detailTable").on('click', 'button.delRow', function(e) {
        e.preventDefault();
        var data = dTable.row($(this).parents('tr')).data();
        dTable.row($(this).parents('tr')).remove().draw();
        calculate();
    });

    $("#addDetail").click(function() {
        if ($("#dQty").val() == 0 || $("#dProd").val() == null) {
            Swal.fire({
                title: 'Gagal',
                text: "Pastikan Product dan Quantity Terisi !",
                type: 'error',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Okay!'
            })
            return;
        }
        console.log($("#dProd option:selected").attr('weight'));
        $("#detailTable").DataTable().row.add({
            "counter": counter,
            "name": $("#dProd option:selected").text(),
            "qty": formatNumber($("#dQty").val()),
            "price": formatNumber($("#dProd option:selected").attr('price')),
            "subtotal": formatNumber($("#dQty").val() * $("#dProd option:selected").attr('price')),
            "h_product": $("#dProd option:selected").val(),
            "h_qty": $("#dQty").val(),
            "h_price": $("#dProd option:selected").attr('price'),
			"h_weight": $("#dProd option:selected").attr('weight')
        }).draw();
        counter++;
        $("#dProd").val(0).trigger('change');
        $("#dQty").val(0);
        calculate();
    });

    $("#dProd").change(function(){
    	$("#dPrice").val($("#dProd option:selected").attr('price'));
	});

    $("#cancelDetail").click(function() {
        $("#dProd").select(0);
        $("#dQty").val(0);
    });

    function calculate() {
        var grandTotal = 0.0;
        var weight = 0.0;
        dTable.rows().every(function(idx, table, row) {
            var data = this.data();
            var sub = data.h_qty * data.h_price * 1.0;
			weight = weight + (data.h_weight*data.h_qty);
            grandTotal = grandTotal + sub;
        });
        $("#weight").val(weight*1.0);
        $("#grandtotal").val(formatNumber(grandTotal));
    }
    $("#cancelForm").click(function() {
        window.location.href = "<?php echo base_url('/sales/all') ?>";
    });

    $("#submitForm").click(function() {
        $(".numField").each(function(idx, value) {
            $(this).val(unformatNumber($(this).val()));
        });

        if(!$("#detailTable").DataTable().data().count()){
			Swal.fire({
				title: 'Silahkan isi data terlebih dahulu!',
				text: 'Tidak ada data yang terisi',
				type: 'error',
				confirmButtonText: 'OK'
			});
        	return;
		}
        var data = {};
        $.each($("#formSales").serializeArray(), function(key, value) {
            data[value.name] = value.value;
        });


        $.ajax({
            url: "<?php echo base_url(); ?>sales/insertSales",
            type: "POST",
            datatype: "json",
            data: {
                sales: JSON.stringify(data),
                data: $("#detailTable").DataTable().rows().data().toArray(),
            },
            success: function(e) {
                showSuccess();
            }
        }).fail(function(a, b, c) {
            console.log(c);
        });
    });
    function showSuccess() {
                Swal.fire({
                    title: 'Success',
                    text: "Data Berhasil Tersimpan !",
                    type: 'warning',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Okay!'
                }).then((result) => {
                    if (result.value) {
                        location.reload();
                    }
                })
            }
</script>
