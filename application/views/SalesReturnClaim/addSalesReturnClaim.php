<style>
	.return-detail-editor {
		max-width: 120px;
	}

	.item-col {
		width: 20%;
	}

	.condition-header-col {
		width: 60%;
	}

	.condition-col {
		width: 15%
	}

	.m-t-20 {
		margin-top: 20px;
	}

	.invalid-row {
		background-color: #dc3545;
		color: white;
	}

	.no-padding-right {
		padding-right: 0 !important;
	}
</style>

<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-center margin-bottom">Klaim Ekspedisi</h2>
				<div class="row margin-bottom">
					<form id="return-main-info-form">
						<input type="hidden" name="salesReturnId" value="<?php echo $salesReturnData->id; ?>">
						<div class="row col-md-12">
							<div class="col-md-12 form-group">
								<label for="return-claim-no" class="col-sm-2 control-label no-padding">
									No. Klaim Retur
								</label>
								<div class="col-sm-10 no-padding-right" style="padding-left: 5px;">
									<input id="return-claim-no" name="returnClaimNo" class="form-control" value="<?php echo $documentNo; ?>"
												 required readonly
									/>
								</div>
							</div>
							<div class="col-md-12 form-group">
								<label for="return-claim-date" class="col-sm-2 control-label no-padding">
									Tanggal Klaim Retur
								</label>
								<div class="col-sm-10 no-padding-right" style="padding-left: 5px">
									<input id="return-claim-date" name="returnClaimDate" class="form-control" required />
								</div>
							</div>
						</div>
						<div class="row col-md-6">
							<div class="col-md-12 form-group">
								<label for="return-no" class="col-sm-4 control-label no-padding">No. Retur</label>
								<div class="col-sm-8 no-padding-right">
									<input id="return-no" value="<?php echo $salesReturnData->returnno ?>"
												 class="form-control" readonly
									/>
								</div>
							</div>
							<div class="col-md-12 form-group">
								<label for="return-date" class="col-sm-4 control-label no-padding">
									Tanggal Retur
								</label>
								<div class="col-sm-8 no-padding-right">
									<input id="return-date" value="<?php echo $salesReturnData->returndate ?>"
												 class="form-control" readonly
									/>
								</div>
							</div>
						</div>
						<div class="row col-md-6 no-padding-right">
							<div class="col-md-12 form-group no-padding-right">
								<label for="expedition-name" class="col-sm-4 control-label no-padding">
									Ekspedisi
								</label>
								<div class="col-sm-8 no-padding-right">
									<input id="expedition-name"
												 value="<?php
												 echo $salesReturnData->order->usecourier ? $salesReturnData->order->courier->name : NO_COURIER
												 ?>"
												 class="form-control no-padding-right" readonly
									/>
								</div>
							</div>
							<div class="col-md-12 form-group no-padding-right">
								<label for="license-plate-no" class="col-sm-4 control-label no-padding">
									No. Polisi</label>
								<div class="col-sm-8 no-padding-right">
									<input id="license-plate-no" value="<?php echo $salesReturnData->vehiclelicenseplate ?>"
												 class="form-control" readonly
									/>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="row col-md-12">
					<table id="order-detail-table" class="table table-bordered" style="width: 100%">
						<thead>
						<tr>
							<td rowspan="2">ID Item</td>
							<td class="text-center item-col" rowspan="2" style="vertical-align: middle">Merek</td>
							<td class="text-center item-col" rowspan="2" style="vertical-align: middle">Nama Produk</td>
							<td rowspan="2">Subtotal Retur</td>
							<td class="text-center item-col" rowspan="2" style="vertical-align: middle">Jumlah Asli</td>
							<td class="text-center condition-header-col" colspan="5">
								Kondisi
							</td>
						</tr>
						<tr>
							<td class="condition-col text-center">Utuh / Toleransi</td>
							<td class="condition-col text-center">Patah / Gupil</td>
							<td class="condition-col text-center">Hancur</td>
							<td class="condition-col text-center">Tidak Kembali</td>
							<td class="condition-col text-center">Lain-lain</td>
						</tr>
						</thead>
						<tbody>
						<?php
						foreach ($salesReturnData->details as $detail) {
							echo "
							<tr>
								<td>".$detail->item->id."</td>
								<td>".$detail->item->brand."</td>
								<td>".$detail->item->name."</td>
								<td>".$detail->subtotalreturn."</td>
								<td>".$detail->totalqty."</td>
								<td>".$detail->totalqty."</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
							</tr>
							";
						}
						?>
						</tbody>
					</table>
				</div>
				<div class="row col-md-12 text-right" style="margin-top: 12px">
					<div class="bottom-nav-button-container">
						<a class="btn btn-warning" href="<?php echo base_url('retur-penjualan'); ?>">
							<i class="fa fa-arrow-left"></i> Kembali
						</a>
						<a id="save-SalesReturn" class="btn btn-success">
							<i class="fa fa-save"></i> Simpan
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<script src="<?php echo getNodeUrl('moment/moment.js') ?>"></script>
<script>
  const returnClaimDateElement = $('#return-claim-date');

  returnClaimDateElement.val(moment().format('DD-MM-YYYY'));
  returnClaimDateElement.datepicker({
    autoclose: true,
    clearBtn: true,
    format: INTENDED_DATE_FORMAT.toLowerCase()
  });

  const renderValueInput = (cell, cellData, rowIndex, editorClass) => {
  	const isReadonly = parseInt(cellData, 10) === 0 ? 'readonly' : '';
    $(cell).html(`
        <input class="form-control return-detail-editor accounting-enabled ${editorClass}"
            id="${editorClass}-${rowIndex}" value="${cellData}"
        />
        <textarea class="form-control return-detail-description-editor ${editorClass}-desc not-resizeable m-t-10"
					id="${editorClass}-${rowIndex}-desc" placeholder="${DESCRIPTION_PLACEHOLDER}" ${isReadonly}
				></textarea>
    `);
  };

  $(document).ready(function () {
    const mainTable = $('#order-detail-table').DataTable({
      columns: [
        { data: 'itemId', visible: false },
        { data: 'itemBrand' },
        { data: 'itemName' },
        { data: 'lineAmount', visible: false },
        {
        	data: 'qty',
					class: 'qty-col text-right',
					createdCell: renderNumericValue
				},
        {
          data: 'intact',
          defaultContent: 0,
          class: 'text-center',
          createdCell: (cell, cellData, rowData, rowIndex) =>
            renderValueInput(cell, rowData.qty, rowIndex, 'intact')
        },
        {
          data: 'cracked',
          defaultContent: 0,
          class: 'text-center',
          createdCell: (cell, cellData, rowData, rowIndex) =>
						renderValueInput(cell, cellData, rowIndex, 'cracked')
        },
        {
          data: 'destroyed',
          defaultContent: 0,
          class: 'text-center',
          createdCell: (cell, cellData, rowData, rowIndex) =>
						renderValueInput(cell, cellData, rowIndex, 'destroyed')
        },
        {
          data: 'lost',
          defaultContent: 0,
          class: 'text-center',
          createdCell: (cell, cellData, rowData, rowIndex) =>
						renderValueInput(cell, cellData, rowIndex, 'lost')
        },
        {
          data: 'others',
          defaultContent: 0,
          class: 'text-center',
          createdCell: (cell, cellData, rowData, rowIndex) =>
						renderValueInput(cell, cellData, rowIndex, 'others')
        }
      ],
			ordering: false
    });

    function constructReturnClaimDetails() {
      const tableData = mainTable.data();
      const tableNode = $('#order-detail-table');
      const items = [];

      for (let x = 0; x < tableData.length; x++) {
        const intact = unformatNumber(tableNode.find(`#${GOODS_CONDITIONS.INTACT}-${x}`).val());
				const intactDescription = tableNode.find(`#${GOODS_CONDITIONS.INTACT}-${x}-desc`).val();
				const cracked = unformatNumber(tableNode.find(`#${GOODS_CONDITIONS.CRACKED}-${x}`).val());
				const crackedDescription = tableNode.find(`#${GOODS_CONDITIONS.CRACKED}-${x}-desc`).val();
				const destroyed = unformatNumber(tableNode.find(`#${GOODS_CONDITIONS.DESTROYED}-${x}`).val());
				const destroyedDescription = tableNode.find(`#${GOODS_CONDITIONS.DESTROYED}-${x}-desc`).val();
				const lost = unformatNumber(tableNode.find(`#${GOODS_CONDITIONS.LOST}-${x}`).val());
				const lostDescription = tableNode.find(`#${GOODS_CONDITIONS.LOST}-${x}-desc`).val();
				const others = unformatNumber(tableNode.find(`#${GOODS_CONDITIONS.OTHERS}-${x}`).val());
        const othersDescription = tableNode.find(`#${GOODS_CONDITIONS.OTHERS}-${x}-desc`).val();

        items.push({
          itemId: tableData[x].itemId,
          qty: parseInt(tableData[x].qty),
          subtotal: parseInt(tableData[x].lineAmount),
          intact,
					intactDescription,
					cracked,
					crackedDescription,
					destroyed,
					destroyedDescription,
					lost,
					lostDescription,
					others,
					othersDescription
        });
      }

      return items;
    }

    function checkDetailIsValid(detailData) {
      for (let x = 0; x < detailData.length; x++) {
        const detail = detailData[x];
        const areDetailsValid = detail.intact < 0 || detail.cracked < 0 || detail.destroyed < 0 || detail.lost < 0 || detail.others < 0;
        const claimedNumberOfGoods = detail.intact + detail.cracked + detail.destroyed + detail.lost + detail.others;
        if (claimedNumberOfGoods !== detail.qty || areDetailsValid) {
          return false;
        }
      }

      return true;
    }

    function isFormInvalid() {
      const returnClaimData = constructReturnClaimDetails();

      if (!checkDetailIsValid(returnClaimData)) {
        invokeError(SWEETALERT.TEXT.RETURN.INVALID_DETAIL_AMOUNT);
        return true;
      }

      if (!validateFields('#return-main-info-form')) {
        invokeError(SWEETALERT.TEXT.RETURN.INVALID_REQUIRED_FIELDS);
        return true;
      }

      return false;
    }

    function enableOrDisableDescriptionEditor(nodeToEnableOrDisable, valueToCheck) {
    	if (valueToCheck <= 0) {
    		nodeToEnableOrDisable.prop('readonly', true);
    		return;
			}

    	nodeToEnableOrDisable.prop('readonly', false);
		}

    $('#save-SalesReturn').on('click', function () {
      if (isFormInvalid()) {
        return;
      }

      const info = JSON.stringify(constructObjectFromFormData('#return-main-info-form'));
      const detail = JSON.stringify(constructReturnClaimDetails());
      invokeLoading();

      $.ajax({
        url: '<?php echo base_url(); ?>klaim-retur-penjualan',
        type: 'POST',
        data: {
          info,
          detail
        }
      })
      .done(function (data) {
        const result = JSON.parse(data);
        dismissDialog();
        invokeDialog({
          title: SWEETALERT.TITLE.SUCCESS,
          text: SWEETALERT.TEXT.RETURN.SAVE_SUCCESS,
          okCallback: () => navigateTo(`<?php echo base_url(); ?>klaim-retur-penjualan/${result.id}`),
          cancelCallback: () => navigateTo('<?php echo base_url(); ?>retur-penjualan')
        });
      })
      .fail(function () {
        dismissDialog();
        invokeError(SWEETALERT.TEXT.RETURN.SAVE_FAILED);
      });
    });

    mainTable.on('change blur', '.return-detail-editor', function () {
      let totalItem = 0;
      let isValueInvalid = false;
      const row = $(this).parents('tr');
      const claimQtyEditorInSameRow = row.find('.return-detail-editor');
      const descriptionEditorInSameRow = row.find('.return-detail-description-editor');
      const originalQty = parseInt(row.find('.qty-col').text());

      for (let index = 0; index < claimQtyEditorInSameRow.length; index++) {
        const claimValue = $(claimQtyEditorInSameRow[index]).val();

        enableOrDisableDescriptionEditor($(descriptionEditorInSameRow[index]), claimValue);
        if (claimValue < 0) {
          isValueInvalid = true;
          break;
				}

        totalItem += parseInt(claimValue);
      }

      if (totalItem !== originalQty || isValueInvalid) {
        row.addClass('invalid-row');
      } else {
        row.removeClass('invalid-row');
      }
    });
  })
</script>
