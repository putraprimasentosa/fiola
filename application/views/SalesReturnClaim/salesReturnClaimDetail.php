<style>
	thead td {
		text-align: center;
		font-weight: bold;
	}
</style>

<div class="content-wrapper">
	<section class="content">
		<h2 style="margin-bottom: 20px">Data Klaim Ekspedisi</h2>
		<div class="row" style="font-size: 12pt">
			<div class="col-md-12 form-group">
				<div class="row col-md-6">
					<div class="col-md-6 no-padding">
						Nomor Klaim Retur
					</div>
					<div class="col-md-6">
						: <?php echo $returnClaimData->claimno ?>
					</div>
				</div>
				<div class="row col-md-6">
					<div class="col-md-6 no-padding">
						Nama Ekspedisi
					</div>
					<div class="col-md-6">
						:
						<?php
						echo $returnClaimData->salesReturn->order->usecourier ?
							$returnClaimData->salesReturn->order->courier->name : NO_COURIER
						?>
					</div>
				</div>
			</div>
			<div class="col-md-12 form-group">
				<div class="row col-md-6">
					<div class="col-md-6 no-padding">
						Tanggal Klaim Retur
					</div>
					<div class="col-md-6">
						: <?php echo DateTime::createFromFormat('Y-m-d', $returnClaimData->claimdate)->format('d-m-Y') ?>
					</div>
				</div>
				<div class="row col-md-6">
					<div class="col-md-6 no-padding">
						Nomor Polisi Ekspedisi
					</div>
					<div class="col-md-6">
						: <?php echo $returnClaimData->salesReturn->vehiclelicenseplate ?>
					</div>
				</div>
			</div>
			<div class="col-md-12 form-group">
				<div class="row col-md-6">
					<div class="col-md-6 no-padding">
						Nomor Retur
					</div>
					<div class="col-md-6">
						: <?php echo $returnClaimData->salesReturn->returnno ?>
					</div>
				</div>
			</div>
		</div>
		<section>
			<h3>Klaim Retur</h3>
			<div style="margin-bottom: 20px; overflow-x: scroll">
				<table id="return-claim-data-table" class="table table-bordered" style="min-width: 768px">
					<thead>
					<tr>
						<td rowspan="2" style="vertical-align: middle">Nama Barang</td>
						<td colspan="<?php echo count($conditions); ?>">Kondisi</td>
					</tr>
					<tr>
						<?php
						foreach ($conditions as $condition) {
							echo "
            <td>".ucwords($condition['name'])."</td>
          ";
						}
						?>
					</tr>
					</thead>
					<tbody>
					<?php
					foreach ($returnClaimData->details as $detail) {
						$item = $detail->item;
						echo "
                <tr>
                    <td rowspan='2'>$item->name</td>
        	";

						foreach ($conditions as $condition) {
							$affectedItemSubtotal = $detail->conditionDetails->where('m_condition_id', $condition->id)->first()['qty'];
							echo "    <td class='text-center'>";
							echo $affectedItemSubtotal > 0 ? number_format(
								$affectedItemSubtotal, 0, ',', '.'
							) : '';
							echo "    </td>";
						}

						echo "
						</tr>
						<tr>
					";

						foreach ($conditions as $condition) {
							$affectedItemData = $detail->conditionDetails->where('m_condition_id', $condition->id)->first();
							$affectedItemDescription = $affectedItemData['claimdescription'];
							$affectedItemQty = $affectedItemData['qty'];

							echo "    <td>";
							if ($affectedItemQty > 0) {
								echo "<strong>Keterangan:</strong> <br>";
								echo $affectedItemDescription ?: 'N/A';
							}
							echo "    </td>";
						}

						echo "        
                </tr>
                <tr>
                  <td class='text-right text-bold'>Jumlah Klaim</td>
        ";

						foreach ($conditions as $condition) {
							$affectedItemData = $detail->conditionDetails->where('m_condition_id', $condition->id)->first();
							$affectedItemSubtotal = $affectedItemData['subtotalclaim'];
							$affectedItemQty = $affectedItemData['qty'];

							echo "    <td class='text-right text-bold'>";
							echo $affectedItemQty > 0 ? "Rp ".number_format(
									$affectedItemSubtotal, 0, ',', '.'
								) : '';
							echo "    </td>";
						}

						echo "
              </tr>
        ";
					}
					?>
					<tr class="text-bold text-right">
						<td>Total Klaim Retur</td>
						<td colspan="<?php echo 1 + count($conditions) ?>">
							Rp <?php echo number_format($returnClaimData->totalreturnclaim, 0, ',', '.'); ?>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="bottom-nav-button-container">
				<a href="<?php echo base_url('retur-penjualan'); ?>" class="btn btn-warning">
					<i class="fa fa-arrow-left"></i> Kembali
				</a>
				<a target="_blank" href="<?php echo base_url('KlaimReturPenjualan/printorder/'.$returnClaimData->id) ?>"
					 class="btn btn-success">
					<i class="fa fa-print"></i> Cetak Nota Klaim
				</a>
			</div>
		</section>
</div>
