<?php
function getPadZero(int $sourceNumber, int $numberOfDigits = 6) {
	$sourceString = (string) $sourceNumber;
	$padZeros = $numberOfDigits - strlen($sourceString);
	$paddedNumber = '';

	for ($zerosLeft = 0; $zerosLeft < $padZeros; $zerosLeft++) {
		$paddedNumber = $paddedNumber.'0';
	}

	return $paddedNumber.$sourceString;
}
