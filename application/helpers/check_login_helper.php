<?php

function checkSession() {
    if (($_SESSION['user'])) {
        return true;
    }

    redirect('home/login');
}