<?php
use Dotenv\Dotenv;

$dotenv = Dotenv::create(__DIR__.'/../..');
$dotenv->load();

function env(string $key, string $fallback): string {
  return getenv($key) ? : $fallback;
}