<?php
define('MASTER_CATEGORY', 'master');
define('REPORT_CATEGORY', 'report');
define('SALES_CATEGORY', 'penjualan');
define('SALES_PAYMENT_CATEGORY', 'payment');
define('SALES_RETURN_CATEGORY', 'retur-penjualan');
define('SALES_RETURN_CLAIM_CATEGORY', 'klaim-retur-penjualan');
define('SALES_LIST', 'allData');
define('SALES_PRICE_HISTORY', 'salesPriceHistory');
define('SALES_RECEIVABLE_PAYMENT_ROUTE', 'pelunasanPiutang');
define('SALES_REPORT_ROUTE', 'sales');
define('RETURN_REPORT_ROUTE', 'return');
define('CLAIM_REPORT_ROUTE', 'claim');
define('DELIVERY_NOTE_REPORT_ROUTE', 'tandaTerimaSuratJalan');

define('MASTER_ITEM_ROUTES', [
	'masterProduct',
	'addMasterProduct'
]);
define('MASTER_CUSTOMER_ROUTES', [
	'masterCustomer',
	'editMasterCustomer',
	'bonusSchema',
	'discountSchema',
	'shippingAddress',
	'productPrice',
	'addMasterCustomer'
]);
define('MASTER_COURIER_ROUTES', [
	'masterCourier'
]);
define('MASTER_FARE_ROUTES', [
	'masterFare'
]);
define('MASTER_PROVINCE_ROUTES', [
	'masterProvince',
	'masterCity'
]);

define('SALES_CATEGORIES', [
	SALES_CATEGORY,
	SALES_PAYMENT_CATEGORY,
	SALES_RETURN_CATEGORY,
	SALES_RETURN_CLAIM_CATEGORY
]);
define('SALES_RETURN_CATEGORIES', [
	SALES_RETURN_CATEGORY,
	SALES_RETURN_CLAIM_CATEGORY
]);

define('NO_COURIER', 'TANPA KURIR');
define('NOT_AVAILABLE', 'N/A');
define('EMPTY_DATA', '-');

define('RETURN_STATUS_CREATED', 1);
define('PERCENTAGE_DIVIDER', 100);

define('LOG_LEVEL_ERROR', 'error');

define('CREATED_CODE', 201);
define('INTERNAL_SERVER_ERROR_CODE', 500);

define('QUERY_ORDER_DESCENDING', 'DESC');
define('QUERY_ORDER_ASCENDING', 'ASC');

define('DOCUMENT_NO_STARTING_NUMBER', 1);
define('DOCUMENT_TYPE_SALES_RETURN', 'SALES_RETURN');
define('DOCUMENT_TYPE_SALES_RETURN_CLAIM', 'SALES_RETURN_CLAIM');
define('DOCUMENT_NO_PREFIX_SALES_RETURN', 'RJ/');
define('DOCUMENT_NO_PREFIX_SALES_RETURN_CLAIM', 'CL/');
