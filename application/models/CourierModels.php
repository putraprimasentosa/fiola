<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class courierModels extends Eloquent
{
    protected $table = 'm_courier';
    public $timestamps = false;
    protected $guarded = [];
}
