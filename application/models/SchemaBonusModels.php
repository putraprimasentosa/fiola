<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class schemaBonusModels extends Eloquent
{
    protected $table = 'm_schema';
    public $timestamps = false;
    protected $fillable = [];
    protected $guarded= [];

}
