<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class UOMModels extends Eloquent
{
    protected $table = 'm_uom';
    public $timestamps = false;
    protected $guarded = [];
    protected $fillable = [];

    public function partner() {
        return $this->belongsTo('ProductModels', 'product_uom_id', 'id');
    }

}
