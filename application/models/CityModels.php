<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class cityModels extends Eloquent
{
    protected $table = 'm_city';
    public $timestamps = false;
    protected $fillable = [];
    protected $guarded= [];
    
    public function province() {
    	return $this->belongsTo('provinceModels', 'm_province_id', 'id');
    }

    public function fare() {
        return $this->hasMany('FareModels', 'm_province_city_id', 'id');
    }

}
