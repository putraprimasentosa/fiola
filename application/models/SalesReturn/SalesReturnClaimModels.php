<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class SalesReturnClaimModels extends Eloquent {
  protected $table = 't_salesreturnclaim';
  protected $guarded = [];

  public function salesReturn() {
    return $this->belongsTo('SalesReturnModels', 't_salesreturn_id', 'id');
  }

  public function details() {
    return $this->hasMany('SalesReturnClaimDetailModels', 't_salesreturnclaim_id', 'id');
  }
}
