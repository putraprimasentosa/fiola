<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class SalesReturnClaimConditionDetailModels extends Eloquent {
  protected $table = 't_salesreturnclaimconditiondetail';
  protected $guarded = [];
  public $timestamps = false;

  public function condition() {
    return $this->belongsTo('GoodsConditionModels', 'm_condition_id', 'id');
  }

  public function parent() {
    return $this->belongsTo('SalesReturnClaimDetailModels', 't_salesreturnclaimdetail_id', 'id');
  }
}
