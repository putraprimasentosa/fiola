<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class SalesReturnDetailModels extends Eloquent {
  protected $table = 't_salesreturndetail';
  protected $guarded = [];
  public $timestamps = false;

  public function parent() {
    return $this->belongsTo('SalesReturnModels', 't_salesreturn_id', 'id');
  }

  public function item() {
    return $this->belongsTo('ProductModels', 'm_product_id', 'id');
  }
}
