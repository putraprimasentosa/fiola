<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class SalesReturnModels extends Eloquent {
  protected $table = 't_salesreturn';
  protected $guarded = [];

  public function order() {
    return $this->belongsTo('SalesModels', 't_order_id', 'id');
  }

  public function details() {
    return $this->hasMany('SalesReturnDetailModels', 't_salesreturn_id', 'id');
  }

  public function claim() {
  	return $this->hasOne('SalesReturnClaimModels', 't_salesreturn_id', 'id');
	}
}
