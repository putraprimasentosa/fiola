<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class SalesReturnClaimDetailModels extends Eloquent {
  protected $table = 't_salesreturnclaimdetail';
  protected $guarded = [];
  public $timestamps = false;

  public function item() {
    return $this->belongsTo('ProductModels', 'm_product_id', 'id');
  }

  public function conditionDetails() {
  	return $this->hasMany('SalesReturnClaimConditionDetailModels', 't_salesreturnclaimdetail_id', 'id');
	}

  public function parent() {
    return $this->belongsTo('SalesReturnClaimModels', 't_salesreturnclaim_id', 'id');
  }
}
