<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class GoodsConditionModels extends Eloquent {
  protected $table = 'm_goodscondition';
  protected $guarded = [];
  public $timestamps = false;

  public function items() {
    return $this->hasMany('SalesReturnConditionDetailModels', 'm_goodscondition_id', 'id');
  }
}