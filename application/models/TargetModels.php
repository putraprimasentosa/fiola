<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class TargetModels extends Eloquent
{
    protected $table = 'c_target';
    public $timestamps = false;
    protected $guarded = [];
    protected $fillable = [];

    public function line() {
        return $this->hasMany('TargetLineModels','target_id','id');
    }

}
