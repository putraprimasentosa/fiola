<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class shippingAddressModels extends Eloquent
{
    protected $table = 'm_shippingaddress';
    public $timestamps = false;
    protected $guarded = [];
    protected $fillable = [];

    public function partner() {
        return $this->belongsTo('customerModels', 'm_partner_id', 'id');
    }

    public function city(){
        return $this->hasOne('cityModels','id','m_city_id');
    }
}
