<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class schemaDiscountModels extends Eloquent
{
    protected $table = 'm_discount';
    public $timestamps = false;
    protected $fillable = [];
    protected $guarded = [];
}
