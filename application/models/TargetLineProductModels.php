<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class TargetLineProductModels extends Eloquent
{
	protected $table = 'c_targetline_p';
	public $timestamps = false;
	protected $guarded = [];
	protected $fillable = [];

	public function product() {
		return $this->hasOne('ProductModels', 'id', 'product_id');
	}

	public function target(){
		return $this->hasOne('TargetLineModels','targetline_id','id');
	}

}
