<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class DocumentNumberModels extends Eloquent {
	protected $table = 'm_documentnumber';
	protected $guarded = [];
	public $timestamps = false;
}
