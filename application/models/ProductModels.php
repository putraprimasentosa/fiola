<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class productModels extends Eloquent {
  protected $table = 'm_product';
  public $timestamps = false;
  protected $fillable = [];
  protected $guarded= [];
  
  public function uom(){
    return $this->hasOne('UOMModels','id','product_uom_id');
  }
  // public function category() {
  //   // return $this->hasOne('productCategoryModels', 'id');
  //   return $this->hasOne('productCategoryModels', 'id', 'm_product_category_id');
  // }

  // public function price() {
  //   return $this->hasOne('priceModels', 'product_id', 'id');
  // }

	// public function orderLine(){
	// 	return $this->hasMany('SalesLineModels','id','m_product_id');
	// }

  // public function returnDetails() {
  //   return $this->hasMany('SalesReturn/SalesReturnDetailModels', 'm_product_id', 'id');
  // }

  // public function returnClaimDetails() {
  //   return $this->hasMany('SalesReturn/SalesReturnClaimDetailModels', 'm_product_id', 'id');
  // }
}
