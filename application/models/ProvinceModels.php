<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class provinceModels extends Eloquent
{
    protected $table = 'm_province';
    public $timestamps = false;
    protected $fillable = [];
    protected $guarded = [];

    public function shippingAddress(){
        return $this->belongsTo('shippingAddressModels','m_province_id','id');
    }
}
