<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class satuanModels extends Eloquent
{
    protected $table = 'm_satuan';
    public $timestamps = false;

}
