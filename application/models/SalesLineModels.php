<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class salesLineModels extends Eloquent
{
    protected $table = 't_salesline';
    protected $fillable = [];
    protected $guarded=[];
    protected $with = 'product';
	public $timestamps = false;
	
	public function product(){
		return $this->belongsTo('productModels','product_id','id');
	}

	public function order() {
		return $this->belongsTo('SalesModels', 'sales_id', 'id');
	}
}
