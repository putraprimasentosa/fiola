<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class priceModels extends Eloquent
{
    protected $table = 'm_price';
    public $timestamps = false;
    protected $guarded = [];
    public function partner(){
        return $this->hasOne('customerModels','id');
    }

    public function product(){
        return $this->hasOne('productModels','id');
    }

}
