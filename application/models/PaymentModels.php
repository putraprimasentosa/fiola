<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class paymentModels extends Eloquent
{
    protected $table = 't_payment';
    public $timestamps = false;
    protected $fillable = [];
    protected $guarded=[];

    public function partner(){
        return $this->hasOne('customerModels','id','m_partner_id');
    }
}
