<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class CustomerModels extends Eloquent
{
    protected $table = 'm_customer';
    public $timestamps = false;
    protected $fillable = [];
    protected $guarded= [];

    public function sales(){
        return $this->hasMany('SalesModels', 'customer_id', 'id');
    }

}
