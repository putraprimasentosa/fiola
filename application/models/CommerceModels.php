<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class CommerceModels extends Eloquent
{
    protected $table = 'm_commerce';
    public $timestamps = false;
    protected $fillable = [];
    protected $guarded= [];
}
