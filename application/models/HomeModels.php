<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class homeModels extends Eloquent
{
    protected $table = 'm_users';
    public $timestamps = false;
}
