<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class AdminModels extends Eloquent
{
    protected $table = 'm_admin';
    public $timestamps = false;
    protected $guarded = [];
    protected $fillable = [];
}
