<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class salesModels extends Eloquent
{
    protected $table = 't_sales';

    //Check for Database
    protected $fillable = [];
    protected $guarded=[];

    public function customer(){
        return $this->hasOne('CustomerModels','id','customer_id');
    }

    public function details() {
    	return $this->hasMany('SalesLineModels', 't_order_id', 'id');
		}

    public function courier() {
    	return $this->belongsTo('CourierModels', 'courier_id', 'id');
        }
        
    public function commerce(){
        return $this->belongsTo('CommerceModels','sales_party_id','id');
    }

}

