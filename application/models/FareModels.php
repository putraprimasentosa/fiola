<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class fareModels extends Eloquent {
    protected $table = 'm_fare';
    protected $guarded = [];
    public $timestamps = false;
    protected $fillable = [];

    public function courier() {
    	return $this->belongsTo('courierModels', 'm_courier_id', 'id');
    }
     
    public function city() {
    	return $this->belongsTo('cityModels', 'm_province_city_id', 'id');
    }
}
