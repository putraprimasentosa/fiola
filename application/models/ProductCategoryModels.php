<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class productCategoryModels extends Eloquent
{
    protected $table = 'm_product_category';
    public $timestamps = false;

    public function product(){
        // return $this->hasOne('productModels','productcategory_ID','id');
        return $this->hasOne('productModels','m_product_category_id','id');

    }

}
