<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class salespriceModels extends Eloquent
{
    protected $table = 't_salesline';

    //Check for Database
    protected $fillable = [];
    protected $guarded=[];

    public function partner(){
        return $this->hasOne('customerModels','id','m_partner_id');
    }

    public function details() {
        return $this->hasMany('SalesLineModels', 't_order_id', 'id');
    }

    public function courier() {
        return $this->belongsTo('CourierModels', 'm_courier_id', 'id');
    }

    public function shippingAddress() {
        return $this->belongsTo('ShippingAddressModels', 'address', 'id');
    }
}

