<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class TargetLineModels extends Eloquent
{
    protected $table = 'c_targetline';
    public $timestamps = false;
    protected $guarded = [];
    protected $fillable = [];

    public function product() {
        return $this->hasOne('ProductModels', 'id', 'product_id');
    }

    public function target(){
        return $this->hasOne('TargetModels','target_id','id');
    }

}
