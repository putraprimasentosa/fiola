<?php

class Payment extends CI_Controller {
    public function __construct() {
        parent:: __construct();
        $this->load->model('courierModels');
        $this->load->model('productModels');
        $this->load->model('satuanModels');
        $this->load->model('productCategoryModels');
        $this->load->model('customerModels');
        $this->load->model('provinceModels');
        $this->load->model('shippingAddressModels');
        $this->load->model('paymentModels');
        $this->load->model('salesModels');

    }

    public function pelunasanPiutang()
    {
        $data['title'] = "Pembayaran Piutang";
        $data['orders'] = salesModels::with('partner')
            ->whereRaw('totallines <> totalpembayaran')
            ->orderBy('id','desc')->get();

        $this->load->view('Include/Header.php', $data);
        $this->load->view('Include/Sidebar.php');
        $this->load->view('Payment/listPayment', $data);
        $this->load->view('Include/Footer.php');
    }

    public function updatePayment(){
        $id = $this->input->post('id');
        $amt = $this->input->post('amt');

        $orderPaid = salesModels::find($id);
        $orderPaid->totalpembayaran = $orderPaid->totalpembayaran+$amt;
        $orderPaid->save();

    }

//    public function listPayment() {
//        $data['title'] = "Pembayaran Piutang";
//
//        $data['Payment'] = paymentModels::all();
//
//
//        $this->load->view('Include/Header.php', $data);
//        $this->load->view('Include/Sidebar.php');
//
//        $this->load->view('Payment/listPayment', $data);
//
//        $this->load->view('Include/Footer.php');
//    }
//
//    public function addPayment() {
//        $data['title'] = "Menambah Pembayaran Piutang";
//
//        $data['Customer'] = customerModels::where('iscustomer',1)->get();
//
//
//        $this->load->view('Include/Header.php', $data);
//        $this->load->view('Include/Sidebar.php');
//
//        $this->load->view('Payment/addPayment', $data);
//
//        $this->load->view('Include/Footer.php');
//    }



}
