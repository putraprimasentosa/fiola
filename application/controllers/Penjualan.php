<?php

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Capsule\Manager as DB;
use PHPJasper\PHPJasper;
require_once('DocumentController.php');
class Penjualan extends DocumentController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('customerModels');
        $this->load->model('cityModels');
        $this->load->model('FareModels');
        $this->load->model('courierModels');
        $this->load->model('productModels');
        $this->load->model('priceModels');
        $this->load->model('salesModels');
        $this->load->model('salesLineModels');
        $this->load->model('SalesReturn/SalesReturnModels');
        $this->load->model('shippingAddressModels');
        $this->setNumberData("SALES_ORDER", "FJ/");
    }

    public function allData()
    {
        $data['title'] = "Daftar Penjualan";
        $data['orders'] = salesModels::with('partner')->orderBy('id','desc')->get();

        $this->load->view('Include/Header.php', $data);
        $this->load->view('Include/Sidebar.php');
        $this->load->view('Pesanan/allData', $data);
        $this->load->view('Include/Footer.php');
    }

    public function viewData($id){
        $header['title'] = "View Penjualan";
        $data['order'] = salesModels::where('id',$id)->with('partner')->with('courier')->with('shippingAddress')->first();
        $data['orderLine'] = salesLineModels::where('t_order_id',$id)->with('product')->get();
        $data['orderID'] = $id;

        $this->load->view('Include/Header.php', $header);
        $this->load->view('Include/Sidebar.php');
        $this->load->view('Pesanan/viewPesanan', $data);
        $this->load->view('Include/Footer.php');
    }
    public function addPesanan($review ="A", $id = 0)
    {
        if($review == "E" && $id == 0)
            $review = "A";
        
        if($id > 0){
            $data['order'] = salesModels::find($id); 
            $data['orderLine'] = salesLineModels::where(['t_order_id' => $id])->get();
        }
        
        $data['title'] = "Menambah Pesanan";
        $data['review'] = $review;
        $data['partners'] = customerModels::all();
        $data['couriers'] = courierModels::all();
        $data['products'] = productModels::all();

        $this->load->view('Include/Header.php', $data);
        $this->load->view('Include/Sidebar.php');
        $this->load->view('Pesanan/Pesanan', $data);
        $this->load->view('Include/Footer.php');
    }

    public function updatePesanan($id = 0){
        if($id > 0){
            $data['orders'] = salesModels::find($id); 
            $data['orderLines'] = salesLineModels::where(['t_order_id' => $id])->get();
        }
        
        $data['title'] = "Menambah Pesanan";
        $data['partners'] = customerModels::all();
        $data['couriers'] = courierModels::all();
        $data['products'] = productModels::all();
        $this->load->view('Include/Header.php', $data);
        $this->load->view('Include/Sidebar.php');
        $this->load->view('Pesanan/updatePesanan', $data);
        $this->load->view('Include/Footer.php');
    }

    public function getLineDetails(){
        $order_id = $this->input->get("t_order_id");
        $data = salesLineModels::where(['t_order_id' => $order_id])->with('product')->get();
        print_r(json_encode($data));
    }

    public function getPrice()
    {
        $product_id = $this->input->post("id");
        $address = $this->input->post("address");
        //$price = priceModels::where(['m_product_id' => $product_id, 'm_partner_id' => $partner_id > 0 ? $partner_id : 0])->get();
        $product = productModels::select("unitprice", "weight")->where('id', '=', $product_id)->get();
        $price =DB::select( DB::raw("
            SELECT a.price from t_order_line a 
                join t_order b on a.t_order_id = b.id 
            where a.m_product_id = '$product_id' and b.address = '$address'
            order by a.id DESC
            LIMIT 1
        "));

        if(count($price) > 0) {
            $price[0]["weight"] = $product[0]->{"weight"};
        }

        if (count($price) == 0)
            echo $product;
        else
            echo json_encode($price);
    }

    public function getCourierCost()
    {
        $courier_id = $this->input->post("id");
        $m_city_id = $this->input->post("cityId");

        echo FareModels::select("price")
            ->where('m_courier_id', '=', $courier_id)
            ->where('m_province_city_id', '=', $m_city_id)
            ->get();
    }

    public function getOrdersByCustomerId($customerId) {
			$excludeReturnedOrders = $this->input->get('excludeReturn');

			if (!isset($excludeReturnedOrders)) {
				$customer = CustomerModels::find($customerId);
				echo json_encode($customer->sales);
				return;
			}

			$returnedSales = SalesReturnModels::whereHas('order', function (Builder $query) use ($customerId) {
				$query->where('m_partner_id', '=', $customerId);
			})
				->get()
				->map(function($salesReturn) {
					return $salesReturn->t_order_id;
				})
				->toArray();
			$availableOrders = SalesModels::with('shippingAddress')
				->where('m_partner_id', $customerId)
				->whereNotIn('id', $returnedSales)
				->orderBy('id', 'desc')
				->get();

			echo json_encode($availableOrders);
		}

    public function getOrder($id) {
			$orderInfo = salesModels::with('courier')->find($id);
			$orderDetail = $orderInfo->details;

			echo json_encode([
				'orderInfo' => $orderInfo,
				'orderDetail' => $orderDetail
			]);
    }
    
    public function getPartnerAddress(){
        $partner = $this->input->get("id");
        $addresses = ShippingAddressModels::where('m_partner_id', $partner)->with('city')->get();
        $partner = CustomerModels::find($partner);
        print_r(json_encode(['addresses' => $addresses, 'partner' => $partner]));
        return $addresses;
    }

    public function getAddress(){
        $partner = $this->input->get("id");
        $addresses = ShippingAddressModels::find($partner);
        print_r(json_encode($addresses));
        return $addresses;
    }
    public function updateSales(){
        $sales = $this->input->post("sales");
        $data = $this->input->post("data");
        $sales = json_decode($sales,true);
        $sales['orderdate'] = DateTime::createFromFormat('d-m-Y', $sales['orderdate'])->format('Y-m-d');
        $sales['dateacct'] = DateTime::createFromFormat('d-m-Y', $sales['dateacct'])->format('Y-m-d');
        $sales['deliverydate'] = DateTime::createFromFormat('d-m-Y', $sales['deliverydate'])->format('Y-m-d');
        $sales['created_at'] = DateTime::createFromFormat('d/m/Y', $sales['created_at'])->format('Y-m-d');
        $sales['updated_at'] = DateTime::createFromFormat('d/m/Y', $sales['updated_at'])->format('Y-m-d');
        if(!isset($sales["isborongan"]))
            $sales["isborongan"]="f";
        unset($sales['detailTable_length']);
        $id = $this->input->post("id");
        $salesModel = salesModels::find($id);
        $salesModel->fill($sales);
        $salesModel->save();
        
        salesLineModels::where(['t_order_id' => $id])->forceDelete();
        
        foreach ($data as $idx => $line) {
            $salesDetailModel = new salesLineModels();
            $salesDetailModel->timestamps = false;
            $salesDetailModel->fill([
                "t_order_id" => $salesModel->id,
                "qty" => $line["h_qtyEntered"],
                "price" => $line["h_priceEntered"],
                "discountamt" => 0,
                "linenetamt" => $line["h_subtotal"],
                "lineamt" => $line["h_subtotal"],
                "m_product_id" => $line["h_product_id"],
                "uom_id" => 0,
                "taxamt" => $sales["taxpct"]*$line["h_subtotal"]/100,
                "weight" => $line["h_weight"],
                "isincludetax" => "f",
                "discountamt" => $line["h_discountitem"],
                "discount" => $line["h_additionaldisc"]
            ]);
            $salesDetailModel->save();
        }
    }
    public function insertSales(){
        $sales = $this->input->post("sales");
        $data = $this->input->post("data");
        $sales = json_decode($sales, true);
        $sales['orderdate'] = DateTime::createFromFormat('d-m-Y', $sales['orderdate'])->format('Y-m-d');
        $sales['dateacct'] = DateTime::createFromFormat('d-m-Y', $sales['dateacct'])->format('Y-m-d');
        $sales['deliverydate'] = DateTime::createFromFormat('d-m-Y', $sales['deliverydate'])->format('Y-m-d');
        $sales['created_at'] = DateTime::createFromFormat('d/m/Y', $sales['created_at'])->format('Y-m-d');
        $sales['updated_at'] = DateTime::createFromFormat('d/m/Y', $sales['updated_at'])->format('Y-m-d');
        $sales['documentno'] = $this->getDocumentNumber();
        unset($sales['detailTable_length']);
        $salesModel = new salesModels();
        $salesModel->fill($sales);
        $salesModel->save();

        foreach ($data as $idx => $line) {
            $salesDetailModel = new salesLineModels();
            $salesDetailModel->timestamps = false;
            $salesDetailModel->fill([
                "t_order_id" => $salesModel->id,
                "qty" => $line["h_qtyEntered"],
                "price" => $line["h_priceEntered"],
                "discountamt" => 0,
                "linenetamt" => $line["h_subtotal"],
                "lineamt" => $line["h_subtotal"],
                "m_product_id" => $line["h_product_id"],
                "uom_id" => 0,
                "taxamt" => $sales["taxpct"]*$line["h_subtotal"]/100,
                "weight" => $line["h_weight"],
                "isincludetax" => "f",
                "discountamt" => $line["h_discountitem"],
                "discount" => $line["h_additionaldisc"],
                "qtyBonus" => $line['h_qtyBonus']
            ]);
            $salesDetailModel->save();
        }

        $this->incrementDocumentNumber();
        print_r($data);
    }

    public function printOrder()
    {
        $order_id = $this->input->get("id");
        $input = FCPATH . 'report/PPS_ReportContainer.jrxml';
//        print_r($input);
        $output = FCPATH . 'report/DeliveryOrder';
        $options = [
            'format' => ['pdf'],
            'locale' => 'en',
            'params' => [
                'ID' => $order_id,
                'REQUIRE_FAK' => 'Y',
                'REQUIRE_DO' => 'Y',
                'REQUIRE_SJ' => 'Y',
                'REQUIRE_LPB' => 'Y',
                'SUBREPORT_DIR' => FCPATH.'report/'
            ],
            'db_connection' => [
                'driver' => 'postgres',
                'username' => env('ELOQUENT_USERNAME',''),
                'password' => env('ELOQUENT_PASSWORD',''),
                'host' => env('ELOQUENT_HOST',''),
                'database' => env('ELOQUENT_DATABASE_NAME',''),
                'port' => '5432',

            ],
            'resources' => FCPATH.'report/Adempiere.jar'
        ];
        $jasper = new PHPJasper;

            $jasper->process(
                $input,
                $output,
                $options
            )->execute();
        $file = file_get_contents($output . '.pdf');
        $filename = 'DeliveryOrder.pdf';
        header('Content-type: application/pdf');
//        header('Content-Disposition: inline; filename="' . $filename . '"');
//        header('Content-Transfer-Encoding: binary');
//        header('Content-Length: ' . filesize($file));
//        header('Accept-Ranges: bytes');

       @readfile($file);
       print_r($file);
    }

    public function getInvoiceByCustomerId()
    {
        $customerId = $this->input->post('customerId');
        $invoices = salesModels::where('m_partner_id', $customerId)->get();

        echo json_encode($invoices);
    }

    public function getDetailInvoiceByCustomerId()
    {
        $invoiceId = $this->input->post('invoiceId');
        $detail = salesModels::where('id', $invoiceId)->get();

        echo json_encode($detail);
    }

    public function salesPriceHistory()
    {
        $data['title'] = "History Harga Jual";
        $data['products'] = productModels::all();
        $data['status'] = 0;
        $this->load->view('Include/Header.php', $data);
        $this->load->view('Include/Sidebar.php');
        $this->load->view('Pesanan/historyHargaJual', $data);
        $this->load->view('Include/Footer.php');
    }

    public function getSalesPriceHistory()
    {
        $product = $this->input->get("item_id");
        $from = $this->input->get("date_from");
        $to = $this->input->get("date_to");

        $history = DB::table('t_order')
            ->select('*')
            ->join('t_order_line', 't_order.id', '=', 't_order_line.t_order_id')
            ->join('m_partner', 'm_partner.id', '=', 't_order.m_partner_id')
            ->where('t_order_line.m_product_id', '=', $product)
            ->whereBetween('t_order.orderdate', [$from, $to]);
        print_r(json_encode($history->get()));

        return $history;
    }

    public function delOrder(){
        salesModels::destroy($this->input->post('id'));
        print_r(true);
    }

    public function receiveOrder(){
        $order = salesModels::find($this->input->post('id'));
        $order->kembaliSuratJalan=true;
        $order->save();
    }

    public function addPesananWithOa($review ="A", $id = 0)
    {
        if($review == "E" && $id == 0)
            $review = "A";

        if($id > 0){
            $data['order'] = salesModels::find($id);
            $data['orderLine'] = salesLineModels::where(['t_order_id' => $id])->get();
        }

        $data['title'] = "Menambah Pesanan";
        $data['review'] = $review;
        $data['partners'] = customerModels::all();
        $data['couriers'] = courierModels::all();
        $data['products'] = productModels::all();

        $this->load->view('Include/Header.php', $data);
        $this->load->view('Include/Sidebar.php');
        $this->load->view('Pesanan/pesananWithOa', $data);
        $this->load->view('Include/Footer.php');
    }
}
