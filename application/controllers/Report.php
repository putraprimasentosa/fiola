<?php

use PHPJasper\PHPJasper;
use Illuminate\Database\Eloquent\Builder;

class Report extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('TargetModels');
    }

    public function index()
    {
        $data['title'] = 'Report';

        $this->load->view('Include/Header.php', $data);
        $this->load->view('Include/Sidebar.php');

        $this->load->view('Common/emptyView.php');

        $this->load->view('Include/Footer.php');
    }

    public function product()
    {
        $input = FCPATH . 'report/product.jrxml';
        //        print_r($input);
        $output = FCPATH . 'report/product';
        $options = [
            'format' => ['pdf'],
            'locale' => 'en',
            'params' => [],
            'db_connection' => [
                'driver' => 'mysql',
                'username' => env('ELOQUENT_USERNAME', ''),
                'password' => env('ELOQUENT_PASSWORD', ''),
                'host' => env('ELOQUENT_HOST', ''),
                'database' => env('ELOQUENT_DATABASE_NAME', ''),
                'port' => '3306',

            ],
            'resources' => FCPATH . 'report/Adempiere.jar'
        ];
        $jasper = new PHPJasper;

        print_r($jasper->process(
            $input,
            $output,
            $options
        )->execute());
        $file = file_get_contents($output . '.pdf');
        $filename = 'DeliveryOrder.pdf';
        header('Content-type: application/pdf');

        @readfile($file);
        print_r($file);
    }

    public function printReportOrder()
    {
        $date1 = $this->input->get("DATE1");
        $date2 = $this->input->get("DATE2");
        $input = FCPATH . 'report/sales-report-1.jrxml';
        //        print_r($input);
        $output = FCPATH . 'report/sales-report';
        $options = [
            'format' => ['pdf'],
            'locale' => 'en',
            'params' => [
                'DATE1' =>  date("Y-m-d", strtotime($date1)),
                'DATE2' => date("Y-m-d", strtotime($date2)),
                'RESOURCE_DIR' => FCPATH . 'report/',
				'ADMIN' => $_SESSION['user']['role'] == 2 ? 0 : $_SESSION['user']['id']
            ],
            'db_connection' => [
                'driver' => 'mysql',
                'username' => env('ELOQUENT_USERNAME', ''),
                'password' => env('ELOQUENT_PASSWORD', ''),
                'host' => env('ELOQUENT_HOST', ''),
                'database' => env('ELOQUENT_DATABASE_NAME', ''),
                'port' => '3306',

            ],
        ];
        $jasper = new PHPJasper;

        print_r($jasper->process(
            $input,
            $output,
            $options
        )->execute());
        $file = file_get_contents($output . '.pdf');
        $filename = 'sales-report.pdf';
        header('Content-type: application/pdf');
        //        header('Content-Disposition: inline; filename="' . $filename . '"');
        //        header('Content-Transfer-Encoding: binary');
        //        header('Content-Length: ' . filesize($file));
        //        header('Accept-Ranges: bytes');

        @readfile($file);
        print_r($file);
    }

	public function printSalesAdmin()
	{
		$date1 = $this->input->get("DATE1");
		$date2 = $this->input->get("DATE2");
		$input = FCPATH . 'report/sales-report-4.jrxml';
		//        print_r($input);
		$output = FCPATH . 'report/sales-report-4';
		$options = [
			'format' => ['pdf'],
			'locale' => 'en',
			'params' => [
				'DATE1' =>  date("Y-m-d", strtotime($date1)),
				'DATE2' => date("Y-m-d", strtotime($date2)),
				'RESOURCE_DIR' => FCPATH . 'report/'
			],
			'db_connection' => [
				'driver' => 'mysql',
				'username' => env('ELOQUENT_USERNAME', ''),
				'password' => env('ELOQUENT_PASSWORD', ''),
				'host' => env('ELOQUENT_HOST', ''),
				'database' => env('ELOQUENT_DATABASE_NAME', ''),
				'port' => '3306',

			],
		];
		$jasper = new PHPJasper;

		print_r($jasper->process(
			$input,
			$output,
			$options
		)->execute());
		$file = file_get_contents($output . '.pdf');
		$filename = 'sales-report.pdf';
		header('Content-type: application/pdf');
		//        header('Content-Disposition: inline; filename="' . $filename . '"');
		//        header('Content-Transfer-Encoding: binary');
		//        header('Content-Length: ' . filesize($file));
		//        header('Accept-Ranges: bytes');

		@readfile($file);
		print_r($file);
	}

    public function sales(){
        $data['title'] = 'Report';
        $data['type'] = 'printReportOrder';
        $data['reportType'] = 'Sales';
        $this->load->view('Include/Header.php', $data);
        $this->load->view('Include/Sidebar.php');
        $this->load->view('Include/Navbar.php');
        $this->load->view('Common/reportGenerator.php',$data);
        $this->load->view('Include/Footer.php');
    }

	public function salesbyadmin(){
		$data['title'] = 'Report';
		$data['type'] = 'printSalesAdmin';
		$data['reportType'] = 'Sales';
		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Include/Navbar.php');
		$this->load->view('Common/reportGenerator.php',$data);
		$this->load->view('Include/Footer.php');
	}
    public function salestarget(){
        $data['title'] = 'Report';
        $data['type'] = 'printReportOrderTarget';
        $data['reportType'] = 'Sales';
        $data['target'] = TargetModels::get();
        $this->load->view('Include/Header.php', $data);
        $this->load->view('Include/Sidebar.php');
        $this->load->view('Include/Navbar.php');
        $this->load->view('Common/reportGeneratorP.php',$data);
        $this->load->view('Include/Footer.php');
    }
    public function salesProd(){
        $data['title'] = 'Report';
        $data['type'] = 'printReportOrderProduct';
        $data['reportType'] = 'Sales By Product';
        $this->load->view('Include/Header.php', $data);
        $this->load->view('Include/Sidebar.php');
        $this->load->view('Include/Navbar.php');
        $this->load->view('Common/reportGenerator.php',$data);

        $this->load->view('Include/Footer.php');
    }
    public function printReportOrderProduct()
    {
        $date1 = $this->input->get("DATE1");
        $date2 = $this->input->get("DATE2");
        $input = FCPATH . 'report/sales-report-2.jrxml';
        //        print_r($input);
        $output = FCPATH . 'report/sales-report-2';
        $options = [
            'format' => ['pdf'],
            'locale' => 'en',
            'params' => [
                'DATE1' =>  date("Y-m-d", strtotime($date1)),
                'DATE2' => date("Y-m-d", strtotime($date2)),
                'RESOURCE_DIR' => FCPATH . 'report/',
				'ADMIN' => $_SESSION['user']['role'] == 2 ? 0 : $_SESSION['user']['id']
            ],
            'db_connection' => [
                'driver' => 'mysql',
                'username' => env('ELOQUENT_USERNAME', ''),
                'password' => env('ELOQUENT_PASSWORD', ''),
                'host' => env('ELOQUENT_HOST', ''),
                'database' => env('ELOQUENT_DATABASE_NAME', ''),
                'port' => '3306',

            ],
        ];
        $jasper = new PHPJasper;

        print_r($jasper->process(
            $input,
            $output,
            $options
        )->execute());
        $file = file_get_contents($output . '.pdf');
        $filename = 'sales-report-2.pdf';
        header('Content-type: application/pdf');
        //        header('Content-Disposition: inline; filename="' . $filename . '"');
        //        header('Content-Transfer-Encoding: binary');
        //        header('Content-Length: ' . filesize($file));
        //        header('Accept-Ranges: bytes');

        @readfile($file);
        print_r($file);
    }
    public function printReportOrderTarget()
    {
        $date1 = $this->input->get("PERIOD");
        $input = FCPATH . 'report/sales-report-3.jrxml';
        //        print_r($input);
        $output = FCPATH . 'report/sales-report-3';
        $options = [
            'format' => ['pdf'],
            'locale' => 'en',
            'params' => [
                'PERIOD' => $date1,
                'RESOURCE_DIR' => FCPATH . 'report/',
				'ADMIN' => $_SESSION['user']['role'] == 2 ? 0 : $_SESSION['user']['id']
            ],
            'db_connection' => [
                'driver' => 'mysql',
                'username' => env('ELOQUENT_USERNAME', ''),
                'password' => env('ELOQUENT_PASSWORD', ''),
                'host' => env('ELOQUENT_HOST', ''),
                'database' => env('ELOQUENT_DATABASE_NAME', ''),
                'port' => '3306',

            ],
        ];  
        $jasper = new PHPJasper;

       print_r($jasper->process(
            $input,
            $output,
            $options
        )->execute());
        $file = file_get_contents($output . '.pdf');
        $filename = 'sales-report-3.pdf';
        header('Content-type: application/pdf');
        @readfile($file);
        print_r($file);
    }

    public function customer()
    {
        $input = FCPATH . 'report/customer.jrxml';
        $output = FCPATH . 'report/customer';
        $options = [
            'format' => ['pdf'],
            'locale' => 'en',
            'params' => [],
            'db_connection' => [
                'driver' => 'mysql',
                'username' => env('ELOQUENT_USERNAME', ''),
                'password' => env('ELOQUENT_PASSWORD', ''),
                'host' => env('ELOQUENT_HOST', ''),
                'database' => env('ELOQUENT_DATABASE_NAME', ''),
                'port' => '3306',

            ],
            'resources' => FCPATH . 'report/Adempiere.jar'
        ];
        $jasper = new PHPJasper;

        print_r($jasper->process(
            $input,
            $output,
            $options
        )->execute());
        $file = file_get_contents($output . '.pdf');
        $filename = 'customer.pdf';
        header('Content-type: application/pdf');

        @readfile($file);
        print_r($file);
    }
    public function admin()
    {
        $input = FCPATH . 'report/admin.jrxml';
        $output = FCPATH . 'report/admin';
        $options = [
            'format' => ['pdf'],
            'locale' => 'en',
            'params' => [],
            'db_connection' => [
                'driver' => 'mysql',
                'username' => env('ELOQUENT_USERNAME', ''),
                'password' => env('ELOQUENT_PASSWORD', ''),
                'host' => env('ELOQUENT_HOST', ''),
                'database' => env('ELOQUENT_DATABASE_NAME', ''),
                'port' => '3306',

            ],
            'resources' => FCPATH . 'report/Adempiere.jar'
        ];
        $jasper = new PHPJasper;

        print_r($jasper->process(
            $input,
            $output,
            $options
        )->execute());
        $file = file_get_contents($output . '.pdf');
        $filename = 'customer.pdf';
        header('Content-type: application/pdf');

        @readfile($file);
        print_r($file);
    }

    public function printSales()
    {
        $date1 = $this->input->get("date1");
        $date2 = $this->input->get("date2");
        $input = FCPATH . 'report/R_RegisterPenjualan_PPS.jasper';
        //        print_r($input);
        $output = FCPATH . 'report/RegisterJual';
        $options = [
            'format' => ['pdf'],
            'locale' => 'en',
            'params' => [
                'DATE1' => date("Y-m-d", strtotime($date1)),
                'DATE2' => date("Y-m-d", strtotime($date2)),

            ],
            'db_connection' => [
                'driver' => 'postgres',
                'username' => env('ELOQUENT_USERNAME', ''),
                'password' => env('ELOQUENT_PASSWORD', ''),
                'host' => env('ELOQUENT_HOST', ''),
                'database' => env('ELOQUENT_DATABASE_NAME', ''),
                'port' => '5432',

            ],
            'resources' => FCPATH . 'report/Adempiere.jar'
        ];
        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        $file = file_get_contents($output . '.pdf');
        $filename = 'DeliveryOrder.pdf';
        header('Content-type: application/pdf');
        @readfile($file);
        print_r($file);
    }
}
