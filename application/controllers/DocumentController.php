<?php
class DocumentController extends CI_Controller {
	private $type;
	private $prefix;
	private $suffix;

	public function __construct() {
		parent:: __construct();
		$this->load->model('DocumentNumberModels');
	}

	public function setNumberData(string $type, string $prefix, string $suffix = null) {
		$this->type = $type;
		$this->prefix = $prefix;
		$this->suffix = $suffix;
	}

	private function getCurrentPeriod() {
		$currentDate = new DateTime();
		$currentMonth = $currentDate->format('m');
		$currentYear = $currentDate->format('y');
		return $currentMonth.$currentYear;
	}

	private function constructDocumentNumber(DocumentNumberModels $numberData) {
		return $numberData->prefix.$numberData->period.'/'.getPadZero($numberData->no).$numberData->suffix;
	}

	public function getDocumentNumber(string $period = null) {
		$periodToSearch = $period === null ? $this->getCurrentPeriod() : $period;

		$existingDocumentNo = DocumentNumberModels::where([
			'type' => $this->type,
			'period' => $periodToSearch
		])->first();

		if ($existingDocumentNo !== null) {
			return $this->constructDocumentNumber($existingDocumentNo);
		}

		$newDocumentNo = DocumentNumberModels::create([
			'type' => $this->type,
			'period' => $periodToSearch,
			'prefix' => $this->prefix,
			'suffix' => $this->suffix,
			'no' => DOCUMENT_NO_STARTING_NUMBER
		]);

		return $this->constructDocumentNumber($newDocumentNo);
	}

	public function incrementDocumentNumber(string $period = null) {
		$existingNumber = DocumentNumberModels::where('type', $this->type)
			->where('period', $period === null ? $this->getCurrentPeriod() : $period)
			->firstOrFail();
		$existingNumber->no = $existingNumber->no + 1;
		$existingNumber->save();
	}
}
