<?php

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Capsule\Manager as DB;
use PHPJasper\PHPJasper;

require_once('DocumentController.php');

class Sales extends DocumentController
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('customerModels');
		$this->load->model('courierModels');
		$this->load->model('commerceModels');
		$this->load->model('productModels');
		$this->load->model('salesModels');
		$this->load->model('salesLineModels');
		$this->setNumberData("SALES_ORDER", "FJ/");
	}

	public function all()
	{
		$data['title'] = "Sales List";
		$data['orders'] = salesModels::with('customer', 'courier')->where('admin_id', $_SESSION['user']['role'] == 2 ? '>=' : '=', $_SESSION['user']['role'] == 2 ? '0' : $_SESSION['user']['id'])->orderby('transaction_no','desc')->get();

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Include/Navbar.php');
		$this->load->view('Pesanan/allData', $data);
		$this->load->view('Include/Footer.php');
	}

	public function view($id)
	{
		$header['title'] = "View Penjualan";
		$data['order'] = salesModels::where('id', $id)->with('customer')->with('courier')->with('commerce')->first();
		$data['orderLine'] = salesLineModels::where('sales_id', $id)->with('product')->get();
		$data['orderID'] = $id;

		$this->load->view('Include/Header.php', $header);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Include/Navbar.php');
		$this->load->view('vSales.php', $data);
		$this->load->view('Include/Footer.php');
	}

	public function add()
	{
		$data['title'] = "Menambah Pesanan";
		$data['customers'] = customerModels::all();
		$data['couriers'] = courierModels::all();
		$data['products'] = productModels::all();
		$data['commerces'] = CommerceModels::all();
		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Include/Navbar.php');
		$this->load->view('sales.php', $data);
		$this->load->view('Include/Footer.php');
	}

	public function updatePesanan($id = 0)
	{
		if ($id > 0) {
			$data['orders'] = salesModels::find($id);
			$data['orderLines'] = salesLineModels::where(['t_order_id' => $id])->get();
		}

		$data['title'] = "Menambah Pesanan";
		$data['partners'] = customerModels::all();
		$data['couriers'] = courierModels::all();
		$data['products'] = productModels::all();
		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Pesanan/updatePesanan', $data);
		$this->load->view('Include/Footer.php');
	}

	public function getLineDetails()
	{
		$order_id = $this->input->get("t_order_id");
		$data = salesLineModels::where(['t_order_id' => $order_id])->with('product')->get();
		print_r(json_encode($data));
	}

	public function getPrice()
	{
		$product_id = $this->input->post("id");
		$address = $this->input->post("address");
		//$price = priceModels::where(['m_product_id' => $product_id, 'm_partner_id' => $partner_id > 0 ? $partner_id : 0])->get();
		$product = productModels::select("unitprice")->where('id', '=', $product_id)->get();

	}

	public function getCourierCost()
	{
		$courier_id = $this->input->post("id");
		$m_city_id = $this->input->post("cityId");

		echo FareModels::select("price")
			->where('m_courier_id', '=', $courier_id)
			->where('m_province_city_id', '=', $m_city_id)
			->get();
	}

	public function getOrdersByCustomerId($customerId)
	{
		$excludeReturnedOrders = $this->input->get('excludeReturn');

		if (!isset($excludeReturnedOrders)) {
			$customer = CustomerModels::find($customerId);
			echo json_encode($customer->sales);
			return;
		}

		$returnedSales = SalesReturnModels::whereHas('order', function (Builder $query) use ($customerId) {
			$query->where('m_partner_id', '=', $customerId);
		})
			->get()
			->map(function ($salesReturn) {
				return $salesReturn->t_order_id;
			})
			->toArray();
		$availableOrders = SalesModels::with('shippingAddress')
			->where('m_partner_id', $customerId)
			->whereNotIn('id', $returnedSales)
			->orderBy('id', 'desc')
			->get();

		echo json_encode($availableOrders);
	}

	public function getOrder($id)
	{
		$orderInfo = salesModels::with('courier')->find($id);
		$orderDetail = $orderInfo->details;

		echo json_encode([
			'orderInfo' => $orderInfo,
			'orderDetail' => $orderDetail
		]);
	}

	public function getPartnerAddress()
	{
		$partner = $this->input->get("id");
		$addresses = ShippingAddressModels::where('m_partner_id', $partner)->with('city')->get();
		$partner = CustomerModels::find($partner);
		print_r(json_encode(['addresses' => $addresses, 'partner' => $partner]));
		return $addresses;
	}

	public function getAddress()
	{
		$partner = $this->input->get("id");
		$addresses = ShippingAddressModels::find($partner);
		print_r(json_encode($addresses));
		return $addresses;
	}

	public function updateSales()
	{
		$sales = $this->input->post("sales");
		$data = $this->input->post("data");
		$sales = json_decode($sales, true);
		$sales['orderdate'] = DateTime::createFromFormat('d-m-Y', $sales['orderdate'])->format('Y-m-d');
		$sales['dateacct'] = DateTime::createFromFormat('d-m-Y', $sales['dateacct'])->format('Y-m-d');
		$sales['deliverydate'] = DateTime::createFromFormat('d-m-Y', $sales['deliverydate'])->format('Y-m-d');
		$sales['created_at'] = DateTime::createFromFormat('d/m/Y', $sales['created_at'])->format('Y-m-d');
		$sales['updated_at'] = DateTime::createFromFormat('d/m/Y', $sales['updated_at'])->format('Y-m-d');
		if (!isset($sales["isborongan"]))
			$sales["isborongan"] = "f";
		unset($sales['detailTable_length']);
		$id = $this->input->post("id");
		$salesModel = salesModels::find($id);
		$salesModel->fill($sales);
		$salesModel->save();

		salesLineModels::where(['t_order_id' => $id])->forceDelete();

		foreach ($data as $idx => $line) {
			$salesDetailModel = new salesLineModels();
			$salesDetailModel->timestamps = false;
			$salesDetailModel->fill([
				"t_order_id" => $salesModel->id,
				"qty" => $line["h_qtyEntered"],
				"price" => $line["h_priceEntered"],
				"discountamt" => 0,
				"linenetamt" => $line["h_subtotal"],
				"lineamt" => $line["h_subtotal"],
				"m_product_id" => $line["h_product_id"],
				"uom_id" => 0,
				"taxamt" => $sales["taxpct"] * $line["h_subtotal"] / 100,
				"weight" => $line["h_weight"],
				"isincludetax" => "f",
				"discountamt" => $line["h_discountitem"],
				"discount" => $line["h_additionaldisc"]
			]);
			$salesDetailModel->save();
		}
	}

	public function insertSales()
	{
		$sales = $this->input->post("sales");
		$data = $this->input->post("data");
		$counter = DB::select(DB::raw("
       SELECT coalesce(max(transaction_no)+1,1) as documentno FROM t_sales
    "));

		$sales = json_decode($sales, true);
		$sales['transaction_no'] = $counter[0]['documentno'];
		$sales['admin_id'] = $_SESSION['user']['id'];
		$salesModel = new salesModels();
		$salesModel->fill($sales);
		$salesModel->save();


		foreach ($data as $idx => $line) {
			$salesDetailModel = new salesLineModels();
			$salesDetailModel->timestamps = false;
			$salesDetailModel->fill([
				"sales_id" => $salesModel->id,
				"product_id" => $line["h_product"],
				"unitprice" => $line["h_price"],
				"sales_qty" => $line['h_qty'],
				"lineamt" => $line["h_qty"] * $line["h_price"]
			]);
			$salesDetailModel->save();
		}

		// $this->incrementDocumentNumber();

	}

	public function printOrder()
	{
		$order_id = $this->input->get("id");
		$input = FCPATH . 'report/PPS_ReportContainer.jrxml';
//        print_r($input);
		$output = FCPATH . 'report/DeliveryOrder';
		$options = [
			'format' => ['pdf'],
			'locale' => 'en',
			'params' => [
				'ID' => $order_id,
				'REQUIRE_FAK' => 'Y',
				'REQUIRE_DO' => 'Y',
				'REQUIRE_SJ' => 'Y',
				'REQUIRE_LPB' => 'Y',
				'SUBREPORT_DIR' => FCPATH . 'report/'
			],
			'db_connection' => [
				'driver' => 'postgres',
				'username' => env('ELOQUENT_USERNAME', ''),
				'password' => env('ELOQUENT_PASSWORD', ''),
				'host' => env('ELOQUENT_HOST', ''),
				'database' => env('ELOQUENT_DATABASE_NAME', ''),
				'port' => '5432',

			],
			'resources' => FCPATH . 'report/Adempiere.jar'
		];
		$jasper = new PHPJasper;

		$jasper->process(
			$input,
			$output,
			$options
		)->execute();
		$file = file_get_contents($output . '.pdf');
		$filename = 'DeliveryOrder.pdf';
		header('Content-type: application/pdf');
//        header('Content-Disposition: inline; filename="' . $filename . '"');
//        header('Content-Transfer-Encoding: binary');
//        header('Content-Length: ' . filesize($file));
//        header('Accept-Ranges: bytes');

		@readfile($file);
		print_r($file);
	}

	public function getInvoiceByCustomerId()
	{
		$customerId = $this->input->post('customerId');
		$invoices = salesModels::where('m_partner_id', $customerId)->get();

		echo json_encode($invoices);
	}

	public function getDetailInvoiceByCustomerId()
	{
		$invoiceId = $this->input->post('invoiceId');
		$detail = salesModels::where('id', $invoiceId)->get();

		echo json_encode($detail);
	}

	public function salesPriceHistory()
	{
		$data['title'] = "History Harga Jual";
		$data['products'] = productModels::all();
		$data['status'] = 0;
		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Pesanan/historyHargaJual', $data);
		$this->load->view('Include/Footer.php');
	}

	public function targetReal()
	{
		$selectedMonth = $this->input->get('month');
		$year = $selectedMonth != null ? explode("|",$selectedMonth)[1] : date("Y");
		$selectedMonth = date($year.'-'.($selectedMonth != null ? explode("|",$selectedMonth)[0] : 'm').'-01');
		$first_day_this_month = date('Y-m-01', strtotime($selectedMonth));
		$last_day_this_month = date('Y-m-t',strtotime($selectedMonth));
		$period = strtoupper(date('M-Y'));
		$sales = $this->input->get('sales');
		$sql = "select * from m_product p
				left join (select product_id, sum(tl.target_qty) as target from
				c_targetline tl JOIN c_target t ON tl.target_id = t.id
				where '".$selectedMonth."' between t.validfrom and t.validto and tl.sales_id = '".$sales."'
				group by product_Id
				) as x1 on x1.product_id = p.id
				left join (select product_id, sum(sl.sales_qty) as realization
				from t_salesline sl
				join t_sales s on sl.sales_id = s.id
				where s.transaction_date between '" . $first_day_this_month . "' AND '" . $last_day_this_month . "' and s.admin_id = '".$sales."'
				group by product_Id
				) as x2 on x2.product_Id = p.id";

		$data['target'] = $this->db->query($sql)->result_array();

		print_r (
			json_encode($data['target'],true)
		);
	}

	public function getSalesPriceHistory()
	{
		$product = $this->input->get("item_id");
		$from = $this->input->get("date_from");
		$to = $this->input->get("date_to");

		$history = DB::table('t_order')
			->select('*')
			->join('t_order_line', 't_order.id', '=', 't_order_line.t_order_id')
			->join('m_partner', 'm_partner.id', '=', 't_order.m_partner_id')
			->where('t_order_line.m_product_id', '=', $product)
			->whereBetween('t_order.orderdate', [$from, $to]);
		print_r(json_encode($history->get()));

		return $history;
	}

	public function delOrder()
	{
		salesModels::destroy($this->input->post('id'));
		print_r(true);
	}

	public function receiveOrder()
	{
		$order = salesModels::find($this->input->post('id'));
		$order->kembaliSuratJalan = true;
		$order->save();
	}

	public function addPesananWithOa($review = "A", $id = 0)
	{
		if ($review == "E" && $id == 0)
			$review = "A";

		if ($id > 0) {
			$data['order'] = salesModels::find($id);
			$data['orderLine'] = salesLineModels::where(['t_order_id' => $id])->get();
		}

		$data['title'] = "Menambah Pesanan";
		$data['review'] = $review;
		$data['partners'] = customerModels::all();
		$data['couriers'] = courierModels::all();
		$data['products'] = productModels::all();

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Pesanan/pesananWithOa', $data);
		$this->load->view('Include/Footer.php');
	}
}
