<?php
require_once('DocumentController.php');

use Illuminate\Database\Capsule\Manager as Capsule;

class ReturPenjualanApi extends DocumentController {
	public function __construct() {
		parent::__construct();
		$this->load->model('SalesReturn/SalesReturnModels');
		$this->load->model('SalesReturn/SalesReturnDetailModels');
		$this->setNumberData(DOCUMENT_TYPE_SALES_RETURN, DOCUMENT_NO_PREFIX_SALES_RETURN);
	}

	private function getReturnSubtotal($returnDetailRequest, $returnDetailRecord) {
		$pricePerUnit = ($returnDetailRequest->subtotal + $returnDetailRequest->tax) / $returnDetailRequest->qty;
		$numberOfGoods = $returnDetailRequest->returnAmount;

		$subtotalReturn = $numberOfGoods * $pricePerUnit;

		$returnDetailRecord->subtotalreturn = $subtotalReturn;
		$returnDetailRecord->save();

		return $returnDetailRecord;
	}

	private function checkIfAllDetailsAreZeros($returnDetailList) {
		$numberOfZeros = 0;
		foreach ($returnDetailList as $detail) {
			if ($detail->returnAmount === 0) {
				$numberOfZeros++;
			}
		}

		if ($numberOfZeros === count($returnDetailList)) {
			throw new Exception('All details are zero.');
		}
	}

	public function createSalesReturn() {
		$info = json_decode($this->input->post('info'), true);
		$detail = json_decode($this->input->post('detail'));
		$totalReturnValue = 0;

		try {
			$this->checkIfAllDetailsAreZeros($detail);
			Capsule::beginTransaction();

			$salesReturn = SalesReturnModels::create([
				't_order_id' => $info['orderId'],
				'returnreceiptno' => $info['returnReceiptNo'],
				'returndate' => DateTime::createFromFormat('d-m-Y', $info['returnDate'])->format('Y-m-d'),
				'returnno' => $info['returnNo'],
				'vehiclelicenseplate' => $info['licensePlateNo'],
				'status' => RETURN_STATUS_CREATED
			]);

			foreach ($detail as $item) {
				if ($item->returnAmount === 0) {
					continue;
				}

				$salesReturnDetail = new SalesReturnDetailModels();
				$salesReturnDetail->fill([
					'm_product_id' => $item->id,
					'totalqty' => $item->returnAmount,
					'returnDescription' => $item->returnDescription
				]);

				$salesReturnDetail->parent()->associate($salesReturn);
				$salesReturnDetail->save();

				$updatedDetail = $this->getReturnSubtotal($item, $salesReturnDetail);

				$totalReturnValue = $totalReturnValue + $updatedDetail->subtotalreturn;
			}

			$salesReturn->totalreturnvalue = $totalReturnValue;

			$salesReturn->save();

			$this->incrementDocumentNumber();

			Capsule::commit();

			$this->output->set_status_header(CREATED_CODE);
			echo json_encode([
				'id' => $salesReturn->id,
			]);
		} catch (Exception $error) {
			Capsule::rollback();
			log_message(LOG_LEVEL_ERROR, $error->getMessage());

			$this->output->set_status_header(INTERNAL_SERVER_ERROR_CODE);
			$this->data['message'] = $error->getMessage();

			echo json_encode($this->data);
		}
	}
}
