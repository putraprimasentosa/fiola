<?php

use PHPJasper\PHPJasper;

require_once('DocumentController.php');

class KlaimReturPenjualan extends DocumentController {
	public function __construct() {
		parent:: __construct();
		$this->load->model('CourierModels');
		$this->load->model('SalesModels');
		$this->load->model('SalesReturn/SalesReturnModels');
		$this->load->model('SalesReturn/SalesReturnDetailModels');
		$this->load->model('SalesReturn/SalesReturnClaimModels');
		$this->load->model('SalesReturn/SalesReturnClaimDetailModels');
		$this->load->model('SalesReturn/SalesReturnClaimConditionDetailModels');
		$this->load->model('SalesReturn/GoodsConditionModels');
		$this->load->model('ProductModels');
		$this->setNumberData(DOCUMENT_TYPE_SALES_RETURN_CLAIM, DOCUMENT_NO_PREFIX_SALES_RETURN_CLAIM);
	}

	public function showAddSalesReturnClaim($salesReturnId) {
		$documentNo = $this->getDocumentNumber();
		$salesReturnData = SalesReturnModels::with('details', 'order', 'details.item')
			->find($salesReturnId);

		$data = [
			'title' => 'Tambah Klaim Retur Penjualan',
			'salesReturnData' => $salesReturnData,
			'documentNo' => $documentNo
		];

		$this->load->view('include/Header', $data);
		$this->load->view('include/Sidebar');
		$this->load->view('SalesReturnClaim/addSalesReturnClaim', $data);
		$this->load->view('include/Footer');
	}

	public function showReturnClaimDetail($returnClaimId) {
		$returnClaimData = SalesReturnClaimModels::with(
			'details',
			'salesReturn'
		)->find($returnClaimId);
		$conditions = GoodsConditionModels::all();

		if ($returnClaimData === null) {
			show_404();
		}

		$data = [
			'title' => 'Detail Klaim Return',
			'conditions' => $conditions,
			'returnClaimData' => $returnClaimData,
		];

		$this->load->view('include/Header', $data);
		$this->load->view('include/Sidebar');
		$this->load->view('SalesReturnClaim/salesReturnClaimDetail', $data);
		$this->load->view('include/Footer');
	}

	public function printOrder($claimSalesReturnId) {
		$input = FCPATH.'report/PPS_Nota_Claim_A5.jrxml';
		$output = FCPATH.'report/Retur';
		$options = [
			'format' => ['pdf'],
			'locale' => 'en',
			'params' => [
				'ID' => $claimSalesReturnId,
			],
			 'db_connection' => [
                'driver' => 'postgres',
                'username' => env('ELOQUENT_USERNAME',''),
                'password' => env('ELOQUENT_PASSWORD',''),
                'host' => env('ELOQUENT_HOST',''),
                'database' => env('ELOQUENT_DATABASE_NAME',''),
                'port' => '5432',
			],
			'resources' => FCPATH.'report/Adempiere.jar'
		];
		$jasper = new PHPJasper;

		$jasper->process(
			$input,
			$output,
			$options
		)->execute();
		$file = file_get_contents($output.'.pdf');
		header('Content-type: application/pdf');

		@readfile($file);
		print_r($file);
	}
}
