<?php
class MasterApi extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('cityModels');
    }

    public function getCity()
    {
        $provinceId = $this->input->post('provinceId');
        $citiesInProvince = cityModels::where('m_province_id', $provinceId)->get();

        echo json_encode($citiesInProvince);
    }

}