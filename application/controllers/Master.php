<?php

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Capsule\Manager as DB;

class Master extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('courierModels');
		$this->load->model('fareModels');
		$this->load->model('productModels');
		$this->load->model('UOMModels');
		$this->load->model('satuanModels');
		$this->load->model('productCategoryModels');
		$this->load->model('customerModels');
		$this->load->model('cityModels');
		$this->load->model('provinceModels');
		$this->load->model('shippingAddressModels');
		$this->load->model('schemaBonusModels');
		$this->load->model('schemaDiscountModels');
		$this->load->model('priceModels');
		$this->load->model('AdminModels');
		$this->load->model('CommerceModels');
		$this->load->model('TargetModels');
		$this->load->model('TargetLineModels');

	}

	public function courier()
	{
		$data['title'] = "Master Ekspedisi";
		$data['Courier'] = courierModels::get();
		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Include/Navbar.php');
		$this->load->view('Master/Courier', $data);
		$this->load->view('Include/Footer.php');
	}

	public function commerce()
	{
		$data['title'] = "Master E-Commerce";
		$data['commerce'] = CommerceModels::get();
		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Include/Navbar.php');
		$this->load->view('Master/Commerce', $data);
		$this->load->view('Include/Footer.php');
	}

	public function province()
	{
		$data['title'] = "Master Province";
		$data['province'] = ProvinceModels::get();
		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Include/Navbar.php');
		$this->load->view('Master/Province', $data);
		$this->load->view('Include/Footer.php');
	}

	public function city($idProvinsi = 0)
	{
		$data['title'] = "Daftar Kota";
		$data['Cities'] = cityModels::where('m_province_id', $idProvinsi)->lists('name');
		$data['Province'] = provinceModels::where('id', $idProvinsi)->first();
		$data['idProvinsi'] = $idProvinsi;

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Include/Navbar.php');
		$this->load->view('Master/City', $data);
		$this->load->view('Include/Footer.php');
	}

	public function getFare()
	{
		$draw = 1;
		$length = 25;
		$start = 0;
		$search = '';

		if ($this->input->post('draw')) {
			$draw = $this->input->post('draw');
		}

		if ($this->input->post('length')) {
			$length = $this->input->post('length');
		}

		if ($this->input->post('start')) {
			$start = $this->input->post('start');
		}

		if ($this->input->post('search[value]') && $this->input->post('search[value]') !== '') {
			$search = $this->input->post('search[value]');
			$fare_raw = fareModels::select('m_fare.id as fare_id', 'm_courier.code as courier_code', 'm_courier.name as courier_name', 'm_city.name as city_name', 'm_province.name as province_name', 'price', 'isactive')
				->leftJoin('m_courier', 'm_fare.m_courier_id', '=', 'm_courier.id')
				->leftJoin('m_city', 'm_fare.m_province_city_id', '=', 'm_city.id')
				->leftJoin('m_province', 'm_city.m_province_id', '=', 'm_province.id')
				->where('m_city.name', 'ilike', '%' . $search . '%')
				->orWhere('m_province.name', 'ilike', '%' . $search . '%')
				->orwhere('m_courier.name', 'ilike', '%' . $search . '%')
				->orWhere('m_courier.code', 'ilike', '%' . $search . '%');
		} else {
			$fare_raw = fareModels::select('m_fare.id as fare_id', 'm_courier.code as courier_code', 'm_courier.name as courier_name', 'm_city.name as city_name', 'm_province.name as province_name', 'price', 'isactive')
				->leftJoin('m_courier', 'm_fare.m_courier_id', '=', 'm_courier.id')
				->leftJoin('m_city', 'm_fare.m_province_city_id', '=', 'm_city.id')
				->leftJoin('m_province', 'm_city.m_province_id', '=', 'm_province.id');
		}

		// $total = fareModels::with([
		//   'courier' => function($qR) use($search) {
		//     $qR->where('m_courier.name', 'like', '%' .$search. '%')
		//     ->orWhere('m_courier.code', 'like', '%' .$search. '%');
		//   },
		//   'city' => function($q) use($search) {
		//       $q->leftjoin('m_province', 'm_province.id', '=', 'm_city.m_province_id')
		//       ->where('m_city.name', 'like', '%' .$search. '%')
		//       ->orWhere('m_province.name', 'like', '%' .$search. '%');
		// }])->count();

		$total = $fare_raw->count();
		$fare_raw = $fare_raw->offset($start)->limit($length)->get();
		$fare_raw = json_decode($fare_raw, true);
		$fare_updated = [];

		// var_dump($fare_raw);
		// $original_province = json_decode(provinceModels::all(), true);

		foreach ($fare_raw as $f) {
			// $province_id = array_search($f['city']['m_province_id'] ,array_column($original_province, 'id'));
			// $province_name = $original_province[$province_id]['name'];

			array_push($fare_updated, [
				$f['fare_id'],
				'(' . $f['courier_code'] . ') ' . $f['courier_name'],
				'(' . $f['province_name'] . ') ' . $f['city_name'],
				$f['price'],
				'<td width="5%" align="center">' .
				'<a href="' . base_url() . 'master/editFare/' . $f['fare_id'] . '" class="btn btn-warning">' .
				'<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>' .
				'</a>' .
				'</td>',
				'<td width="5%" align="center">' .
				'<a class="btn btn-danger delete-fare" data-id="' . $f['fare_id'] . '">' .
				'<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>' .
				'</a>' .
				'</td>'
			]);
		}
		// var_dump( $fare_updated );

		$output = array(
			"draw" => $draw,
			"recordsTotal" => $total,
			"recordsFiltered" => $total,
			"data" => $fare_updated,
		);

		$fare = json_encode($output, true);
		echo $fare;
	}

	public function insertFare()
	{
		// $fare['created_at'] = DateTime::createFromFormat('d/m/Y', $fare['created_at'])->format('Y-m-d H:i:s');
		// $fare['updated_at'] = DateTime::createFromFormat('d/m/Y', $fare['updated_at'])->format('Y-m-d H:i:s');

		$fare = $this->input->post('fare');
		$fare = json_decode($fare, true);
		$existingFare = fareModels::where('m_courier_id', $fare['m_courier_id'])
			->where('m_province_city_id', $fare['m_province_city_id'])
			->first();
		if ($existingFare) {
			$data['code'] = '414';
			$data['message'] = 'Harga tarif untuk lokasi tujuan sudah terdaftar.';
		} else {
			$data['code'] = '200';
			$newFare = new fareModels();
			$newFare->fill($fare);
			$newFare->save();
		}
		echo json_encode($data);
	}

	public function editFare($id)
	{
		$data['title'] = " Ubah Master Tarif";
		$data['Courier'] = courierModels::all();
		$data['City'] = cityModels::all();
		$data['Province'] = provinceModels::all();
		$data['data'] = fareModels::where('id', $id)->first();
		$data['fare_city'] = cityModels::where('id', $data['data']->m_province_city_id)->first();

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Master/editFare', $data);
		$this->load->view('Include/Footer.php');
	}

	public function updateFare($id = 0)
	{
		//$fare['updated_at'] = DateTime::createFromFormat('d/m/Y', $fare['updated_at'])->format('Y-m-d H:i:s');
		$fare = $this->input->post('fare');
		$fare = json_decode($fare, true);

		$existingFare = fareModels::where('m_courier_id', $fare['m_courier_id'])
			->where('m_province_city_id', $fare['m_province_city_id'])
			->first();
		if ($existingFare->id != $id) {
			$data['code'] = '414';
			$data['message'] = 'Lokasi yang dipilih sudah terdaftar.';
		} else {
			$data['code'] = '200';
			$newFare = new fareModels();
			$newFare = fareModels::where('id', $id)->orderBy('id', 'DESC')->first();
			if ($newFare !== null) {
				$newFare->fill($fare);
				$newFare->save();
			}
		}

		echo json_encode($data);
	}

	public function deleteFare()
	{
		$id = json_decode($this->input->post('id'), true);
		$fare = fareModels::destroy($id);
	}

	public function activeAdmin()
	{
		$admin = AdminModels::find($this->input->post('id'));
		$admin["status"] = 1;
		$admin->save();
	}

	public function deActiveAdmin()
	{
		$admin = AdminModels::find($this->input->post('id'));
		$admin["status"] = 0;
		$admin->save();
	}

	public function customer()
	{
		$data['title'] = "Master Pelanggan";
		$data['Customer'] = customerModels::get();
		foreach ($data['Customer'] as $address) {
			$address['province'] = provinceModels::where('id', $address['m_province_id'])->pluck('name');
			$address['city'] = cityModels::where('id', $address['m_city_id'])->pluck('name');
		}
		$data['Province'] = provinceModels::all();

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Include/Navbar.php');
		$this->load->view('Master/Customer', $data);
		$this->load->view('Include/Footer.php');
	}

	public function target()
	{
		$data['title'] = "Master Target";
		$data['target'] = TargetModels::orderBy('validto', 'desc')->get();
		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Include/Navbar.php');
		$this->load->view('Master/Target.php', $data);
		$this->load->view('Include/Footer.php');
	}

	public function targetLine($id)
	{
		$data['title'] = "Master Target";
		$data['target'] = TargetModels::find($id);
		$data['targetList'] = TargetModels::where('id', '!=', $id)->get();
		$data['line'] = TargetLineModels::where('target_id', '=', $id)->with('product')->get();
		$data['product'] = productModels::get();
		$data['sales'] = AdminModels::where('role', '=', 1)->get();

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Include/Navbar.php');
		$this->load->view('Master/TargetLine.php', $data);
		$this->load->view('Include/Footer.php');
	}

	public function editMasterCustomer($idPartner)
	{
		$data['title'] = " Ubah Master Pelanggan";
		$data['Province'] = provinceModels::all();
		$data['data'] = customerModels::where('id', $idPartner)->first();

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Master/editCustomer', $data);
		$this->load->view('Include/Footer.php');
	}

	public function insertCustomer()
	{
		$partner = $this->input->post('partner');
		$customer = new customerModels();
		$partner = json_decode($partner, true);
		$partner['isactive'] = 1;
		$customer->fill($partner);
		$customer->save();
		//echo $customer;
	}

	public function insertCommerce()
	{
		$com = $this->input->post('commerce');
		$commerce = new CommerceModels();
		$com = json_decode($com, true);
		$commerce->fill($com);
		$commerce->save();
		//echo $customer;
	}

	public function insertCourier()
	{
		$cour = $this->input->post('courier');
		$courier = new courierModels();
		$cour = json_decode($cour, true);
		$courier->fill($cour);
		$courier->save();
		//echo $customer;
	}

	public function updateCustomer()
	{
		$idCustomer = $this->input->post('id');
		$_partner = $this->input->post('partner');
		$_partner = json_decode($_partner, true);
		//$partner['updated_at'] = DateTime::createFromFormat('d/m/Y', $partner['updated_at'])->format('Y-m-d H:i:s');
		$partner = customerModels::where('id', $idCustomer)->first();
		if ($partner !== null) {
			$partner->fill($_partner);
			$partner->save();
		}
	}


	public function addProduct()
	{
		$data['title'] = "Menambah Master Produk";
		$data['Satuan'] = satuanModels::all();
		$data['Category'] = productCategoryModels::all();

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Master/AddProduct', $data);
		$this->load->view('Include/Footer.php');
	}

	public function insertAdmin()
	{
		$data = $this->input->post('admin');
		$data = json_decode($data, true);
		$data["password"] = password_hash($data["password"], PASSWORD_DEFAULT);
		$admin = new AdminModels();
		$admin->fill($data);
		$admin->save();
	}

	public function insertProduct()
	{
		$_product = $this->input->post('product');
		$product = new productModels();
		$_product = json_decode($_product, true);

		$product->fill($_product);
		$product->save();
	}

	public function insertTarget()
	{
		$_target = $this->input->post('target');
		$_target = json_decode($_target, true);
		$period = $_target["month"] . '-' . $_target["year"];
		$target = new TargetModels();
		$validfrom = date('Y-m-d', strtotime($_target["month"] . ' 1 ' . $_target["year"]));
		$validto = date('Y-m-t', strtotime($_target["month"] . ' 1 ' . $_target["year"]));
		$target->fill([
			"period" => $period,
			"validfrom" => $validfrom,
			"validto" => $validto
		]);
		$target->save();
	}

	public function insertTargetLine()
	{
		$_target = $this->input->post('target');
		$_target = json_decode($_target, true);
		$target = new TargetLineModels();
		$target->fill($_target);
		$target->save();
	}

	public function copyTarget()
	{
		$data = $this->input->post("data");
		$target_id = $this->input->post("target_id");
		$sales_id = $this->input->post("sales_id");
		foreach ($data as $idx => $val) {
			$tl = new TargetLineModels();
			$tl->fill(
				[
					"target_id" => $target_id,
					"product_id" => $val['product_id'],
					"sales_id" => $sales_id,
					"target_qty" => $val['quantity']
				]
			);
			$tl->save();
		}
	}

	public function getTargetLineSalesProduct()
	{
		$sales_id = $this->input->get('sales_id');
		$target_id = $this->input->get('target_id');
		$data['targetLineProducts'] = DB::table('c_targetline')
			->join('m_product', 'c_targetline.product_id', '=', 'm_product.id')
			->select('c_targetline.sales_id', 'c_targetline.product_id', 'm_product.product_name as product', 'c_targetline.target_qty as quantity')
			->where(['c_targetline.sales_id' => $sales_id, 'c_targetline.target_id' => $target_id])
			->get();
		print_r(json_encode($data['targetLineProducts']));
	}

	public function getTargetLineSalesTarget()
	{
		$sales_id = $this->input->get('sales_id');
		$target_id = $this->input->get('target_id');
		$cur_target_id = $this->input->get('cur_target_id');
		$data['targetLineProducts'] = DB::table('c_targetline')
			->join('m_product', 'c_targetline.product_id', '=', 'm_product.id')
			->select('c_targetline.sales_id', 'c_targetline.product_id', 'm_product.product_name as product', 'c_targetline.target_qty as quantity')
			->where(['c_targetline.sales_id' => $sales_id, 'c_targetline.target_id' => $target_id])
			->whereNotIn('c_targetline.product_id', DB::table('c_targetline')->select('product_id')->where(['target_id' => $cur_target_id, 'sales_id' => $sales_id])->get())
			->get();
		print_r(json_encode($data['targetLineProducts']));
	}

	public function editMasterProduct($idProduct)
	{
		$data['title'] = "Ubah Master Product";
		$data['Satuan'] = satuanModels::all();
		$data['Category'] = productCategoryModels::all();
		$data['data'] = productModels::where('id', $idProduct)->first();

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Master/editProduct', $data);
		$this->load->view('Include/Footer.php');
	}

	public function updateProduct()
	{
		$idProduct = $this->input->post('id');
		$_product = $this->input->post('product');
		$_product = json_decode($_product, true);
		//$partner['updated_at'] = DateTime::createFromFormat('d/m/Y', $partner['updated_at'])->format('Y-m-d H:i:s');
		$product = productModels::where('id', $idProduct)->first();
		if ($product !== null) {
			$product->fill($_product);
			$product->save();
		}
	}

	public function bonusSchema($idPartner = 0)
	{
		$data['title'] = "Skema Bonus";

		$data['Satuan'] = satuanModels::all();
		$data['Schema'] = schemaBonusModels::where('m_shippingaddress_id', $idPartner)->get();
		foreach ($data['Schema'] as $schema) {
			$schema['barang'] = productModels::where('id', $schema['m_product_id'])->pluck('name');
			$schema['barang_bonus'] = productModels::where('id', $schema['m_product_id_bonus'])->pluck('name');
		}
		$data['shippingAddress'] = shippingAddressModels::where('id', $idPartner)->first();
		$data['Products'] = productModels::all();

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Master/bonusSchema', $data);
		$this->load->view('Include/Footer.php');
	}

	public function insertBonusSchema()
	{
		$_bonusSchema = $this->input->post('schema');
		$bonusSchema = new schemaBonusModels();
		$_bonusSchema = json_decode($_bonusSchema, true);
		$bonusSchema->fill($_bonusSchema);
		$bonusSchema->save();
	}

	public function delTargetLine()
	{
		$target_id = $this->input->post('target_id');
		$product = $this->input->post('product');
		$sales_id = $this->input->post('sales_id');
		print_r($target_id);
		print_r($product);

		$targetLine = TargetLineModels::where([
			'target_id' => $target_id,
			'product_id' => $product,
			'sales_id' => $sales_id
		])->delete();
		return 'Success' . $this->input->post('id');
	}

	public function delProduct()
	{
		$product = productModels::destroy($this->input->post('id'));
		return 'Success' . $this->input->post('id');
	}

	public function delCustomer()
	{
		$product = CustomerModels::destroy($this->input->post('id'));
		return 'Success' . $this->input->post('id');
	}

	public function delCommerce()
	{
		$commerce = CommerceModels::destroy($this->input->post('id'));
		return 'Success' . $this->input->post('id');
	}

	public function delCourier()
	{
		$courier = courierModels::destroy($this->input->post('id'));
		return 'Success' . $this->input->post('id');
	}

	public function discountSchema($idPartner = 0)
	{
		$data['title'] = "Skema Bonus Diskon";

		$data['Satuan'] = satuanModels::all();
		$data['Schema'] = schemaDiscountModels::where('m_shippingaddress_id', $idPartner)->get();
		foreach ($data['Schema'] as $schema) {
			$schema['barang'] = productModels::where('id', $schema['m_product_id'])->pluck('name');
		}

		$data['shippingAddress'] = shippingAddressModels::where('id', $idPartner)->first();
		$data['Products'] = productModels::all();

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Master/discountSchema', $data);
		$this->load->view('Include/Footer.php');
	}

	public function insertDiscountSchema()
	{
		$_discSchema = $this->input->post('schema');
		$discSchema = new schemaDiscountModels();
		$_discSchema = json_decode($_discSchema, true);
		$discSchema->fill($_discSchema);
		$discSchema->save();
	}

	public function deleteDiscountSchema()
	{
		$idSchema = json_decode($this->input->post('idSchema'), true);
		$schema = schemaDiscountModels::destroy($idSchema);
	}

	public function productPrice($idPartner = 0)
	{
		$data['title'] = "Harga Jual Beli Barang";

		$data['Satuan'] = satuanModels::all();
		$data['Price'] = priceModels::where('m_partner_id', $idPartner)->get();
		foreach ($data['Price'] as $key => $price) {
			$price['barang'] = productModels::where('id', $price['m_product_id'])->pluck('name');
		}

		$data['Customer'] = customerModels::where('id', $idPartner)->first();
		$data['Products'] = productModels::all();

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Master/productPrice', $data);
		$this->load->view('Include/Footer.php');
	}

	public function insertProductPrice()
	{
		$_productPrice = $this->input->post('productPrice');
		$productPrice = new priceModels();
		$_productPrice = json_decode($_productPrice, true);
		$productPrice->fill($_productPrice);
		$productPrice->save();
	}

	public function deleteProductPrice()
	{
		$idPrice = json_decode($this->input->post('idPrice'), true);
		$idPrice = priceModels::destroy($idPrice);
	}

	public function shippingAddress($idPartner = 0)
	{
		$data['title'] = "Alamat Kirim";

		$data['Customer'] = customerModels::find($idPartner);
		$data['Address'] = $data['Customer']->shippingAddresses;
		foreach ($data['Address'] as $address) {
			$address['province'] = provinceModels::where('id', $address['m_province_id'])->pluck('name');
			$address['city'] = cityModels::where('id', $address['m_city_id'])->pluck('name');
		}
		$data['Province'] = provinceModels::all();
		$data['idPartner'] = $idPartner;

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Master/shippingAddress', $data);
		$this->load->view('Include/Footer.php');
	}

	public function addShippingAddress()
	{
		$requestBody = json_decode($this->input->post('payload'), true);

		$shippingAddress = new ShippingAddressModels;
		$shippingAddress->fill($requestBody);
		$shippingAddress->save();
	}

	public function updateShippingAddressState()
	{
		$_idAdd = $this->input->post('idAddress');
		$_state = $this->input->post('state');
		$idAdd = json_decode($_idAdd, true);
		$state = json_decode($_state, true);
		$shippingAddress = shippingAddressModels::where('id', $idAdd)->first();
		if ($shippingAddress !== null) {
			$shippingAddress->isactive = $state;
			$shippingAddress->save();
		}
	}

	public function addCourier()
	{
		$requestBody = json_decode($this->input->post('payload'), true);

		$courier = new CourierModels;
		$courier->fill($requestBody);
		$courier->save();
	}

	public function deleteMasterCourier()
	{
		$idCourier = json_decode($this->input->post('idCourier'), true);
		$courier = courierModels::where('id', $idCourier)->first();
		if ($courier !== null) {
			$courier->isactive = 0;
			$courier->save();
		}
	}

	public function admin()
	{
		$data['title'] = "Master Admin";
		$data['admin'] = AdminModels::get();

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Include/Navbar.php');
		$this->load->view('Master/Admin', $data);
		$this->load->view('Include/Footer.php');
	}

	public function product()
	{
		$data['title'] = "Master Produk";
		$data['Product'] = productModels::with('uom')->get();
		$data['UOM'] = UOMModels::get();

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Include/Navbar.php');
		$this->load->view('Master/Product', $data);
		$this->load->view('Include/Footer.php');
	}

	public function addProvince()
	{
		$requestBody = json_decode($this->input->post('payload'), true);
		$existingProvince = provinceModels::where('name', $requestBody['name'])->first();

		if ($existingProvince !== null) {
			$data['code'] = '414';
			$data['message'] = 'Lokasi yang dimasukkan sudah terdaftar.';
		} else {
			$data['code'] = '200';
			$province = new provinceModels;
			$province->fill($requestBody);
			$province->save();
		}

		echo json_encode($data);
	}


	public function addCity()
	{
		$requestBody = json_decode($this->input->post('payload'), true);
		$existingCity = cityModels::where('name', $requestBody['name'])->first();

		if ($existingCity !== null) {
			$data['code'] = '414';
			$data['message'] = 'Lokasi yang dimasukkan sudah terdaftar.';
		} else {
			$data['code'] = '200';
			$city = new cityModels;
			$city->fill($requestBody);
			$city->save();
		}

		echo json_encode($data);
	}

	public function getBonusDiscount()
	{
		$qty = $this->input->get('qty');
		$product = $this->input->get('m_product_id');
		$shipping = $this->input->get('m_shippingaddress_id');


		$diskon = 0;

		$bonus = DB::table('m_discount')
			->select('*')
			->where('m_product_id', '=', $product)
			->where('m_shippingaddress_id', '=', ($shipping == null) ? 0 : $shipping)
			->first();

		if ($qty >= $bonus['minvalue']) {
			$diskon = $bonus['discount'];
		}

		echo json_encode([
			'diskon' => $diskon
		]);
	}

	public function highchart()
	{
		$data['title'] = "Master Admin";
		$data['admin'] = AdminModels::get();

		$data['web'] = DB::select(DB::raw("
            SELECT * from web
        "));

		$data['detail_web'] = DB::select(DB::raw("
            SELECT * from detail_web
        "));

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Include/Navbar.php');
		$this->load->view('Master/highchart_view', $data);
		$this->load->view('Include/Footer.php');
	}
}
