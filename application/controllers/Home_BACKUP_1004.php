<?php

class Home extends CI_Controller
{

	public function __construct()
	{
		parent:: __construct();
		$this->load->model('HomeModels');
		$this->load->model('AdminModels');
	}

	public function login()
	{
		if ($this->session->has_userdata('user'))
			redirect('home/');
		else {
			$data['judul'] = "Login";

			$this->load->view('Home/loginView', $data);
		}
	}

	public function verifyUser()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$user = AdminModels::where('username', '=', $username)
			->where('status', '=', 1)
			->first();

		if ($user) {
			$isPasswordValid = password_verify($password, $user['password']);
			if ($isPasswordValid) {
				$_SESSION['user'] = $user->toArray();
				redirect('/home?user=');
			} else {
				$this->sendLoginMessage('error');
				redirect('/home/login');
			}
		} else {
			$this->sendLoginMessage('belum_login');
			redirect('/home/login');
		}
	}

	private function sendLoginMessage($messageType)
	{
		$this->session->set_flashdata('loginMessage', $messageType);
	}

	public function logout()
	{
		session_destroy();
		session_start();
		$this->sendLoginMessage('logout');

		redirect('home/login');
	}

	public function index()
	{
		checkSession();
		$data['title'] = 'Beranda';
		$sql = "SELECT WEEK(s.transaction_date)-WEEK(DATE_FORMAT(NOW() ,'%Y-%m-01')) as week, sum(s.grandtotal)  as revenue
				FROM t_sales s WHERE month(s.transaction_date) = month(now())
				GROUP BY WEEK(s.transaction_date)-WEEK(DATE_FORMAT(NOW() ,'%Y-%m-01'))";
		$data['weeklychart'] = $this->db->query($sql)->result();

		$sql = "
        	SELECT p.product_name as name, sum(sl.sales_qty) as y, uom.uom_name as unit
        	FROM t_sales s
        	JOIN t_salesline sl on s.id = sl.sales_id
        	JOIN m_product p on p.id = sl.product_id
        	JOIN m_uom uom on uom.id = p.product_uom_id
        	WHERE month(s.transaction_date) = month(now())
        	group by p.product_name, uom.uom_name
        	LIMIT 5
        ";
		$data['soldchart'] = $this->db->query($sql)->result();
		
		$sql = "SELECT pr.name as province_name, sum(sl.sales_qty) as y,po.id 
				FROM t_salesline sl
				JOIN t_sales s 
				on sl.sales_id=s.id
				JOIN m_customer cs 
				ON s.customer_id = cs.id
				JOIN m_province pr 
				ON cs.m_province_id = pr.id
				JOIN m_product po
				ON sl.product_id = po.id
				WHERE po.id IN (
				SELECT p.id
				FROM t_sales s
				JOIN t_salesline sl on s.id = sl.sales_id
				JOIN m_product p on p.id = sl.product_id
				JOIN m_uom uom on uom.id = p.product_uom_id
				WHERE month(s.transaction_date) = month(now())
				group by p.product_name, uom.uom_name
				LIMIT 5)
				GROUP BY province_name,po.id ";

		$data['soldchartDrilldown'] = $this->db->query($sql)->result();


		$sql = "
		SELECT pro.name as name, sum(s.grandtotal) as y
        	FROM t_sales s
        	JOIN m_customer c on c.id = s.customer_id
        	JOIN m_province pro on pro.id = c.m_province_id
        	WHERE month(s.transaction_date) = month(now())
        	group by pro.name
        ";
		$data['regionchart'] = $this->db->query($sql)->result();

		$sql = "SELECT SUM(GrandTotal) gt FROM T_Sales WHERE month(transaction_date) = month(now())";
		$data['earning'] = $this->db->query($sql);

		$sql = "SELECT c.customer_name as name, sum(s.grandtotal) as total
        FROM t_sales s 
        JOIN m_customer c on s.customer_Id = c.id
        WHERE MONTH(s.Transaction_date) = MONTH(NOW())
        GROUP BY c.customer_name";
		$data['customer_earning'] = json_decode(json_encode($this->db->query($sql)->result()), true);

		$sql = "SELECT p.Product_Name as name, (Qty) as qty, uom.uom_name as uom, t.total as total
        		FROM (
        			SELECT Product_ID, SUM(Sales_Qty) qty, SUM(sl.lineamt) total
        			FROM T_sales s
        			JOIN T_SalesLine sl ON s.id = sl.Sales_ID
        			WHERE MONTH(s.Transaction_Date) = MONTH(NOW())
        			GROUP BY product_ID 
        		) t
        		JOIN M_product p ON p.id = t.Product_ID
        		JOIN M_UOM uom ON uom.id = p.product_uom_id
        		GROUP BY p.Product_name
        		ORDER BY t.qty desc
        		";
		$data['best_product'] = json_decode(json_encode($this->db->query($sql)->result()), true);

		$sql = "SELECT  c.name as name, (Qty) as qty, t.total as total
        		FROM (
        			SELECT c.id, COUNT(*) qty, sum(s.grandtotal) total
        			FROM T_sales s
        			JOIN m_commerce c on c.id = s.sales_party_id
        			WHERE MONTH(s.Transaction_Date) = MONTH(NOW())
        			GROUP BY c.id 	
        		) t
        		JOIN m_commerce c on c.id = t.id
        		GROUP BY c.name
        		ORDER BY qty desc
        		";

		$data['best_party'] = json_decode(json_encode($this->db->query($sql)->result()), true);


		$first_day_this_month = date('Y-m-01');
		$last_day_this_month = date('Y-m-t');
		$period = strtoupper(date('M-Y'));

		$sql = "SELECT  * 
				FROM c_targetline tl
				JOIN c_target t ON tl.target_id=t.id
				JOIN m_product p on tl.product_id=p.id
				WHERE t.period = '$period'
				ORDER BY tl.product_id ASC
        		";

		$data['target_product'] = $this->db->query($sql)->result_array();

		$sql = "SELECT  sum(sl.sales_qty) as sales_quantity
				FROM t_salesline sl
				JOIN t_sales s on sl.sales_id = s.id
				JOIN m_product p on p.id =  sl.product_id
				WHERE sl.product_id IN (select product_id FROM c_targetline tl JOIN c_target t ON tl.target_id=t.id WHERE t.period = '$period'  ) 
				AND s.created_at between '" . $first_day_this_month . "' AND '" . $last_day_this_month . "'
				AND s.admin_id = " . $_SESSION['user']['id'] . "
				GROUP BY sl.product_id
				ORDER BY sl.product_id ASC
        		";

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Include/Navbar.php');
		if ($_SESSION['user']['role'] == 2)
			$this->load->view('Home/Super/home.php');
		else
			$this->load->view('Home/Sales/home.php');
		$this->load->view('Include/Footer.php');
	}
}
