<?php

class Home extends CI_Controller
{

	public function __construct()
	{
		parent:: __construct();
		$this->load->model('HomeModels');
		$this->load->model('AdminModels');
	}

	public function login()
	{
		if ($this->session->has_userdata('user'))
			redirect('home/');
		else {
			$data['judul'] = "Login";
			$this->load->view('Home/loginView', $data);
		}
	}

	public function verifyUser()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$user = AdminModels::where('username', '=', $username)
			->where('status', '=', 1)
			->first();

		if ($user) {
			$isPasswordValid = password_verify($password, $user['password']);
			if ($isPasswordValid) {
				$_SESSION['user'] = $user->toArray();
				redirect('/home?user=');
			} else {
				$this->sendLoginMessage('error');
				redirect('/home/login');
			}
		} else {
			$this->sendLoginMessage('belum_login');
			redirect('/home/login');
		}
	}

	private function sendLoginMessage($messageType)
	{
		$this->session->set_flashdata('loginMessage', $messageType);
	}

	public function logout()
	{
		session_destroy();
		session_start();
		$this->sendLoginMessage('logout');

		redirect('home/login');
	}

	public function index()
	{
		checkSession();
		$selectedMonth = $this->input->get('month');
		$year = $selectedMonth != null ? explode("|",$selectedMonth)[1] : date("Y");
		$selectedMonth = date($year.'-'.($selectedMonth != null ? explode("|",$selectedMonth)[0] : 'm').'-01');

		$data['title'] = 'Beranda';
		if($_SESSION['user']['role']==2){
			$sql = "SELECT WEEK(s.transaction_date)-WEEK(DATE_FORMAT('".$selectedMonth."' ,'%Y-%m-01')) as week, sum(s.grandtotal)  as revenue
				FROM t_sales s WHERE month(s.transaction_date) = month(now())
				GROUP BY WEEK(s.transaction_date)-WEEK(DATE_FORMAT(NOW() ,'%Y-%m-01'))";
			$data['weeklychart'] = $this->db->query($sql)->result();

			$sql = "
        	SELECT p.product_name as name, sum(sl.sales_qty) as y, uom.uom_name as unit,p.id,p.product_name as drilldown
        	FROM t_sales s
        	JOIN t_salesline sl on s.id = sl.sales_id
        	JOIN m_product p on p.id = sl.product_id
        	JOIN m_uom uom on uom.id = p.product_uom_id
        	WHERE month(s.transaction_date) = month('".$selectedMonth."')
        	AND year(s.transaction_date) = year('".$selectedMonth."')
        	group by p.product_name, uom.uom_name,p.id
        	order by y desc
        	limit 5
        ";
			$data['soldchart'] = json_encode($this->db->query($sql)->result());

			$sql = "
        	SELECT p.product_name as name, sum(sl.sales_qty) as y, uom.uom_name as unit,p.id,p.product_name as drilldown
        	FROM t_sales s
        	JOIN t_salesline sl on s.id = sl.sales_id
        	JOIN m_product p on p.id = sl.product_id
        	JOIN m_uom uom on uom.id = p.product_uom_id
        	WHERE month(s.transaction_date) = month('".$selectedMonth."')
        	AND year(s.transaction_date) = year('".$selectedMonth."')
        	group by p.product_name, uom.uom_name,p.id
        	order by y desc
        ";
			$data['soldchartd'] = json_encode($this->db->query($sql)->result());

			$sql = "SELECT pr.name as province_name, sum(sl.sales_qty) as y,po.id 
				FROM t_salesline sl
				JOIN t_sales s 
				on sl.sales_id=s.id
				JOIN m_customer cs 
				ON s.customer_id = cs.id
				JOIN m_province pr 
				ON cs.m_province_id = pr.id
				JOIN m_product po
				ON sl.product_id = po.id
				INNER JOIN (
					SELECT p.id
					FROM t_sales s
					JOIN t_salesline sl on s.id = sl.sales_id
					JOIN m_product p on p.id = sl.product_id
					JOIN m_uom uom on uom.id = p.product_uom_id
					WHERE month(s.transaction_date) = month('".$selectedMonth."')
					AND year(s.transaction_date) = year('".$selectedMonth."')
					group by p.product_name, uom.uom_name
				) as v2
				ON v2.id=po.id
				GROUP BY province_name,po.id ";

			$data['soldChartDrilldown'] = json_encode($this->db->query($sql)->result());

			/*
			 * Top Sales Chart
			 */
			$sql = "SELECT a.admin_name as name, sum(s.grandtotal) as y,a.id,a.admin_name as drilldown
        	FROM t_sales s
        	JOIN m_admin a on a.id = s.admin_id
        	WHERE month(s.transaction_date) = month('".$selectedMonth."')
        	AND year(s.transaction_date) = year('".$selectedMonth."')
        	group by a.admin_name, a.id
        	order by y desc
        	LIMIT 5
        ";
			$data['salesChart'] = json_encode($this->db->query($sql)->result());

			$sql = "SELECT a.admin_name as name, sum(s.grandtotal) as y,a.id,a.admin_name as drilldown
        	FROM t_sales s
        	JOIN m_admin a on a.id = s.admin_id
        	WHERE month(s.transaction_date) = month('".$selectedMonth."')
        	AND year(s.transaction_date) = year('".$selectedMonth."')
        	group by a.admin_name, a.id
        	order by y desc
        ";
			$data['salesChartd'] = json_encode($this->db->query($sql)->result());


			$sql = "SELECT po.product_name as product_name, sum(sl.lineamt) as y,a.id
				FROM t_salesline sl
				JOIN t_sales s
				on sl.sales_id=s.id
				JOIN m_customer cs
				ON s.customer_id = cs.id
				JOIN m_province pr
				ON cs.m_province_id = pr.id
				JOIN m_product po
				ON sl.product_id = po.id
				JOIN m_admin a
				ON a.id = s.admin_id
				INNER JOIN (
				SELECT ad.id
					FROM t_sales s
					join m_admin ad on ad.id = s.admin_id
					WHERE month(s.transaction_date) = month('".$selectedMonth."')
					AND year(s.transaction_date) = year('".$selectedMonth."')
					group by ad.admin_name
					LIMIT 5
				) as v2
				ON v2.id=a.id
				GROUP BY product_Name,a.id ";

			$data['salesChartDrilldown'] = json_encode($this->db->query($sql)->result());

			$sql = "SELECT po.product_name as product_name, sum(sl.lineamt) as y,a.id
				FROM t_salesline sl
				JOIN t_sales s
				on sl.sales_id=s.id
				JOIN m_customer cs
				ON s.customer_id = cs.id
				JOIN m_province pr
				ON cs.m_province_id = pr.id
				JOIN m_product po
				ON sl.product_id = po.id
				JOIN m_admin a
				ON a.id = s.admin_id
				INNER JOIN (
				SELECT ad.id
					FROM t_sales s
					join m_admin ad on ad.id = s.admin_id
					WHERE month(s.transaction_date) = month('".$selectedMonth."')
					AND year(s.transaction_date) = year('".$selectedMonth."')
					group by ad.admin_name
				) as v2
				ON v2.id=a.id
				GROUP BY product_Name,a.id ";

			$data['salesChartDrilldownd'] = json_encode($this->db->query($sql)->result());


			// Top Sales Chart --
			//echo $this->db->last_query();

			$sql = "
			SELECT p.product_name as name, sum(sl.sales_qty) as y, p.product_name as drilldown
			FROM t_sales s
			JOIN t_salesline sl ON sl.sales_id = s.id
			JOIN m_customer c on s.customer_id = c.id
			JOIN m_city ct on c.m_city_id = ct.id
			JOIN m_province pro on ct.m_province_id = pro.id
			JOIN m_product p on p.id = sl.product_Id
			WHERE month(s.transaction_date) = month('".$selectedMonth."')
			AND year(s.transaction_date) = year('".$selectedMonth."')
			group by p.product_name
			order by p.product_name
		";
			$data['regionChart'] = json_encode($this->db->query($sql)->result());

			//echo $this->db->last_query();
			$sql = "
        SELECT p.product_name as name, sum(s.grandtotal) as y, pro.name as drilldown, pro.id as province_id
        	FROM t_sales s
        	JOIN t_salesline sl ON sl.sales_id = s.id
			JOIN m_customer c on s.customer_id = c.id
			JOIN m_city ct on c.m_city_id = ct.id
			JOIN m_province pro on ct.m_province_id = pro.id
			JOIN m_product p on p.id = sl.product_Id
			WHERE month(s.transaction_date) = month('".$selectedMonth."')
			AND year(s.transaction_date) = year('".$selectedMonth."')
			group by p.product_name, ct.m_province_id
			order by  pro.id,p.product_name, pro.name
        ";


			$data['regionChartDrilldown'] = json_encode($this->db->query($sql)->result());

			$sql ="
			SELECT cit.name as name, sum(sl.sales_qty) as y, p.product_name as product_name, cit.m_province_id as province_id 
			FROM t_sales s
			JOIN t_salesline sl ON sl.sales_id = s.id
			JOIN M_product p on p.id = sl.product_id
			JOIN m_customer c on c.id = s.customer_id
			JOIN m_city cit on cit.id = c.m_city_id
			WHERE month(s.transaction_date) = month('".$selectedMonth."')
			AND year(s.transaction_date) = year('".$selectedMonth."')
			GROUP BY name, product_name, province_id
			order by cit.m_province_Id, p.product_Name, cit.name;
			";

			$data['regionChartDrilldown2'] = json_encode($this->db->query($sql)->result());

			//echo $this->db->last_query();

			$sql = "SELECT SUM(GrandTotal) gt FROM T_Sales WHERE month(transaction_date) = month('".$selectedMonth."')";
			$data['earning'] = $this->db->query($sql);

			$sql = "SELECT c.customer_name as name, sum(s.grandtotal) as total
        FROM t_sales s 
        JOIN m_customer c on s.customer_Id = c.id
        WHERE MONTH(s.Transaction_date) = MONTH('".$selectedMonth."')
        AND year(s.transaction_date) = year('".$selectedMonth."')
        GROUP BY c.customer_name";
			$data['customer_earning'] = json_decode(json_encode($this->db->query($sql)->result()), true);

			$sql = "SELECT p.Product_Name as name, (Qty) as qty, uom.uom_name as uom, t.total as total
        		FROM (
        			SELECT Product_ID, SUM(Sales_Qty) qty, SUM(sl.lineamt) total
        			FROM T_sales s
        			JOIN T_SalesLine sl ON s.id = sl.Sales_ID
        			WHERE MONTH(s.Transaction_Date) = MONTH('".$selectedMonth."')
        			AND year(s.transaction_date) = year('".$selectedMonth."')
        			GROUP BY product_ID 
        		) t
        		JOIN M_product p ON p.id = t.Product_ID
        		JOIN M_UOM uom ON uom.id = p.product_uom_id
        		GROUP BY p.Product_name
        		ORDER BY t.qty desc
        		";
			$data['best_product'] = json_decode(json_encode($this->db->query($sql)->result()), true);

			$sql = "SELECT  c.name as name, (Qty) as qty, t.total as total
        		FROM (
        			SELECT c.id, COUNT(*) qty, sum(s.grandtotal) total
        			FROM T_sales s
        			JOIN m_commerce c on c.id = s.sales_party_id
        			WHERE MONTH(s.Transaction_Date) = MONTH('".$selectedMonth."')
        			AND year(s.transaction_date) = year('".$selectedMonth."')
        			GROUP BY c.id 	
        		) t
        		JOIN m_commerce c on c.id = t.id
        		GROUP BY c.name
        		ORDER BY qty desc
        		";

			$data['best_party'] = json_decode(json_encode($this->db->query($sql)->result()), true);

			$data['sale'] = AdminModels::get();
		}else{
			$first_day_this_month = date('Y-m-01', strtotime($selectedMonth));
			$last_day_this_month = date('Y-m-t',strtotime($selectedMonth));
			$period = strtoupper(date('M-Y'));
			$sql = "SELECT  * 
				FROM c_targetline tl
				JOIN c_target t ON tl.target_id=t.id
				JOIN m_product p on tl.product_id=p.id
				WHERE '".$selectedMonth."' between t.validfrom and t.validto and tl.sales_id = '".$_SESSION['user']['id']."'
				ORDER BY tl.product_id ASC
        		";

			$data['target_product'] = $this->db->query($sql)->result_array();

			$sql = "SELECT  sum(sl.sales_qty) as sales_quantity
				FROM t_salesline sl
				JOIN t_sales s on sl.sales_id = s.id
				JOIN m_product p on p.id =  sl.product_id
				WHERE sl.product_id IN (select product_id FROM c_targetline tl JOIN c_target t ON tl.target_id=t.id WHERE now() between t.validfrom and t.validto  ) 
				AND s.transaction_date between '" . $first_day_this_month . "' AND '" . $last_day_this_month . "'
				AND s.admin_id = " . $_SESSION['user']['id'] . "
				GROUP BY sl.product_id
				ORDER BY sl.product_id ASC
        		";
			$data['realization_product'] = $this->db->query($sql)->result_array();

			$sql = "select p.product_name, coalesce(x1.target,0) as target_qty, coalesce(x2.realization,0) as sales_quantity from m_product p
				left join (select product_id, sum(tl.target_qty) as target from
				c_targetline tl JOIN c_target t ON tl.target_id = t.id
				where '".$selectedMonth."' between t.validfrom and t.validto and tl.sales_id = '".$_SESSION['user']['id']."'
				group by product_Id
				) as x1 on x1.product_id = p.id
				left join (select product_id, sum(sl.sales_qty) as realization
				from t_salesline sl
				join t_sales s on sl.sales_id = s.id
				where s.transaction_date between '" . $first_day_this_month . "' AND '" . $last_day_this_month . "' and s.admin_id = '".$_SESSION['user']['id']."'
				group by product_Id
				) as x2 on x2.product_Id = p.id";

			$data['real'] = $this->db->query($sql)->result_array();
			$sql = "
        	SELECT p.product_code as code, p.product_name as name, sum(sl.lineamt) as total
        	FROM t_sales s
        	JOIN t_salesline sl on s.id = sl.sales_id
        	JOIN m_product p on p.id = sl.product_id
        	JOIN m_uom uom on uom.id = p.product_uom_id
        	WHERE month(s.transaction_date) = month('".$selectedMonth."') 
        	AND year(s.transaction_date) = year('".$selectedMonth."')
        	AND s.admin_Id = ".$_SESSION['user']['id']."
        	group by p.product_name, p.product_code
        	order by p.product_code asc
        ";
			$data['soldchart'] = ($this->db->query($sql)->result_array());

			$sql = "
        	SELECT p.customer_Name as name, count(sl.sales_qty) as qty, sum(sl.lineamt) as total
        	FROM t_sales s
        	JOIN t_salesline sl on s.id = sl.sales_id
        	JOIN m_customer p on p.id = s.customer_Id
        	WHERE month(s.transaction_date) = month('".$selectedMonth."')
        	 AND year(s.transaction_date) = year('".$selectedMonth."')
        	 AND s.admin_Id = ".$_SESSION['user']['id']."
        	group by p.customer_name
        	order by p.customer_name asc
        ";
			$data['custchart'] = ($this->db->query($sql)->result_array());

		}

		$this->load->view('Include/Header.php', $data);
		$this->load->view('Include/Sidebar.php');
		$this->load->view('Include/Navbar.php');

		if ($_SESSION['user']['role'] == 2)
			$this->load->view('Home/Super/home.php');
		else
			$this->load->view('Home/Sales/home.php');

		$this->load->view('Include/Footer.php');
	}

	public function getTrendProduct(){

		$date1 = 	new DateTime(date('M-Y',strtotime($this->input->get('date1'))));
		$date2 = new DateTime(date('M-Y',strtotime($this->input->get('date2'))));
		$counter = $date1<$date2 ? ($date2->format('Y')-$date1->format('Y'))*12+($date2->format('m')-$date1->format('m')) : 0;

		$month = [];
		$trend = [];
		$qty = [[]];
		for($i = 0; $i<=$counter; $i++){
			$date = $date1;
			array_push($month, date_format($date,'M-Y'));
			$sql = "SELECT p.product_name, sum(case when s.id is not null then sl.sales_qty else NULL end) qty
				FROM M_Product p
				LEFT JOIN t_salesline sl ON sl.product_id = p.id
				LEFT JOIN t_sales s ON s.id = sl.sales_id and s.transaction_date between '".date_format($date,'Y-m-d')."' and '".date_format($date,'Y-m-t')."'
				group by p.product_Name";
			$result = $this->db->query($sql)->result_array();

			foreach($result as $r){
				if(!isset($qty[$r['product_name']])){
					array_push($trend, $r['product_name']);
					$qty[$r['product_name']] = [];
				}

				array_push($qty[$r['product_name']], $r['qty']);
			}

			;
//			array_push($trend,$result['product_name']);

			date_add($date1,date_interval_create_from_date_string("1 month"));
		}
		unset($qty["0"]);
		print_r(json_encode(Array('month' => json_encode($month), 'trend' =>json_encode($trend), 'qty' => json_encode($qty, JSON_NUMERIC_CHECK))));
	}
}
