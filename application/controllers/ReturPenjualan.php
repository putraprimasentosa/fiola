<?php

use PHPJasper\PHPJasper;

require_once('DocumentController.php');

class ReturPenjualan extends DocumentController {
	public function __construct() {
		parent:: __construct();
		$this->load->model('CourierModels');
		$this->load->model('SalesModels');
		$this->load->model('SalesLineModels');
		$this->load->model('CustomerModels');
		$this->load->model('ShippingAddressModels');
		$this->load->model('SalesReturn/SalesReturnModels');
		$this->load->model('SalesReturn/SalesReturnDetailModels');
		$this->load->model('SalesReturn/SalesReturnClaimModels');
		$this->load->model('ProductModels');
		$this->setNumberData(DOCUMENT_TYPE_SALES_RETURN, DOCUMENT_NO_PREFIX_SALES_RETURN);
	}

	public function index() {
		$data = [
			'title' => 'Retur Penjualan',
			'data' => salesReturnModels::with('order', 'claim')
				->orderBy('id', QUERY_ORDER_DESCENDING)
				->get()
		];

		$this->load->view('include/Header', $data);
		$this->load->view('include/Sidebar');
		$this->load->view('SalesReturn/salesReturnList', $data);
		$this->load->view('include/Footer');
	}

	public function showAddSalesReturn() {
		$partners = CustomerModels::where([
			'iscustomer' => true,
			'isactive' => true
		])->get();
		$documentNo = $this->getDocumentNumber();

		$data = [
			'title' => 'Tambah Retur Penjualan',
			'partners' => $partners,
			'documentNo' => $documentNo
		];

		$this->load->view('include/Header', $data);
		$this->load->view('include/Sidebar');
		$this->load->view('SalesReturn/addSalesReturn', $data);
		$this->load->view('include/Footer');
	}

	public function showDetail($id) {
		$returnData = SalesReturnModels::find($id);
		$returnDetailData = SalesReturnDetailModels::with('item')->where('t_salesreturn_id', $id)->get();
		$orderData = SalesModels::with('details')->find($returnData->t_order_id);
		$orderDetails = $orderData->details->toArray();

		$mergedReturnDetailData = [];

		foreach ($returnDetailData as $returnDetail) {
			$singlePrice = $returnDetail->subtotalreturn / $returnDetail->totalqty;
			$itemId = $returnDetail->item->id;

			// Matches price and item ID to get the matching order detail to get order price.
			$matchedDetail = array_filter($orderDetails, function ($orderDetail) use ($singlePrice, $itemId) {
				$singlePriceMatch = intval($orderDetail['price']) === $singlePrice;
				$itemIdMatch = $orderDetail['m_product_id'] === $itemId;

				return $singlePriceMatch && $itemIdMatch;
			});

			$mergedDetail = array_merge($returnDetail->toArray(), [
				'price' => reset($matchedDetail)['price']
			]);
			array_push($mergedReturnDetailData, $mergedDetail);
		}

		if ($returnData === null) {
			show_404();
		}

		$data = [
			'title' => 'Detail Retur',
			'returnData' => $returnData,
			'returnDetailData' => $mergedReturnDetailData
		];

		$this->load->view('include/Header', $data);
		$this->load->view('include/Sidebar');
		$this->load->view('SalesReturn/salesReturnDetail', $data);
		$this->load->view('include/Footer');
	}

	public function printOrder($salesReturnId) {
		$input = FCPATH.'report/PPS_NOTA_RETUR_A5.jrxml';
		$output = FCPATH.'report/Retur';
		$options = [
			'format' => ['pdf'],
			'locale' => 'en',
			'params' => [
				'ID' => $salesReturnId,
			],
			'db_connection' => [
                'driver' => 'postgres',
                'username' => env('ELOQUENT_USERNAME',''),
                'password' => env('ELOQUENT_PASSWORD',''),
                'host' => env('ELOQUENT_HOST',''),
                'database' => env('ELOQUENT_DATABASE_NAME',''),
                'port' => '5432',
			],
			'resources' => FCPATH.'report/Adempiere.jar'
		];
		$jasper = new PHPJasper;

		$jasper->process(
			$input,
			$output,
			$options
		)->execute();
		$file = file_get_contents($output.'.pdf');
		header('Content-type: application/pdf');

		@readfile($file);
		print_r($file);
	}

    public function editRetur($id) {
        $returnData = SalesReturnModels::find($id);
        $returnDetailData = SalesReturnDetailModels::with('item')->where('t_salesreturn_id', $id)->get();
        $orderData = SalesModels::with('details')->find($returnData->t_order_id);
        $orderDetails = $orderData->details->toArray();

        $mergedReturnDetailData = [];

        foreach ($returnDetailData as $returnDetail) {
            $singlePrice = $returnDetail->subtotalreturn / $returnDetail->totalqty;
            $itemId = $returnDetail->item->id;

            // Matches price and item ID to get the matching order detail to get order price.
            $matchedDetail = array_filter($orderDetails, function ($orderDetail) use ($singlePrice, $itemId) {
                $singlePriceMatch = intval($orderDetail['price']) === $singlePrice;
                $itemIdMatch = $orderDetail['m_product_id'] === $itemId;

                return $singlePriceMatch && $itemIdMatch;
            });

            $mergedDetail = array_merge($returnDetail->toArray(), [
                'price' => reset($matchedDetail)['price']
            ]);
            array_push($mergedReturnDetailData, $mergedDetail);
        }

        if ($returnData === null) {
            show_404();
        }

        $data = [
            'title' => 'Detail Retur',
            'returnData' => $returnData,
            'returnDetailData' => $mergedReturnDetailData
        ];

        $this->load->view('include/Header', $data);
        $this->load->view('include/Sidebar');
        $this->load->view('SalesReturn/editSalesReturn', $data);
        $this->load->view('include/Footer');
    }
}
