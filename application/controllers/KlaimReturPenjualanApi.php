<?php
require_once('DocumentController.php');

use Illuminate\Database\Capsule\Manager as Capsule;

class KlaimReturPenjualanApi extends DocumentController {
  public function __construct() {
    parent::__construct();
    $this->load->model('SalesReturn/SalesReturnClaimModels');
    $this->load->model('SalesReturn/SalesReturnClaimDetailModels');
    $this->load->model('SalesReturn/SalesReturnClaimConditionDetailModels');
    $this->load->model('SalesReturn/GoodsConditionModels');
		$this->setNumberData(DOCUMENT_TYPE_SALES_RETURN_CLAIM, DOCUMENT_NO_PREFIX_SALES_RETURN_CLAIM);
  }

  private function getSubtotalByCondition($returnClaimDetailRequest, $returnClaimDetailRecord) {
    $pricePerUnit = $returnClaimDetailRequest->subtotal / $returnClaimDetailRequest->qty;
    $conditions = GoodsConditionModels::all();
    $grandTotalClaim = 0;

    try {
      foreach ($conditions as $condition) {
        $conditionKey = $condition->key;
        $conditionDescriptionKey = $conditionKey.'Description';
        $numberOfGoods = $returnClaimDetailRequest->$conditionKey;
        $description = $returnClaimDetailRequest->$conditionDescriptionKey;

        if ($numberOfGoods === 0) {
          continue;
        }

        $claimPercentage = $condition->claimpercentage;
				$claimLinePrice = $pricePerUnit * ($claimPercentage / PERCENTAGE_DIVIDER);
				$subtotalClaim = $numberOfGoods * $claimLinePrice;
				$grandTotalClaim += $subtotalClaim;

        $goodConditionDetails = new SalesReturnClaimConditionDetailModels();
        $goodConditionDetails->fill([
          'qty' => $numberOfGoods,
          'claimprice' => ceil($claimLinePrice),
          'subtotalclaim' => ceil($subtotalClaim),
					'claimdescription' => $description
        ]);
        $goodConditionDetails->condition()->associate($condition);
        $goodConditionDetails->parent()->associate($returnClaimDetailRecord);
        $goodConditionDetails->save();
      }

      $returnClaimDetailRecord->subtotalclaim = ceil($grandTotalClaim);
      $returnClaimDetailRecord->save();

      return $returnClaimDetailRecord;
    } catch (Exception $error) {
      throw $error;
    }
  }

  public function createSalesReturnClaim() {
    $info = json_decode($this->input->post('info'), true);
    $detail = json_decode($this->input->post('detail'));
    $totalReturnClaim = 0;

    try {
      Capsule::beginTransaction();

      $salesReturnClaim = SalesReturnClaimModels::create([
        't_salesreturn_id' => $info['salesReturnId'],
        'claimdate' => DateTime::createFromFormat('d-m-Y', $info['returnClaimDate'])->format('Y-m-d'),
        'claimno' => $info['returnClaimNo'],
      ]);

      foreach ($detail as $item) {
        $salesReturnClaimDetail = new SalesReturnClaimDetailModels();
        $salesReturnClaimDetail->fill([
          'm_product_id' => $item->itemId,
					'qty' => $item->qty
        ]);

        $salesReturnClaimDetail->parent()->associate($salesReturnClaim);
        $salesReturnClaimDetail->save();

        $updatedDetail = $this->getSubtotalByCondition($item, $salesReturnClaimDetail);

        $totalReturnClaim = $totalReturnClaim + $updatedDetail->subtotalclaim;
      }

      $salesReturnClaim->totalreturnclaim = $totalReturnClaim;
      $salesReturnClaim->save();

      $this->incrementDocumentNumber();

      Capsule::commit();

      $this->output->set_status_header(CREATED_CODE);
      echo json_encode([
        'id' => $salesReturnClaim->id,
      ]);
    } catch (Exception $error) {
      Capsule::rollback();
      log_message(LOG_LEVEL_ERROR, $error->getMessage());

      $this->output->set_status_header(INTERNAL_SERVER_ERROR_CODE);
      $this->data['message'] = $error->getMessage();

      echo json_encode($this->data);
    }
  }
}
