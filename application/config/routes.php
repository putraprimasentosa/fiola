<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Claim sales return routes
$route['klaim-retur-penjualan']['POST'] = 'KlaimReturPenjualanApi/createSalesReturnClaim';
$route['klaim-retur-penjualan/tambah/(:num)'] = 'KlaimReturPenjualan/showAddSalesReturnClaim/$1';
$route['klaim-retur-penjualan/(:num)'] = 'KlaimReturPenjualan/showReturnClaimDetail/$1';

// Sales return routes
$route['retur-penjualan']['GET'] = 'ReturPenjualan';
$route['retur-penjualan']['POST'] = 'ReturPenjualanApi/createSalesReturn';
$route['retur-penjualan/tambah'] = 'ReturPenjualan/showAddSalesReturn';
$route['retur-penjualan/(:num)'] = 'ReturPenjualan/showDetail/$1';
$route['retur-penjualan/edit/(:num)'] = 'ReturPenjualan/editRetur/$1';

// Order routes
$route['partner/(:num)/order'] = 'Penjualan/getOrdersByCustomerId/$1';
$route['order/(:num)'] = 'getOrder/$1';
